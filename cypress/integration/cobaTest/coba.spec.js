context('Assertions',function () {

    function doLogin(username, password) {
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.contains("Log Out").click({force: true});
        cy.url().should("include", "/login");
    }

    beforeEach(function () {
        Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_session"); //local
        // Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session"); //taurus
        cy.viewport(1366,768);
    });

    before(function () {
        cy.clearCookies();
    });
    describe("Contoh Upload", function () {
        it(".should() - success login as OCTIDWI (Anggaran)", function () {
            cy.visit("http://localhost/sipatpln/public/login");
            doLogin("OCTIDWI", "123");
        });
        it(".should() - success upload", function () {
            cy.visit("http://localhost/sipatpln/public/anggaran/anggterbit/skki");
            cy.get("tr td a").contains("03R/AI-DIS/W.ACEH/2019").click();
            cy.contains("UPLOAD FILE").click();

            cy.wait(3000);

            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });

            cy.get("button[type='submit']").contains("KIRIM").click();
        });

    });
});