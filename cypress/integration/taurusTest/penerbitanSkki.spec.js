context('Assertions',function () {
    beforeEach(function () {
        // cy.visit("http://localhost/sipatpln/public/login")
        Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session");
        // Cypress.Cookies.preserveOnce();
        cy.viewport(1366,768);

    });

    before(function () {
        cy.clearCookies();
    });

    describe("Buat 1 PRK, 1 SKKI dengan 1 PRK, <10M", function () {
        it(".should() - success login as OCTIDWI (Anggaran)", function () {
            cy.visit("http://103.28.22.109/sipat-testing/public/login");
            cy.contains("Sign In");
            cy.get("#emailaddress")
                .type("OCTIDWI")
                .should("have.value", "OCTIDWI");
            cy.get("#password")
                .type("123")
                .should("have.value", "123");
            cy.contains("Sign In").click();
            cy.url().should("include", "public/");

            Cypress.on('uncaught:exception', (err, runnable) => {
                // returning false here prevents Cypress from
                // failing the test
                return false;
            });
        });
        it(".should() - success create PRK", function () {
            cy.get("li span").contains("ANGGARAN").click();
            cy.contains("PRK").click();
            cy.contains("PRK SKKI").click();
            cy.url().should('include', '/anggaran/prkai');
            cy.contains("TAMBAH PRK").click();
            cy.get("#jnspagu").select("DEFINITIF");
            cy.get("span").contains("PILIH PRK").click();
            cy.get("#prk").select("2019.WNAD.1.001", {force: true});
            cy.get("input[name='jumlah']").type(1000000).should("have.value", 1000000);
            cy.get("input[name='disburse']").type(1000000).should("have.value", 1000000);
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.contains("Berhasil");
            // cy.screenshot();
        });
        it(".should() - success create Kel Prk", function () {
            cy.get("li span").contains("INPUT AI / AO").click();
            cy.contains("INVESTASI").click();
            cy.contains("PILIH PRK").click();
            cy.get("#unitprk").select("6101");
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("#prk").select("2019.WNAD.1.001.02");
            cy.get("#rppakai").type(1000000).should("have.value", 1000000);
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.contains("Berhasil");
        });
        it(".should() - success create SKKI", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("button span").contains("BUAT SKKI").click();
            cy.get("#gennoskki").click();
            cy.get("select[name='f_kd_ao']").select("PERENCANAAN");
            cy.get("textarea[name='uraian_pekerjaan']").type("AUTO TEST").should("have.value", "AUTO TEST");
            cy.get("#fungsi").select("PEMBANGKIT");
            cy.wait(2000); //wait for program to get value
            cy.get("input[name='notadinas']").type("AT/I/2019").should("have.value", "AT/I/2019");
            cy.get("input[name='tgl_notadinas']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("select[name='nd']").select("PERENCANAAN");
            cy.get("select[name='jnspagu']").select("DEFINITIF");
            cy.get("button[type='submit']").contains("SIMPAN DATA").click();
            cy.contains("Berhasil");
        });
    });

});