context('Assertions',function () {

    function doLogin(username, password) {
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.get("#btnLogout").click({force: true});
    }

    beforeEach(function () {
        // cy.visit("http://localhost/sipatpln/public/login")
        Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session");
        // Cypress.Cookies.preserveOnce();
        cy.viewport(1366,768);
        

    });

    before(function () {
        cy.clearCookies();
    });

    describe("KOREKSI KONTRAK", function () {
        it(".should() - success login as A.NOBEL (ASMAN AREA)", function () {
            cy.visit("http://103.28.22.109/sipat-testing/public/login");
            doLogin("A.NOBEL", "123");
        });
        it(".should() - success KOREKSI NOTA DINAS", function () {
            cy.get("li span").contains("ASMAN").click();
            cy.contains("PERUBAHAN").click();
            cy.contains("KOREKSI").click();
            cy.url().should('include', '/public/kontrak-revisi');
            cy.contains("nd-sko-123").click();
            cy.get("textarea[name='reason']").type("Ada data yang tidak sesuai").should("have.value", "Ada data yang tidak sesuai");
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN PERUBAHAN").click();
            cy.contains("Berhasil");
            // cy.screenshot();
        });
        it(".should() - logout dan login as EDDI SAPUTRA (Manager AREA)", function () {
            doLogout();
            doLogin("EDDI.S", "123");
        });
        //Masih belum selesai di aplikasi :)
        /*it(".should() - success APPROVAL KOREKSI NOTA DINAS", function () {
            cy.get("li span").contains("MANAGER AREA").click();
            cy.contains("APPROVAL").click();
            cy.url().should('include', '/pengadaan/listnd_ma');
            cy.get("#dt-approval-nd > tbody > tr:nth-child(1) > td.sorting_1 > a").click();
            cy.wait(3000); //wait all xhr
            cy.contains("TERKIRIM").click();
            cy.get("select[name='METODE']").select("Pengadaan Langsung");
            cy.get("button[type='submit'").contains("OK").click();
            cy.contains("Berhasil");
            // cy.screenshot();
        });*/
    });
});