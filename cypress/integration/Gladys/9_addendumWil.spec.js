context('Assertions',function () {

    function doLogin(username, password) {
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.get("#btnLogout").click({force: true});
    }

    beforeEach(function () {
        // cy.visit("http://localhost/sipatpln/public/login")
        Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session");
        // Cypress.Cookies.preserveOnce();
        cy.viewport(1366,768);
        

    });

    before(function () {
        cy.clearCookies();
    });

    describe("ADDENDUM", function () {
        it(".should() - success login as RISDA MULYANI (DM WILAYAH)", function () {
            cy.visit("http://103.28.22.109/sipat-testing/public/login");
            doLogin("RISDA", "123");
        });
        it(".should() - success ADDENDUM KONTRAK", function () {
            cy.visit("http://103.28.22.109/sipat-testing/public/kontrakAddendum-revisi");
            cy.contains("FILTER").click();
            cy.get("select[name='tahun']").select("2018");
            cy.get("button[type='submit'").contains("PILIH").click();
            cy.get("#datatable-kontrakAddendum-revisi > tbody > tr:nth-child(1) > td.dt-left.sorting_1 > a").click();
            cy.contains("UBAH HARGA PRK").click();
            cy.get("#datatable-prk-edit > tbody > tr > td.dt-center > button").click();
            cy.get("input[name='addendum']").type(1000000).should("have.value", 1000000);
            cy.contains("SIMPAN").click();
            cy.contains("KEMBALI").click();
            cy.get("select[name='dasar_permohonan']").select("ADDENDUM_KONTRAK");
            //seharusnya ini yang di upload pdf, tapi di cypress upload image(png) berhasil :)
            cy.fixture("gambar.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "gambar/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'gambar.png', { type: 'gambar/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });
            cy.wait(3000); 
            cy.get("input[name='no_permohonan']").type("AD.2019.03.22-01").should("have.value", "AD.2019.03.22-01");
            cy.get("textarea[name='note']").type("TEST").should("have.value", "TEST");
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.contains("Berhasil"); //Masih Error
            // cy.screenshot();
        });
    });
});