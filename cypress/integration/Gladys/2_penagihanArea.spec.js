context('Assertions',function () {

    function doLogin(username, password) {
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.get("#btnLogout").click({force: true});
    }

    beforeEach(function () {
        // cy.visit("http://localhost/sipatpln/public/login")
        Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session");
        // Cypress.Cookies.preserveOnce();
        cy.viewport(1366,768);
        

    });

    before(function () {
        cy.clearCookies();
    });

    describe("PENAGIHAN AREA", function () {
        it(".should() - success login as EDDI SAPUTRA (Manager AREA)", function () {
            cy.visit("http://103.28.22.109/sipat-testing/public/login");
            doLogin("EDDI.S", "123");
        });
        it(".should() - success DISPOSISI", function () {
            cy.get("li span").contains("MANAGER AREA").click();
            cy.contains("ANGGARAN MASUK").click();
            cy.url().should('include', '/pengadaan/anggaranmsk');
            cy.contains("02/R/AO-SDM/W.ACEH/2019-LSA").click();
            cy.get("input[name='RENC']").check(); //untuk ceklis pilihan disposisi bagian(RENC=Perencanaan)
            cy.get("#keterangan").type("TEST").should("have.value", "TEST");
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.contains("Berhasil");
            // cy.screenshot();
        });
        it(".should() - logout dan login as BIAN (PIC AREA)", function () { //untuk Kota Langsa Bagian Perencanaan
            doLogout();
            doLogin("BIAN.W", "123");
        });
        it(".should() - success INPUT NOTA DINAS", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("MY SECTION").click();
            cy.contains("EXECUTION").click();
            cy.contains("NOTA DINAS").click();
            cy.url().should('include', '/pengadaan/tbh_kontrak');
            cy.contains("02/R/AO-SDM/W.ACEH/2019-LSA").click();
            cy.contains("BUAT / TAMPIL NOTA DINAS").click();
            cy.contains("BUAT NOTA DINAS BARU").click();
            cy.contains("PILIH PRK").click();
            cy.get("#pilihprk").select("PRK.2019.4.12.52400.1|AO000011|SKKO");
            cy.get("#pakai").type(10000).should("have.value", 10000);
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.contains("Berhasil");
        });
        it(".should() - success NOTA DINAS IJIN PRINSIP", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("button span").contains("NOTA DINAS IJIN PRINSIP").click();
            cy.wait(2000); //wait for program to get value
            cy.get("input[name='no']").type("1234").should("have.value", "1234");
            cy.get("input[name='tgl']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='sifat']").type("PENTING").should("have.value", "PENTING");
            cy.get("input[name='lampiran']").type("PERIHAL").should("have.value", "PERIHAL");
            cy.get("select[name='jenis_kontrak']").select("JASA");
            cy.wait(2000); //wait for program to get value
            cy.get("input[name='perihal']").type("TEST").should("have.value", "TEST");
            cy.get("select[name='sbr_pendanaan']").select("APLN");
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });
            cy.get("select[name='jns_pekerjaan']").select("RUTIN");
            //masukkan html belum bisa :)
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.contains("Berhasil");
        });
        it(".should() - logout dan login as EDDI SAPUTRA (Manager AREA)", function () {
            doLogout();
            doLogin("EDDI.S", "123");
        });
        it(".should() - success APPROVAL NOTA DINAS", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("MANAGER AREA").click();
            cy.contains("APPROVAL").click();
            cy.get("#table-2 > tbody > tr:nth-child(10) > td:nth-child(2) > a").click(); 
            //note: #table-2: id, tr:nth-child(9): baris keberapa, td:nth-child(2): kolom keberapa
            //cara mudah: inspect-klik kanan-copy selector :)
            cy.contains("APPROVE").click();
            cy.wait(2000);
            cy.get("select[name='METODE']").select("Pengadaan Langsung");
            cy.get("button[type='submit']").contains("APPROVE").click();
            cy.contains("Berhasil");
        });
        it(".should() - logout dan login as BIAN (PIC AREA)", function () { //untuk Kota Langsa Bagian Perencanaan
            cy.visit("http://103.28.22.109/sipat-testing/public/login");
            doLogout();
            doLogin("BIAN.W", "123");
        });
        it(".should() - success UPLOAD RKS", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("MY SECTION").click();
            cy.contains("EXECUTION").click();
            cy.contains("INPUT RKS").click();
            cy.url().should('include', '/pengadaan/rks');
            cy.get("#table-1 > tbody > tr > td:nth-child(2) > a").click();
            //tr:nth-child(2)=tabel ke 2, td:nth-child(2)=baris kedua
            cy.get("a").contains("RENCANA KERJA DAN SYARAT").click(); //apabila ada tulisan yang ganda
            cy.contains("UPLOAD RKS").click();
            cy.get("input[name='NOMOR']").type("TS-01").should("have.value", "TS-01");
            cy.get("input[name='TGL']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            //seharusnya ini yang di upload pdf, tapi di cypress upload image(png) berhasil :)
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("input[name='PEL_PENG1']").type("OCTI DWI").should("have.value", "OCTI DWI");
            cy.get("input[name='PEL_PENG2']").type("EDDI SAPUTRA").should("have.value", "EDDI SAPUTRA");
            cy.get("input[name='MENYETUJUI']").type("DEVARIZA").should("have.value", "DEVARIZA");
            cy.get("button[type='submit']").contains("SIMPAN RKS").click();
            cy.contains("Berhasil");
        });
        it(".should() - success UPLOAD HPE", function () {
            cy.get("body > div > div.content-wrapper > section.content > div > div > div.box > div.box-body > div > div:nth-child(8) > div.box-body > table > tbody > tr > td:nth-child(6) > a").click();
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.wait(3000); //tunggu masukan data ke select).click();
            cy.get("a").contains("HARGA PERKIRAAN ENGINEERING").click();
            //seharusnya ini yang di upload pdf, tapi di cypress upload image(png) berhasil :)
            cy.fixture("gambar.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "gambar/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'gambar.png', { type: 'gambar/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            }); 
            //note: kalau ada button 2 yang sama, masukkan id form sebelum buttonnya
            cy.get("#hpe-form button[type='submit']").contains("SIMPAN").click();
            cy.contains("Berhasil");  
        });
        it(".should() - success SIMPAN DATA", function () {
            cy.get("#updaterks > button[type='submit']").contains("SIMPAN").click();
            cy.contains("Berhasil");
        });
        it(".should() - logout dan login as RULI RIZALUDDIN (ASMAN APPROVAL)", function () {
            doLogout();
            doLogin("RULI", "123");
        });
        it(".should() - success APPROVAL UPLOAD RKS & HPE", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("ASMAN").click();
            cy.contains("EXECUTION").click();
            cy.contains("APPROVAL").click();
            cy.get("#approval-table > tbody > tr > td.sorting_1 > a").click(); 
            //note: #approval-table: id, td.sorting_1: sorting pertama
            //cara mudah: inspect-klik kanan-copy selector :)
            cy.get("button[type='submit']").contains("APPROVE").click();
            cy.contains("Berhasil");
        });
        it(".should() - logout dan login as RIDWAN SURYALESMANA (LAKDAN AREA)", function () {
            doLogout();
            doLogin("RIDWAN", "123");
        });
        it(".should() - success UPLOAD/PEMBUATAN KONTRAK", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("PENGADAAAN").click();
            cy.contains("INPUT SPK/KONTRAK").click();
            cy.url().should('include', '/pengadaan/spk');
            cy.get("#raport-table > tbody > tr:nth-child(3) > td.sorting_1 > a").click();
            cy.get("#sendspk > div.box.box-solid.box-info > table > tbody > tr > td:nth-child(8) > a").click();
            cy.contains("SIMPAN").click({force:true});
            cy.contains("UPLOAD PERJANJIAN").click({force:true});
            cy.get("input[name='lokasi']").type("Aceh Langsa").should("have.value", "Aceh Langsa");
            cy.get("select[name='vendor_id']").select("PT. VEGANA ELEKTRIK UTAMA", {force:true});
            cy.get("input[name='nomor']").type("123").should("have.value", "123");        
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                });
            });
            cy.get("input[name='no_penawaran']").type("123").should("have.value", "123");
            cy.get("input[name='no_skma']").type("321").should("have.value", "321");
            cy.get("input[name='tgl0']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='tgl1']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='tgl2']").type("30/03/2019").should("have.value", "30/03/2019"); //hari lebih besar dari tanggal hari ini
            cy.get("input[name='nm_pihakii']").type("DEVARIZA").should("have.value", "DEVARIZA");
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN PERJANJIAN").click({force:true});
            cy.contains("Berhasil");
            // cy.screenshot();  
            cy.get("button[type='submit'").contains("KIRIM PROGRESS KE ASMAN").click({force:true});
            cy.contains("Berhasil");   
        });
        it(".should() - logout dan login as BIAN (PIC AREA)", function () { //untuk Kota Langsa Bagian Perencanaan
            cy.visit("http://103.28.22.109/sipat-testing/public/login");
            doLogout();
            doLogin("BIAN.W", "123");
        });
        it(".should() - success INPUT RAPORT", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("MY SECTION").click();
            cy.contains("PENILAIAN").click();
            cy.contains("ADD RAPORT").click();
            cy.url().should('include', '/public/penilaian');
            cy.get("#raport-table > tbody > tr > td:nth-child(6) > a").click();
            //note: nilai yang dikasih
            cy.get("#R_mutu_3").click();  
            cy.get("#R_waktu_4").click();
            cy.get("#R_lingkungan_4").click();
            cy.get("#R_ketaatan_dokumen_4").click();
            cy.get("#R_komunikasi_4").click();
            cy.get("input[type='submit'").contains("Submit Nilai Vendor").click({force:true});
            cy.contains("Berhasil");
        });
        it(".should() - success INPUT PENAGIHAN", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("MY SECTION").click();
            cy.contains("PENAGIHAN").click();
            cy.contains("KONTRAK").click();
            cy.url().should('include', '/public/penagihan');
            cy.contains("INPUT TAGIHAN").click();
            cy.get("#nokontrak").select("123-TEST", {force:true});
            cy.get("select[name='termin']").select("3", {force:true});
            cy.get("#listPrk1").type("5").should("have.value", 5);
            cy.get("textarea[name='ket_tagihan']").type("TES").should("have.value", "TES");
            cy.get("textarea[name='nomiro']").type("T-001").should("have.value", "T-001");
            cy.get("input[name='nopo']").type("1111").should("have.value", "1111");
            cy.get("input[name='nose']").type("0001").should("have.value", "0001");
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.contains("Berhasil");
        });
        it(".should() - logout dan login as HERLINA (KEUANGAN AREA)", function () { //untuk Kota Langsa
            cy.visit("http://103.28.22.109/sipat-testing/public/login");
            doLogout();
            doLogin("YESI", "123");
        });
        it(".should() - success VERIFIKASI PENAGIHAN", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("VERIFIKASI").click();
            cy.contains("KONTRAK").click();
            cy.url().should('include', '/public/verifikasi/siap_bayar');
            cy.get("#datatable-siap_bayar > tbody > tr > td:nth-child(2) > a").click();
            cy.get("a").contains("FORM VERIFIKASI").click();
            cy.get("input[name='pembayaran[0][jml_tagihan]']").type(1000000).should("have.value", 1000000);
            cy.get("input[name='nopo']").type("PO-01").should("have.value", "PO-01");
            cy.get("button[type='submit'").contains("VERIFIKASI").click();
            cy.contains("Berhasil");
        });
    });
});