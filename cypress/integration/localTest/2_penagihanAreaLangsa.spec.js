context('Assendrtions',function () {

    var geminiUrl = "http://192.168.0.2/sipat";
    var geminiTestingUrl = "http://192.168.0.2/sipat-testing";
    var taurusTestingUrl = "http://103.28.22.109/sipat-testing";
    var taurusUrl = "http://103.28.22.109/sipat";
    var localUrl = "http://localhost/sipatpln";
    var usedUrl = "";
    var GEMINI = "GEMINI";
    var GEMINITESTING = "GEMINITESTING";
    var TAURUSTESTING = "TAURUSTESTING";
    var TAURUS = "TAURUS";
    var LOCAL = "LOCAL";
    var useConfig = LOCAL;

    function doLogin(username, password) {
        cy.wait(3000);
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.get("#btnLogout").click({force: true});
    }

    beforeEach(function () {
        // cy.visit(usedUrl + "/public/login")
        if(useConfig == TAURUSTESTING || useConfig == GEMINITESTING) {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session");
        }
        else if(useConfig == TAURUS){
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_git_session");
        }
        else {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_session");
        }
        // Cypress.Cookies.preserveOnce();
        cy.viewport(1366,768);
    });

    before(function () {
        cy.clearCookies();
        switch (useConfig) {
            case TAURUSTESTING:
                usedUrl = taurusTestingUrl;
                break;
            case TAURUS:
                usedUrl = taurusUrl;
                break;
            case GEMINI:
            default:
                usedUrl = geminiUrl;
                break;
            case GEMINITESTING:
                usedUrl = geminiTestingUrl;
                break;
            case LOCAL:
                usedUrl = localUrl;
                break;
        }
    });

    var nomorSKKI = "01/R/AI-REN/W.ACEH/2019-LSA";
    var nomorNotaDinas = "ND/AUTO/2019/I";
    var nomorRKS = "RKS/AUTO/2019/I";
    var kontrakid = "KT000001";
    var nomorPR = "PR/AT/2019/I";
    var nomorEPROC = "EPROC/AT/2019/I";
    var nomorPO = "PO/AT/2019/I";
    var nomorKontrakPj = "PJ/AT/2019/I";
    var nomorPenawaran = "Penawaran/AT/2019/I";
    var nomorPenunjukan = "Penunjukan/AT/2019/I";

    describe("Penagihan Area Langsa", function () {
        it(".should() - success login as EDDI.S (Manager Area Langsa)", function () {
            cy.visit(usedUrl + "/public/login");
            cy.document().then((doc) => {
                if(doc.getElementById("btnLogout"))
                    doLogout();
            });
            doLogin("EDDI.S", "123");
        });
        it(".should() - success create Disposisi", function () {
            cy.visit(usedUrl + "/public/pengadaan/anggaranmsk");
            cy.wait(5000);
            cy.contains(nomorSKKI).click();
            cy.get("input[value='RENC']").check();
            cy.get("#keterangan").type("AUTO TEST").should("have.value", "AUTO TEST");
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - success login as BIAN.W (PIC Area Langsa)", function () {
            doLogout();
            doLogin("BIAN.W", "123");
        });
        it(".should() - Success Input Nota Dinas Ijin Prinsip", function () {
            cy.visit(usedUrl + "/public/pengadaan/tbh_kontrak");
            cy.wait(5000);
            cy.contains(nomorSKKI).click();
            cy.get("a").contains("Input Kegiatan").click();
            cy.get("a").contains("PILIH PRK").click();
            cy.get("#pilihprk").select("2019.WNAD.11.002.01|AI000001|SKKI", {force:true});
            cy.wait(2000);
            cy.get("#pakai").type(1000000000).should("have.value", 1000000000);
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");

            cy.get("#dropDownKegiatan").select("EPROC", {force: true});
            cy.get("#form-EPROC input[name='no']").type(nomorNotaDinas).should("have.value", nomorNotaDinas);
            cy.get("#form-EPROC input[name='tgl']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("#form-EPROC input[name='sifat']").type("SIFAT AUTO").should("have.value", "SIFAT AUTO");
            cy.get("#form-EPROC input[name='lampiran']").type("1").should("have.value", "1");
            cy.get("#form-EPROC select[name='jenis_kontrak']").select("KONSTRUKSI", {force:true});
            cy.get("#form-EPROC input[name='perihal']").type("PERIHAL AUTO").should("have.value", "PERIHAL AUTO");
            cy.get("#METODE").select("PNL", {force: true});
            cy.get("#form-EPROC select[name='sbr_pendanaan']").select("APLN", {force:true});
            cy.fixture("image.png").as("file");
            cy.get("#form-EPROC input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                    const el = subject[0];
                    const testFile = new File([blob], 'image.png', { type: 'image/png' });
                    const dataTransfer = new DataTransfer();
                    dataTransfer.items.add(testFile);
                    el.files = dataTransfer.files
                });
            });
            cy.get("#form-EPROC select[name='jns_pekerjaan']").select("RUTIN", {force:true});
            cy.get("#form-EPROC button[type='submit']").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - success login as EDDI.S (Manager Area Langsa)", function () {
            doLogout();
            doLogin("EDDI.S", "123");
        });
        it(".should() - Success Approval Nota Dinas", function () {
            cy.visit(usedUrl + "/public/pengadaan/listnd_ma");
            cy.wait(5000);
            cy.contains(nomorNotaDinas).click();
            cy.get("a").contains("APPROVE").click();
            cy.wait(1000);
            cy.get("select[name='METODE']").select("PNL", {force:true});
            cy.get("button[type='submit']").contains("APPROVE").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - success login as BIAN.W (PIC Area Langsa)", function () {
            doLogout();
            doLogin("BIAN.W", "123");
        });
        it(".should() - Success Membuat RKS dan HPE", function () {
            cy.visit(usedUrl + "/public/pengadaan/rks");
            cy.wait(5000);
            //TODO: UNCOMMENT THIS
            cy.contains(nomorNotaDinas).click();
            //TODO:COMMENT THIS
            // cy.contains(nomorRKS).click();
            //<editor-fold desc="Input RKS">
            cy.get("a").contains("RENCANA KERJA DAN SYARAT").click();
            cy.get("#no_rks").type(nomorRKS).should("have.value", nomorRKS);
            cy.get("#tanggal_rks").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("#user_1").type("Pengadaan User 1").should("have.value", "Pengadaan User 1");
            cy.get("#user_2").type("Pengadaan User 2").should("have.value", "Pengadaan User 2");
            cy.get("#user_3").type("Manager Area").should("have.value", "Manager Area");
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                    const el = subject[0];
                    const testFile = new File([blob], 'image.png', { type: 'image/png' });
                    const dataTransfer = new DataTransfer();
                    dataTransfer.items.add(testFile);
                    el.files = dataTransfer.files
                });
            });
            cy.get("button[type='submit']").contains("SIMPAN RKS").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            //</editor-fold>

            //<editor-fold desc="Input RP HPE">
            cy.url().should("include", "public/pengadaan/rks");
            cy.get("button").contains("INPUT HPE").click();
            cy.wait(5000);
            cy.get("#modal-hpe button[type='submit']").contains("SIMPAN").click();
            // cy.get("button.swal2-confirm").contains("Ya").click();
            //</editor-fold>

            //<editor-fold desc="Input HPE">
            cy.get("a").contains("HARGA PERKIRAAN ENGINEERING").click();
            cy.fixture("image.png").as("file");
            cy.get("#modal-upload_hpe input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                    const el = subject[0];
                    const testFile = new File([blob], 'image.png', { type: 'image/png' });
                    const dataTransfer = new DataTransfer();
                    dataTransfer.items.add(testFile);
                    el.files = dataTransfer.files
                });
            });
            cy.get("#modal-upload_hpe button[type='submit']").contains("UPLOAD").click();
            // cy.get("button.swal2-confirm").contains("Ya").click();
            //</editor-fold>

            //<editor-fold desc="Tambah Lokasi">
            cy.get("a").contains("TAMBAH").click();
            cy.get("#unit").select("6112604", {force:true});
            cy.get("#modal-tambah_lokasi button[type='submit']").contains("TAMBAH").click();
            // cy.get("button.swal2-confirm").contains("Ya").click();
            //</editor-fold>

            //<editor-fold desc="Simpan Data">
            cy.get("button[type='submit']").contains("DISPOSISI KE ASRENC").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
            //</editor-fold>
        });

        it(".should() - success login as RULI (ASMAN Area Langsa)", function () {
            doLogout();
            doLogin("RULI", "123");
        });
        it(".should() - success menyetujui RKS", function () {
            cy.visit(usedUrl + "/public/pengadaan/approverksarea");
            cy.wait(5000);
            cy.contains(nomorRKS).click();
            cy.get("button[type='submit']").contains("APPROVE").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");

        });

        it(".should() - success login as RIDWAN (Pelaksana Pengadaan Area Langsa)", function () {
            doLogout();
            doLogin("RIDWAN", "123");
        });
        it(".should() - success input PR", function () {
            cy.visit(usedUrl + "/public/pengadaan/integrasi");
            cy.wait(2000);
            cy.get("#btnpredit").click();
            cy.get("input[name='nomor']").type(nomorPR).should("have.value", nomorPR);
            cy.get("#modal-nomor button").contains("PILIH").click();
            // cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });
        it(".should() - berhasil melakukan Input EPROC", function () {
            cy.wait(3000);
            cy.get("#btneprocedit").click();
            cy.get("input[name='nomor']").type(nomorEPROC).should("have.value", nomorEPROC);
            cy.get("#modal-nomor button").contains("PILIH").click();
            // cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });
        it(".should() - berhasil melakukan Input PO", function () {
            cy.wait(3000);
            cy.get("#listpo").click();
            cy.get("a i[class='fa fa-plus']").click();
            cy.get("input[name='nomor']").type(nomorPO).should("have.value", nomorPO);
            cy.get("#modal-nomor button").contains("PILIH").click();
            // cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });
        it(".should() - berhasil Input Rp Kontrak", function () {
            cy.contains("INPUT SPK/KONTRAK").click({force: true});
            cy.wait(3000);
            cy.contains(nomorNotaDinas).click();
            cy.get("#sendspk table a i.fa-cog").click();
            cy.wait(1000);
            cy.get("#formupdateprk > button").contains("SIMPAN").click();
            // cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });
        it(".should() - berhasil Input Perjanjian/SPK (Upload Perjanjian)", function () {
            cy.contains("PERJANJIAN / SPK").click();
            cy.contains("UPLOAD PERJANJIAN").click({force: true});
            cy.get("input[name='lokasi']").type("Bandung").should("have.value", "Bandung");
            cy.get("input[name='nomor']").type(nomorKontrakPj).should("have.value", nomorKontrakPj);
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                    const el = subject[0];
                    const testFile = new File([blob], 'image.png', { type: 'image/png' });
                    const dataTransfer = new DataTransfer();
                    dataTransfer.items.add(testFile);
                    el.files = dataTransfer.files
                });
            });
            cy.get("input[name='no_penawaran']").type(nomorPenawaran).should("have.value", nomorPenawaran);
            cy.get("input[name='no_skma']").type(nomorPenunjukan).should("have.value", nomorPenunjukan);
            cy.get("input[name='tgl0']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='tgl1']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='tgl2']").click();
            cy.get("div[class='datepicker-days'] th.next").click(); //next month
            cy.get("tr td.day").contains("25").click(); //tgl 25 next month
            cy.get("input[name='nm_pihakii']").type("RIDWAN").should("have.value", "RIDWAN");
            cy.contains("SIMPAN PERJANJIAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
        });
        it(".should() - berhasil kirim progress ke asman)", function () {
            cy.get("#jenis_pembayaran").select("TERMIN", {force:true});
            cy.get("input[name='jumlahtermin']").type("1").should("have.value", "1");
            cy.contains("Jika tombol tidak tampil").click();
            cy.get("input[name='laporankerja[1]']").check();
            cy.get("input[name='raportvendor[1]']").check();
            cy.contains("KIRIM PROGRESS KE ASMAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - success login as Zulfan ", function () {
            doLogout();
            doLogin("ZULFAN", "123")
        });
        it(".should() - berhasil membuat laporan", function () {
            cy.visit(usedUrl + "/public/pengadaan/laporankerja");
            cy.wait(5000);
            cy.get("a").contains("detail").click();
            cy.wait(3000);
            cy.get("#raport-table a > i.fa-pencil").click();
            cy.get("#R_mutu_3").check();
            cy.get("#nil_mutu_4").clear().type("95").should("have.value", "95");
            cy.get("textarea[name='keterangan']").type("Keterangan Laporan").should("have.value", "Keterangan Laporan");
            cy.get("#button").click();
            // cy.get("button.swal2-confirm").contains("Ya").click();
        });

        it(".should() - success login as BIAN.W", function () {
            doLogout();
            doLogin("BIAN.W", "123");
        });
        it(".should() - berhasil menambah raport", function () {
            cy.visit(usedUrl + "/public/penilaian");
            cy.wait(5000);
            cy.get("#raport-table a").contains("detail").click();
            cy.wait(3000);
            cy.get("a").contains("EDIT").click();
            cy.wait(2000);
            cy.get("#R_mutu_4").check();
            cy.get("#R_waktu_4").check();
            cy.get("#R_lingkungan_4").check();
            // cy.get("#R_lingkungan").check(); //NOTE: di depan salah penamaan id, salah tutup kutip
            cy.get("#R_ketaatan_dokumen_4").check();
            cy.get("#R_komunikasi_4").check();
            cy.get("#button").click();
        });
    });
});