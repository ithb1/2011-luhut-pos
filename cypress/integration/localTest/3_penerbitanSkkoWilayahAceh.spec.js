context('Assertions',function () {

    function doLogin(username, password) {
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.get("#btnLogout").click({force: true});
    }

    beforeEach(function () {
        Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_session");
        cy.viewport(1366,768);
    });

    before(function () {
        cy.clearCookies();
    });

    var nomorPRK = "2019.WNAD.11.002";
    var nomorSKKO = "";
    var nomorPRKAO = "2019.PRKAO.AUTO.I";
    var empatjuta = 4000000;
    var duajuta = 2000000;
    var sepuluhjuta = empatjuta * 2 + duajuta;
    var kepegawaian = "025";
    var manfaatpegawai = "5202502";

    describe("Buat 1 PRK, 1 SKKI dengan 1 PRK, <=10M Area Langsa", function () {
        it(".should() - success login as OCTIDWI (Anggaran)", function () {
            cy.visit("http://localhost/sipatpln/public/login");
            cy.document().then((doc) => {
                if(doc.getElementById("btnLogout"))
                    doLogout();
            });
            doLogin("OCTIDWI", "123");
        });
        it(".should() - success create PRK", function () {
            cy.get("li span").contains("ANGGARAN").click();
            cy.contains("PRK").click({force: true});
            cy.get("a").contains("AO NON PEMELIHARAAN").click({force: true});
            cy.url().should('include', '/anggaran/prkao/ebudget');
            cy.wait(2000);
            cy.get("#prkao-table > tbody > tr:nth-child(9) > td:nth-child(6) > a").click();
            cy.wait(2000);
            cy.contains("TAMBAH PRK").click({force: true});
            cy.get("input[name='prk']").type(nomorPRKAO).should("have.value", nomorPRKAO);
            cy.get("textarea[name='detail']").type("DETAIL PRK AO AUTO").should("have.value", "DETAIL PRK AO AUTO");
            cy.get("textarea[name='keterangan']").type("KETERANGAN PRK AO AUTO").should("have.value", "KETERANGAN PRK AO AUTO");
            cy.get("input[name='volume']").type(1000).should("have.value", "1000");
            cy.get("input[name='satuan']").type("MV").should("have.value", "MV");
            cy.get("input[name='detail_mat']").type("Material Auto").should("have.value", "Material Auto");
            cy.get("input[name='rpmat']").type(empatjuta).should("have.value", empatjuta);
            cy.get("input[name='rpgudang']").type(empatjuta).should("have.value", empatjuta);
            cy.get("input[name='detail_jasa']").type("Jasa Auto").should("have.value", "Jasa Auto");
            cy.get("input[name='rpjasa']").type(duajuta).should("have.value", duajuta);
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
        });
        it(".should() - success create Kel Prk", function () {
            cy.get("li span").contains("INPUT AI / AO").click({force: true});
            cy.contains("OPERASI").click({force: true});
            cy.contains("PILIH PRK").click();
            cy.get("#unitprk").select("6101", {force: true});
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("#prk").select(nomorPRKAO, {force: true});
            cy.get("#pakai").type(sepuluhjuta).should("have.value", sepuluhjuta);
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
        });
        it(".should() - success create SKKO", function () {
            cy.get("button span").contains("BUAT SKKO").click();
            cy.get("select[name='f_kd_ao']").select("/R/AO-KIT/");
            cy.get("textarea[name='uraian_pekerjaan']").type("URAIAN AUTO TEST").should("have.value", "URAIAN AUTO TEST");
            cy.get("#kdfungsi").select(kepegawaian);
            cy.wait(3000);
            cy.get("#kdjenis").select(manfaatpegawai);
            cy.wait(2000); //wait for program to get value
            cy.get("input[name='notadinas']").type("AT/O/2019").should("have.value", "AT/O/2019");
            cy.get("input[name='tgl_notadinas']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                    const el = subject[0];
                    const testFile = new File([blob], 'image.png', { type: 'image/png' });
                    const dataTransfer = new DataTransfer();
                    dataTransfer.items.add(testFile);
                    el.files = dataTransfer.files
                });
            });
            cy.get("select[name='nd']").select("PERENCANAAN");
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
            cy.wait(3000); //wait for table
            cy.document().then((doc) => {
                if(doc.querySelector("#penerbitan-ai-table > tbody > tr:nth-child(1) > td:nth-child(3) > a > strong"))
                    nomorSKKO = doc.querySelector("#penerbitan-ai-table > tbody > tr:nth-child(1) > td:nth-child(3) > a > strong").innerText;
            });
        });
        it(".should() - success approve by Anggaran", function () {
            cy.visit("http://localhost/sipatpln/public/anggaran/approve");
            if(nomorSKKO == "") nomorSKKO = "01/R/AO-KIT/W.ACEH/2019";
            cy.wait(2000);
            cy.contains(nomorSKKO).click();
            // cy.get("#approve-table > tbody > tr:nth-child(1) > td:nth-child(4) > a").click(); //selector didapat dari copy selector dectools chrome, ini harus berhasil, karena memang harus selalu item pertama (tr(1))
            cy.url().should("include", "/anggaran/approve/detail?dt0=");
            cy.contains("APPROVE ANGGARAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
        });
        it(".should() - success login as DEVARIZA (Keuangan)", function () {
            doLogout();
            cy.wait(2000);
            doLogin("DEVARIZA", "123");
        });
        it(".should() - success approve by Keuangan", function () {
            cy.visit("http://localhost/sipatpln/public/anggaran/approve");
            if(nomorSKKO == "") nomorSKKO = "01/R/AO-KIT/W.ACEH/2019";
            cy.wait(2000);
            cy.contains(nomorSKKO).click();
            cy.url().should("include", "/anggaran/approve/detail?dt0=");
            cy.contains("APPROVE ANGGARAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
        });

        it(".should() - success login as OCTIDWI (Anggaran)", function () {
            doLogout();
            doLogin("OCTIDWI", "123");
        });
        it(".should() - success Kirim Anggaran Ke Area/Bidang", function () {
            cy.visit("http://localhost/sipatpln/public/anggaran/penerbitan/skko");
            if(nomorSKKO == "") nomorSKKO = "01/R/AO-KIT/W.ACEH/2019";
            cy.wait(2000);
            cy.contains(nomorSKKO).click();
            // cy.get("#penerbitan-ai-table > tbody > tr:nth-child(1) > td:nth-child(3) > a").click();
            cy.url().should("include", "/anggaran/penerbitan/detail?dt0=");
            cy.contains("KIRIM ANGGARAN").click();
            cy.get("button").contains("KIRIM").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
        });
    });
});