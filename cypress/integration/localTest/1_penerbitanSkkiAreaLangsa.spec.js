context('Assertions',function () {

    var geminiUrl = "http://192.168.0.2/sipat";
    var geminiTestingUrl = "http://192.168.0.2/sipat-testing";
    var taurusTestingUrl = "http://103.28.22.109/sipat-testing";
    var taurusUrl = "http://103.28.22.109/sipat";
    var localUrl = "http://localhost/sipatpln";
    var usedUrl = "";
    var GEMINI = "GEMINI";
    var GEMINITESTING = "GEMINITESTING";
    var TAURUSTESTING = "TAURUSTESTING";
    var TAURUS = "TAURUS";
    var LOCAL = "LOCAL";
    var useConfig = LOCAL;

    function doLogin(username, password) {
        cy.wait(3000);
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.get("#btnLogout").click({force: true});
    }

    beforeEach(function () {
        // cy.visit(usedUrl + "/public/login")
        if(useConfig == TAURUSTESTING || useConfig == GEMINITESTING) {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session");
        }
        else if(useConfig == TAURUS){
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_git_session");
        }
        else {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_session");
        }
        // Cypress.Cookies.preserveOnce();
        cy.viewport(1366,768);
    });

    before(function () {
        cy.clearCookies();
        switch (useConfig) {
            case TAURUSTESTING:
                usedUrl = taurusTestingUrl;
                break;
            case TAURUS:
                usedUrl = taurusUrl;
                break;
            case GEMINI:
            default:
                usedUrl = geminiUrl;
                break;
            case GEMINITESTING:
                usedUrl = geminiTestingUrl;
                break;
            case LOCAL:
                usedUrl = localUrl;
                break;
        }
    });

    var nomorPRK = "2019.WNAD.11.002";
    var nomorSKKI = "";

    describe("Buat 1 PRK, 1 SKKI dengan 1 PRK, <=10M Area Langsa", function () {
        it(".should() - success login as OCTIDWI (Anggaran)", function () {
            cy.visit(usedUrl + "/public/login");
            cy.document().then((doc) => {
                if(doc.getElementById("btnLogout"))
                    doLogout();
            });
            doLogin("OCTIDWI", "123");
        });
        it(".should() - success create PRK", function () {
            /*cy.get("li span").contains("ANGGARAN").click();
            cy.contains("PRK").click({force: true});
            cy.get("a").contains("AI").click({force: true});
            cy.url().should('include', '/anggaran/prkai/ebudget');*/
            cy.visit(usedUrl + "/public/anggaran/prkai/ebudget");
            cy.wait(2000);
            cy.get("#prkaiebudget-table_paginate li.paginate_button > a").contains("2").click();
            cy.wait(3000);
            cy.contains(nomorPRK).click({force: true});
            cy.wait(2000);
            cy.contains("TAMBAH PRK").click({force: true});
            cy.get("#unit").select("6111", {force: true});
            cy.get("input[name='jumlah']").type(1000000000).should("have.value", 1000000000);
            cy.get("input[name='disburse']").type(1000000000).should("have.value", 1000000000);
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
        });
        it(".should() - success create Kel Prk", function () {
            /*cy.get("li span").contains("INPUT AI / AO").click({force: true});
            cy.contains("INVESTASI").click({force: true});*/
            cy.visit(usedUrl + "/public/anggaran/introai");
            cy.contains("PILIH PRK").click();
            cy.get("#unitprk").select("6111", {force: true});
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("#prk").select("2019.WNAD.11.002.01", {force: true});
            cy.get("#rppakai").type(1000000000).should("have.value", 1000000000);
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
        });
        it(".should() - success create SKKI", function () {
            cy.get("button span").contains("BUAT SKKI").click();
            cy.get("select[name='f_kd_ao']").select("PERENCANAAN");
            cy.get("textarea[name='uraian_pekerjaan']").type("AUTO TEST").should("have.value", "AUTO TEST");
            cy.get("#fungsi").select("01");
            cy.wait(2000); //wait for program to get value
            cy.get("input[name='notadinas']").type("AT/I/2019").should("have.value", "AT/I/2019");
            cy.get("input[name='tgl_notadinas']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                    const el = subject[0];
                    const testFile = new File([blob], 'image.png', { type: 'image/png' });
                    const dataTransfer = new DataTransfer();
                    dataTransfer.items.add(testFile);
                    el.files = dataTransfer.files
                });
            });
            cy.get("select[name='nd']").select("PERENCANAAN");
            cy.get("button[type='submit']").contains("SIMPAN DATA").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
            cy.wait(3000); //wait for table
            cy.document().then((doc) => {
                if(doc.querySelector("#penerbitan-ai-table > tbody > tr:nth-child(1) > td:nth-child(3) > a > strong"))
                    nomorSKKI = doc.querySelector("#penerbitan-ai-table > tbody > tr:nth-child(1) > td:nth-child(3) > a > strong").innerText;
            });
        });
        it(".should() - success approve by Anggaran", function () {
            cy.visit(usedUrl + "/public/anggaran/approve");
            if(nomorSKKI == "") nomorSKKI = "01/R/AI-REN/W.ACEH/2019-LSA";
            cy.wait(2000);
            cy.contains(nomorSKKI).click();
            // cy.get("#approve-table > tbody > tr:nth-child(1) > td:nth-child(4) > a").click(); //selector didapat dari copy selector dectools chrome, ini harus berhasil, karena memang harus selalu item pertama (tr(1))
            cy.url().should("include", "/anggaran/approve/detail?dt0=");
            cy.contains("APPROVE ANGGARAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
        });
        it(".should() - success login as DEVARIZA (Keuangan)", function () {
            doLogout();
            cy.wait(2000);
            doLogin("DEVARIZA", "123");
        });
        it(".should() - success approve by Keuangan", function () {
            cy.visit(usedUrl + "/public/anggaran/approve");
            if(nomorSKKI == "") nomorSKKI = "01/R/AI-REN/W.ACEH/2019-LSA";
            cy.wait(2000);
            cy.contains(nomorSKKI).click();
            cy.url().should("include", "/anggaran/approve/detail?dt0=");
            cy.contains("APPROVE ANGGARAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
        });

        it(".should() - success login as OCTIDWI (Anggaran)", function () {
            doLogout();
            doLogin("OCTIDWI", "123");
        });
        it(".should() - success Kirim Anggaran Ke Area/Bidang", function () {
            cy.visit(usedUrl + "/public/anggaran/penerbitan/skki");
            if(nomorSKKI == "") nomorSKKI = "01/R/AI-REN/W.ACEH/2019-LSA";
            cy.wait(2000);
            cy.contains(nomorSKKI).click();
            // cy.get("#penerbitan-ai-table > tbody > tr:nth-child(1) > td:nth-child(3) > a").click();
            cy.url().should("include", "/anggaran/penerbitan/detail?dt0=");
            cy.contains("KIRIM ANGGARAN").click();
            cy.get("select[name='ket_ai']").select("MURNI");
            cy.get("button").contains("KIRIM").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil").click();
        });
    });
});