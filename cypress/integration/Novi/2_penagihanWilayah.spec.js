context('Assertions',function () {

    var geminiUrl = "http://192.168.0.2/sipat";
    var geminiTestingUrl = "http://192.168.0.2/sipat-testing";
    var taurusUrl = "http://103.28.22.109/sipat-testing";
    var usedUrl = "";
    var GEMINI = "GEMINI";
    var GEMINITESTING = "GEMINITESTING";
    var TAURUS = "TAURUS";
    var useConfig = TAURUS;

    function doLogin(username, password) {
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.get("#btnLogout").click({force: true});
    }

    beforeEach(function () {
        // cy.visit("http://localhost/sipatpln/public/login")
        if(useConfig == TAURUS || useConfig == GEMINITESTING) {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session");
        }
        else {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_session");
        }
        // Cypress.Cookies.preserveOnce();
        cy.viewport(1366,768);
        

    });

    before(function () {
        cy.clearCookies();
        switch (useConfig) {
            case TAURUS:
                usedUrl = taurusUrl;
                break;
            case GEMINI:
            default:
                usedUrl = geminiUrl;
                break;
            case GEMINITESTING:
                usedUrl = geminiTestingUrl;
                break;
        }
    });

    describe("Penagihan Wilayah", function () {
        it(".should() - success login as JAKA.SUMANTRI (PENGADAAN)", function () {
            cy.visit(usedUrl + "/public/login", {timeout : 120000});
            cy.document().then((doc) => {
                if(doc.getElementById("btnLogout"))
                    doLogout();
            });
            doLogin("JAKA.SUMANTRI", "123");

        });
        it(".should() - success create NOTA DINAS", function () {
            cy.get("li span").contains("MANAGER BIDANG").click();
            cy.contains("PENGADAAN").click();
            cy.contains("DISPOSISI").click();
            cy.url().should('include', '/pengadaan/anggaranmsk');
            cy.contains("01R/AI-REN/W.ACEH/2019").click();
            cy.contains("TAMBAH NOTA DINAS").click();
            cy.contains("PILIH PRK").click();
            cy.get("#pilihprk").select("2019.WNAD.9.001.02|AI000001|SKKI"); 
            cy.get("input[name='pakai']").type(1).should("have.value", 1);
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.contains("Berhasil");
            // cy.screenshot();
        });

        it(".should() - success create KONTRAK BIASA/EPROC", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.contains("KONTRAK BIASA / EPROC").click();
            cy.url().should('include', '/pengadaan/disposisidana_mb');
            cy.get("textarea[name='nama']").type("PEKERJAAN PEMBANGUNAN").should("have.value", "PEKERJAAN PEMBANGUNAN");
            cy.get("input[name='no_ijinprinsip']").type("0490/LOG.00.01/W.ACEH/2017").should("have.value", "0490/LOG.00.01/W.ACEH/2017");
            cy.get("input[name='tgl_ijinprinsip']").type("20190226").should("have.value", "20190226");
            cy.get("select[name='jenis_kontrak']").select("JASA", {force: true});
            cy.get("select[name='metode']").select("Penunjukan Langsung");
            cy.get("select[name='sbr_pendanaan']").select("APLN");
            cy.get("select[name='jns_pekerjaan']").select("RUTIN");

            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });

            cy.wait(3000); //tunggu masukan data ke select
            cy.get("button[type='submit']").contains("SIMPAN").click();
            //cy.wait(3000); //tunggu masukan data ke select
            cy.contains("Berhasil");
        });

        it(".should() - logout dan login as ZUBIR", function () {
            cy.get("#userDropdown").click({force:true});
            cy.get("#btnLogout").click();
            const stub = cy.stub();
            stub.onFirstCall().returns(true);
            cy.on('window:confirm', stub);
            
            cy.wait(1000);
            cy.get("#emailaddress")
                .type("ZUBIR")
                .should("have.value", "ZUBIR");
            cy.get("#password")
                .type("123")
                .should("have.value", "123");
            cy.contains("Sign In").click();
            cy.url().should("include", "public/");
        });
   
        it(".should() - success create UPLOAD RKS", function () {
            cy.get("li span").contains("RENDAN WIL").click();
            cy.contains("INPUT RKS").click();
            cy.contains("178/DAN.00.01/MANTEK/2018").click();

            cy.get("a").contains("RENCANA KERJA DAN SYARAT").click();
            cy.contains("UPLOAD RKS").click();
            cy.get("input[name='NOMOR']").type("1").should("have.value", "1");
            cy.get("input[name='TGL']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });
            cy.get("input[name='PEL_PENG1']").type("OCTI DWI").should("have.value", "OCTI DWI");
            cy.get("input[name='PEL_PENG2']").type("EDDI SAPUTRA").should("have.value", "EDDI SAPUTRA");
            cy.get("input[name='MENYETUJUI']").type("DEVA RIZA").should("have.value", "DEVA RIZA");
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.contains("Berhasil");
            // cy.screenshot();

            cy.get("a").contains("HARGA PERKIRAAN ENGINEERING").click();
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });
            
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN").click({force:true});
            cy.contains("Berhasil");
            // cy.screenshot();
        });

        it(".should() - logout dan login as TAUFIK.HIDAYAT2", function () {
            cy.get("#userDropdown").click({force:true});
            cy.get("#btnLogout").click();
            const stub = cy.stub();
            stub.onFirstCall().returns(true);
            cy.on('window:confirm', stub);
            
            cy.wait(1000);
            cy.get("#emailaddress")
                .type("TAUFIK.HIDAYAT2")
                .should("have.value", "TAUFIK.HIDAYAT2");
            cy.get("#password")
                .type("123")
                .should("have.value", "123");
            cy.contains("Sign In").click();
            cy.url().should("include", "public/");
        });
   
        it(".should() - success create APPROVAL RKS", function () {
            cy.get("li span").contains("MANAGER BIDANG").click();
            cy.contains("PENGADAAN").click();
            cy.contains("APPROVAL").click();
            cy.contains("0062.PJ/DAN.02.01/AI/W.ACEH/2017").click();
        });

        it(".should() - logout dan login as AGUS.SALIM", function () {
            cy.get("#userDropdown").click({force:true});
            cy.get("#btnLogout").click();
            const stub = cy.stub();
            stub.onFirstCall().returns(true);
            cy.on('window:confirm', stub);
            
            cy.wait(1000);
            cy.get("#emailaddress")
                .type("AGUS.SALIM")
                .should("have.value", "AGUS.SALIM");
            cy.get("#password")
                .type("123")
                .should("have.value", "123");
            cy.contains("Sign In").click();
            cy.url().should("include", "public/");
        });
   
        it(".should() - success create INPUT SPK/KONTRAK", function () {
            cy.get("li span").contains("LAKDAN WIL").click();
            cy.contains("INPUT SPK/KONTRAK").click();
            cy.contains("00021/DAN.01.03/K.UPPK/2019").click();
            cy.contains("PERJANJIAN / SPK").click();
            cy.contains("SIMPAN").click({force:true});
            cy.wait(3000);
            cy.get("#sendspk > div:nth-child(12) > div > div > div.box-body > div > div > ul > li:nth-child(2) > label > a.btn.btn-raised.btn-info.btn-icon.icon-left").click();        
            cy.contains(" UPLOAD PERJANJIAN ").click({force:true});

            cy.get("input[name='lokasi']").type("Bandung").should("have.value", "Bandung");
            cy.get("select[name='vendor_id']").select("PT. VEGANA ELEKTRIK UTAMA", {force:true});
            cy.get("input[name='nomor']").type("123").should("have.value", "123");
                      
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                        });
                });

            cy.get("input[name='no_penawaran']").type("123").should("have.value", "123");
            cy.get("input[name='no_skma']").type("321").should("have.value", "321");
            cy.get("input[name='tgl0']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='tgl1']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='tgl2']").type("06/03/2019").should("have.value", "06/03/2019");
            cy.get("input[name='nm_pihakii']").type("DEVA RIZA").should("have.value", "DEVA RIZA");

            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN PERJANJIAN").click({force:true});
            cy.contains("Berhasil");
            // cy.screenshot();
            
            cy.get("button[type='submit'").contains("KIRIM KE USER").click({force:true});
            cy.contains("Berhasil");   
        });

        it(".should() - logout dan login as ZULFAN", function () {
            cy.get("#userDropdown").click({force:true});
            cy.get("#btnLogout").click();
            const stub = cy.stub();
            stub.onFirstCall().returns(true);
            cy.on('window:confirm', stub);
            
            cy.wait(1000);
            cy.get("#emailaddress")
                .type("ZULFAN")
                .should("have.value", "ZULFAN");
            cy.get("#password")
                .type("123")
                .should("have.value", "123");
            cy.contains("Sign In").click();
            cy.url().should("include", "public/");
        });

        it(".should() - success create INPUT LAPORAN PEKERJAAN", function () {
            cy.get("li span").contains("LAPORAN PEKERJAAN").click();
            cy.wait(1000);
            cy.get("a").contains(" detail").click({force:true});
            cy.contains("TAMBAH LAPORAN").click();
            cy.get("select[name='var_jml_lapor']").select("1");
            cy.get("#R_mutu_3").click();
            cy.get("textarea[name='keterangan']").type("LAPORAN KEMAJUAN ANGGARAN").should("have.value", "LAPORAN KEMAJUAN ANGGARAN");
            cy.wait(3000); //wait all xhr
            cy.get("input[type='submit'").contains("SUBMIT LAP PEKERJAAN").click({force:true});
            cy.contains("Berhasil");
            // cy.screenshot();
        });

        it(".should() - logout dan login as TAUFIK.HIDAYAT2", function () {
            cy.get("#userDropdown").click({force:true});
            cy.get("#btnLogout").click();
            const stub = cy.stub();
            stub.onFirstCall().returns(true);
            cy.on('window:confirm', stub);

            cy.wait(1000);
            cy.get("#emailaddress")
                .type("TAUFIK.HIDAYAT2")
                .should("have.value", "TAUFIK.HIDAYAT2");
            cy.get("#password")
                .type("123")
                .should("have.value", "123");
            cy.contains("Sign In").click();
            cy.url().should("include", "public/");
        });

        it(".should() - success INPUT PENAGIHAN", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("MANAGER BIDANG").click();
            cy.contains("PENAGIHAN").click();
            cy.contains("KONTRAK").click();
            cy.url().should('include', '/public/penagihan');
            cy.contains("INPUT TAGIHAN").click();
            cy.get("#nokontrak").select("123/PJ/RENC/2019-TERPUSAT - PERENCANAAN - 01", {force:true});
            cy.get("select[name='termin']").select("2", {force:true});
            cy.wait(1000);
            cy.get("#listPrk1").type("10").should("have.value", 10); //masih error (lagi di perbaiki mas fauzi)
            cy.get("#listPrk2").type("20").should("have.value", 20);
            cy.get("textarea[name='ket_tagihan']").type("TES").should("have.value", "TES");
            cy.get("textarea[name='nomiro']").type("T-001").should("have.value", "T-001");
            cy.get("input[name='nopo']").type("1111").should("have.value", "1111");
            cy.get("input[name='nose']").type("0001").should("have.value", "0001");
            cy.get("button[type='submit'").contains("SIMPAN").click(); //masih error karena RP Kontrak, Jumlah Bayar, Sisa Bayar 0 semua (total bayar melebihi sisa bayar)
            cy.contains("Berhasil");
        });

        it(".should() - logout dan login as FACHRUL", function () {
            cy.get("#userDropdown").click({force:true});
            cy.get("#btnLogout").click();
            const stub = cy.stub();
            stub.onFirstCall().returns(true);
            cy.on('window:confirm', stub);

            cy.wait(1000);
            cy.get("#emailaddress")
                .type("FACHRUL")
                .should("have.value", "FACHRUL");
            cy.get("#password")
                .type("123")
                .should("have.value", "123");
            cy.contains("Sign In").click();
            cy.url().should("include", "public/");
        });

        it(".should() - success INPUT AGENDA BAYAR", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("KEUANGAN").click();
            cy.contains("AGENDA TAGIHAN").click();
            cy.url().should('include', '/public/keuangan');
            cy.contains("ADD AGENDA").click();
            cy.get("#unit").select("Kantor Wilayah", {force:true});
            cy.get("select[name='pilihNoreg']").select("6100032421", {force:true});
            cy.wait(5000); //tunggu masukan data ke select
            cy.get("button[type='submit'").contains("    SIMPAN").click({force:true}); //masih error karena RP Kontrak, Jumlah Bayar, Sisa Bayar 0 semua (total bayar melebihi sisa bayar)
            cy.contains("Berhasil");
        });



         it(".should() - logout dan login as OCTIDWI", function () {
            cy.get("#userDropdown").click({force:true});
            cy.get("#btnLogout").click();
            const stub = cy.stub();
            stub.onFirstCall().returns(true);
            cy.on('window:confirm', stub);

            cy.wait(1000);
            cy.get("#emailaddress")
                .type("OCTIDWI")
                .should("have.value", "OCTIDWI");
            cy.get("#password")
                .type("123")
                .should("have.value", "123");
            cy.contains("Sign In").click();
            cy.url().should("include", "public/");
        });

        it(".should() - success VERIFIKASI TAGIHAN", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("KEUANGAN").click();
            cy.contains("VERIFIKASI").click();
            cy.contains("TAG KONTRAK").click();
            cy.url().should('include', '/public/keuangan/');
            cy.contains("051.PJ/DAN.02.01/AI/W.ACEH/2019").click();
            cy.wait(3000);
            cy.get("input[name='jml_tagihan']").should("have.value", 47500000);
            cy.get("select[name='jnsbyr']").select("5 %");
            cy.get("input[name='jml_denda']").type("1000").should("have.value", 1000);
            cy.get("input[name='tglbayar']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='nogiro']").type("123").should("have.value", "123");
            cy.get("select[name='tagbulan']").select("JANUARI");
            cy.get("button[type='submit'").contains("APPROVE TAGIHAN").click();
            cy.contains("Berhasil");
        });
        it(".should() - logout dan login as SISKA", function () {
            cy.get("#userDropdown").click({force:true});
            cy.get("#btnLogout").click();
            const stub = cy.stub();
            stub.onFirstCall().returns(true);
            cy.on('window:confirm', stub);

            cy.wait(1000);
            cy.get("#emailaddress")
                .type("SISKA")
                .should("have.value", "SISKA");
            cy.get("#password")
                .type("123")
                .should("have.value", "123");
            cy.contains("Sign In").click();
            cy.url().should("include", "public/");
        });

        it(".should() - success VERIFIKASI PAJAK", function () {

            cy.get("li span").contains("KEUANGAN").click();
            cy.contains("VERIFIKASI").click({force:true});
            cy.contains("PAJAK").click({force:true});
            //cy.visit("http://103.28.22.109/sipat-testing/public/keuangan/pajak-pembayaran");
            cy.wait(3000);
        });
            /*


*/
    });

});