context('Assertions',function () {

    var geminiUrl = "http://192.168.0.2/sipat";
    var geminiTestingUrl = "http://192.168.0.2/sipat-testing";
    var taurusTestingUrl = "http://103.28.22.109/sipat-testing";
    var taurusNoviUrl = "http://103.28.22.109/sipat-novi";
    var taurusUrl = "http://103.28.22.109/sipat";
    var localUrl = "http://localhost/sipatpln";
    var usedUrl = "";
    var GEMINI = "GEMINI";
    var GEMINITESTING = "GEMINITESTING";
    var TAURUSTESTING = "TAURUSTESTING";
    var TAURUS = "TAURUS";
    var TAURUSNOVI = "TAURUSNOVI";
    var LOCAL = "LOCAL";
    var useConfig = GEMINI;

    function doLogin(username, password) {
        cy.wait(3000);
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.get("#btnLogout").click({force: true});
    }

    beforeEach(function () {
        // cy.visit("http://localhost/sipatpln/public/login")
        if(useConfig == TAURUSTESTING || useConfig == GEMINITESTING) {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session");
        }
        else if(useConfig == TAURUS || useConfig == TAURUSNOVI){
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_git_session");
        }
        else {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_session");
        }
        // Cypress.Cookies.preserveOnce();
        cy.viewport(1366,768);
    });

    before(function () {
        cy.clearCookies();
        switch (useConfig) {
            case TAURUSTESTING:
                usedUrl = taurusTestingUrl;
                break;
            case TAURUS:
                usedUrl = taurusUrl;
                break;
            case TAURUSNOVI:
                usedUrl = taurusNoviUrl;
                break;
            case GEMINI:
            default:
                usedUrl = geminiUrl;
                break;
            case GEMINITESTING:
                usedUrl = geminiTestingUrl;
                break;
            case LOCAL:
                usedUrl = localUrl;
                break;
        }
    });

    var nomorPRK = "2019.WNAD.6.002";

    describe("Buat 1 PRK, 1 SKKI dengan Multi PRK, 100JT", function () {
        it(".should() - success login as OCTIDWI (Anggaran)", function () {
            cy.visit(usedUrl + "/public/login");
            cy.document().then((doc) => {
                if(doc.getElementById("btnLogout"))
                    doLogout();
            });
            doLogin("OCTIDWI", "123");
        });

        /*for(var i=1; i<=1; i++)
        {}*/

        it(".should() - success create PRK", function () {
            cy.get("li span").contains("ANGGARAN").click({force: true});
            cy.contains("PRK").click({force: true});
            cy.contains("AI").click({force: true});
            cy.url().should('include', '/anggaran/prkai/ebudget');
            cy.wait(3000); //wait all xhr
            cy.contains(nomorPRK).click();
            cy.wait(3000); //wait all xhr
            cy.contains("TAMBAH PRK").click();
            cy.wait(3000); //wait all xhr
            cy.get("#unit").select("6101", {force: true});
            cy.get("#det_uraian").clear();
            cy.get("textarea[name='det_uraian']").type("PEKERJAAN PEMBANGUNAN PERTAMA").should("have.value", "PEKERJAAN PEMBANGUNAN PERTAMA");
            cy.get("input[name='jumlah']").type(1250000000).should("have.value", 1250000000);
            cy.get("input[name='disburse']").type(1250000000).should("have.value", 1250000000);
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN").click();
			cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
            // cy.screenshot();

            cy.contains("TAMBAH PRK").click();
            cy.get("#unit").select("6101", {force: true});
            cy.get("#det_uraian").clear();
            cy.get("textarea[name='det_uraian']").type("PEKERJAAN PEMBANGUNAN KEDUA").should("have.value", "PEKERJAAN PEMBANGUNAN KEDUA");
            cy.get("input[name='jumlah']").type(1700000000).should("have.value", 1700000000);
            cy.get("input[name='disburse']").type(1700000000).should("have.value", 1700000000);
            cy.wait(3000); //wait all xhr
			cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
			cy.contains("Berhasil");
            // cy.screenshot();
        });
        
        it(".should() - success create Kel Prk", function () {

            /*cy.get("li span").contains("ANGGARAN").click();
            cy.contains("INPUT AI / AO").click({force: true});
            cy.contains("INVESTASI").click({force: true});*/
            cy.visit(usedUrl + "/public/anggaran/introai");
            cy.wait(3000);
            cy.contains("PILIH PRK").click();
            cy.get("#unitprk").select("6101");
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("#prk").select("2019.WNAD.6.002.08", {force: true});
            cy.get("#rppakai").type(100000000).should("have.value", 100000000);
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("button[type='submit']").contains("SIMPAN").click();
			cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");

            cy.contains("PILIH PRK").click();
            cy.get("#unitprk").select("6101");
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("#prk").select("2019.WNAD.6.002.09", {force: true});
            cy.get("#rppakai").type(150000000).should("have.value", 150000000);
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
			cy.contains("Berhasil");
        });


        it(".should() - success create SKKI", function () {
            cy.get("button span").contains("BUAT SKKI").click();
            cy.get("select[name='f_kd_ao']").select("PERENCANAAN");
            cy.get("textarea[name='uraian_pekerjaan']").type("ANGGARAN INVESTASI JASA (NON MDU) LISDES 2019").should("have.value", "ANGGARAN INVESTASI JASA (NON MDU) LISDES 2019");
            cy.wait(2000); //wait for program to get value
            cy.get("#fungsi").select("Distribusi");
             cy.get("#program").select("Lisdes");
            cy.wait(2000); //wait for program to get value
            cy.get("input[name='notadinas']").type("0/REN.01.03/MANREN/2019").should("have.value", "0/REN.01.03/MANREN/2019");
            cy.get("input[name='tgl_notadinas']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("select[name='nd']").select("PERENCANAAN");

            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });

            cy.wait(2000); //wait for program to get value
            cy.get("button[type='submit']").contains("SIMPAN DATA").click();
			cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - success approval 1 SKKI", function () {

            cy.wait(3000); //tunggu masukan data ke select
            cy.visit(usedUrl + "/public/anggaran/approve");
            cy.wait(3000);
            cy.get("#approve-table > tbody > tr:nth-child(1) > td:nth-child(4) > a", {force: true}).click();
            cy.wait(2000); //wait for program to get value
            cy.get("button[type='submit']").contains("APPROVE ANGGARAN").click();
			cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - logout dan login as DEVARIZA", function () {
            doLogout();
            doLogin("DEVARIZA", "123");
        });

        it(".should() - success approval 2 SKKI", function () {
            cy.wait(3000); //tunggu masukan dataOC ke select
            cy.get("li span").contains("ANGGARAN").click();
            cy.contains("APPROVAL ANGG").click();
            cy.get("#approve-table > tbody > tr > td:nth-child(4) > a > strong", {force:true}).click();
            cy.contains("APPROVE ANGGARAN").click();
			cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        /*
        it(".should() - logout dan login as JEFRI.ROSIADI", function () {
            cy.get("#userDropdown").click({force:true});
            cy.get("#btnLogout").click();
            const stub = cy.stub();
            stub.onFirstCall().returns(true);
            cy.on('window:confirm', stub);
            
            cy.wait(1000);
            cy.get("#emailaddress")
                .type("JEFRI.ROSIADI")
                .should("have.value", "JEFRI.ROSIADI");
            cy.get("#password")
                .type("123")
                .should("have.value", "123");
            cy.contains("Sign In").click();
            cy.url().should("include", "public/");
        });
         it(".should() - success approval 3 SKKI GM", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("GENERAL MANAGER").click();
            cy.contains("APPROVAL").click();
            cy.get("#approve-table > tbody > tr:nth-child(1) > td:nth-child(4) > a").click();
            cy.contains("APPROVE ANGGARAN").click();
            cy.contains("Berhasil");
        });   
        */

        it(".should() - logout dan login as OCTIDWI", function () {
            doLogout();
            doLogin("OCTIDWI", "123");
        });

        it(".should() - Kirim Anggaran", function () {
            cy.visit(usedUrl + "/public/anggaran/penerbitan/skki");
            cy.wait(2000);
            cy.get("#penerbitan-ai-table > tbody > tr:nth-child(1) > td:nth-child(3) > a > strong").click({force:true});
            cy.contains("KIRIM ANGGARAN", {force:true}).click();
            cy.get("select[name='ket_ai']").select("MURNI");
            cy.get("button[type='submit'").contains("KIRIM").click();
			cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });
    });
});