context('Assertions',function () {

    var geminiUrl = "http://192.168.0.2/sipat";
    var geminiTestingUrl = "http://192.168.0.2/sipat-testing";
    var taurusTestingUrl = "http://103.28.22.109/sipat-testing";
    var taurusGladysUrl = "http://103.28.22.109/sipat-gladys";
    var taurusUrl = "http://103.28.22.109/sipat";
    var localUrl = "http://localhost/sipatpln";
    var usedUrl = "";
    var GEMINI = "GEMINI";
    var GEMINITESTING = "GEMINITESTING";
    var TAURUSTESTING = "TAURUSTESTING";
    var TAURUS = "TAURUS";
    var TAURUSGLADYS = "TAURUSGLADYS";
    var LOCAL = "LOCAL";
    var useConfig = GEMINI;

    function doLogin(username, password) {
        cy.wait(3000);
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.get("#btnLogout").click({force: true});
    }

    beforeEach(function () {
        // cy.visit("http://localhost/sipatpln/public/login")
        if(useConfig == TAURUSTESTING || useConfig == GEMINITESTING) {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session");
        }
        else if(useConfig == TAURUS || useConfig == TAURUSGLADYS){
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_git_session");
        }
        else {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_session");
        }
        // Cypress.Cookies.preserveOnce();
        cy.viewport(1366,768);
    });

    before(function () {
        cy.clearCookies();
        switch (useConfig) {
            case TAURUSTESTING:
                usedUrl = taurusTestingUrl;
                break;
            case TAURUS:
                usedUrl = taurusUrl;
                break;
            case TAURUSGLADYS:
                usedUrl = taurusGladysUrl;
                break;
            case GEMINI:
            default:
                usedUrl = geminiUrl;
                break;
            case GEMINITESTING:
                usedUrl = geminiTestingUrl;
                break;
            case LOCAL:
                usedUrl = localUrl;
                break;
        }
    });

    var nomorPRKAO = "";
    var nomorSKKO = "12/R/AO-KIT/UIW.ACEH/2019-LSA";

    describe("Buat 1 PRK, 1 SKKO dengan 1 PRK, <10M", function () {
        it(".should() - success login as OCTIDWI (Anggaran)", function () {
            cy.visit(usedUrl + "/public/login");
            cy.document().then((doc) => {
                if(doc.getElementById("btnLogout"))
                    doLogout();
            });
            doLogin("OCTIDWI", "123");
        });
        it(".should() - success create PRKO", function () {
            cy.get("li span").contains("ANGGARAN").click();
            cy.contains("PRK").click({force: true});
            cy.contains("AO NON PEMELIHARAAN").click({force: true});
            cy.url().should('include', '/public/anggaran/prkao/ebudget');
            cy.get("#prkao-table > tbody > tr:nth-child(1) > td:nth-child(7) > a").click(); //Bahan Bakar Dan Pelumas - 5102201
            cy.url().should('include', '/public/anggaran/prkao/nonpemeliharaan');
            cy.contains("TAMBAH PRK").click();
            cy.get("select[name='unit']").select("6111", {force: true});
	    	// cy.get("input[name='prk']").type("PRK.2019.WA-4.3.51150.41").should("have.value", "PRK.2019.WA-4.3.51150.41");
            // cy.get("#kdfungsi").select("Bahan Bakar Dan Pelumas", {force: true});
            // cy.get("#kdjenis").select("5102201 - Bahan Bakar Minyak dan Bahan Bakar Nabati", {force: true});
            //cy.get("#kdsub").select("", {force: true});
            cy.get("textarea[name='detail']").type("Ongkos Angkut BBM Area Langsa").should("have.value", "Ongkos Angkut BBM Area Langsa");
            cy.get("textarea[name='keterangan']").type("Ongkos Angkut BBM Area Langsa").should("have.value", "Ongkos Angkut BBM Area Langsa");
            //cy.get("input[name='volume']").type("10000").should("have.value", "10000");
            //cy.get("input[name='satuan']").type("tes").should("have.value", "tes");
            cy.wait(3000); //wait all xhr
            cy.get("input[name='detail_jasa']").type("Ongkos Angkut BBM Area Langsa").should("have.value", "Ongkos Angkut BBM Area Langsa");
            cy.get("input[name='rpjasa']").type(20000000).should("have.value", 20000000); //5259652000
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
            // cy.screenshot();
            cy.wait(3000);
            cy.document().then((doc) => {
                if(doc.querySelector("#prkao-table a > strong"))
                     nomorPRKAO = doc.querySelector("#prkao-table a > strong").innerText;
            });
        });
        it(".should() - success create Kel Prk", function () {
            // cy.get("li span").contains("ANGGARAN").click();
            // cy.contains("INPUT AI / AO").click({force: true});
            // cy.contains("OPERASI").click({force: true});
            // cy.url().should('include', '/anggaran/introao');
        	cy.visit(usedUrl + "/public/anggaran/introao");
            // cy.get("li span").contains("INPUT AI / AO").click();
            // cy.contains("OPERASI").click();
            cy.contains("PILIH").click();
            cy.get("#unitprk").select("6111");
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("#prk").select(nomorPRKAO + "-Ongkos Angkut BBM Area Langsa", {force:true});
            cy.get("#pakai").type(100000).should("have.value", 100000);
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.contains("Berhasil");
        });
        it(".should() - success create SKKO", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.get("button span").contains("BUAT SKKO").click();
            //cy.get("#gennoskko").click();
            cy.wait(2000); //wait for program to get value
            cy.get("select[name='f_kd_ao']").select("PEMBANGKIT");
            cy.get("textarea[name='uraian_pekerjaan']").type("Ongkos Angkut BBM Area Langsa").should("have.value", "Ongkos Angkut BBM Area Langsa");
            cy.wait(2000); //wait for program to get value
            cy.get("#kdfungsi").select("Bahan Bakar Dan Pelumas");
            cy.get("#kdjenis").select("5102201 - Bahan Bakar Minyak dan Bahan Bakar Nabati");
            // cy.get("#kdsub").select("");
            cy.wait(2000); //wait for program to get value
            cy.get("input[name='notadinas']").type("01/R/AO-KIT/W.ACEH/2019-LSA").should("have.value", "01/R/AO-KIT/W.ACEH/2019-LSA");
            cy.get("input[name='tgl_notadinas']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });
            cy.get("select[name='nd']").select("BIDANG TEKNIK");
            // cy.get("select[name='jnspagu']").select("DEFINITIF");
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
            cy.wait(3000);
            cy.document().then((doc) => {
                if(doc.querySelector("#penerbitan-ao-table tbody  a > strong"))
                    nomorSKKO = doc.querySelector("#penerbitan-ao-table tbody  a > strong").innerText;
            });
        });
        it(".should() - success approval 1 SKKO", function () {
            cy.wait(3000); //tunggu masukan data ke select
            // cy.get("li span").contains("ANGGARAN").click();
            // cy.contains("APPROVAL ANGG").click({force: true});
            cy.visit(usedUrl + "/public/anggaran/approve");
            cy.contains(nomorSKKO).click();
            cy.contains("APPROVE ANGGARAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });
        it(".should() - logout dan login as DEVARIZA", function () {
            doLogout();
            doLogin("DEVARIZA", "123");
        });
        it(".should() - success approval 2 SKKO", function () {
            cy.wait(3000); //tunggu masukan data ke select
            // cy.get("li span").contains("ANGGARAN").click();
            // cy.contains("APPROVAL ANGG").click({force: true});
            cy.visit(usedUrl + "/public/anggaran/approve");
            cy.contains(nomorSKKO).click();
            cy.contains("APPROVE ANGGARAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });
        it(".should() - logout dan login as OCTIDWI", function () {
            doLogout();
            doLogin("OCTIDWI", "123");
        });
        it(".should() - Kirim Anggaran", function () {
            cy.visit(usedUrl + "/public/anggaran/penerbitan/skko");
            cy.contains(nomorSKKO).click();
            cy.contains("KIRIM ANGGARAN").click({force: true});
            cy.get("button[type='submit'").contains("KIRIM").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });
    });

});