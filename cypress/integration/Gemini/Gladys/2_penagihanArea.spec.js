context('Assertions',function () {

    var geminiUrl = "http://192.168.0.2/sipat";
    var geminiTestingUrl = "http://192.168.0.2/sipat-testing";
    var taurusTestingUrl = "http://103.28.22.109/sipat-testing";
    var taurusGladysUrl = "http://103.28.22.109/sipat-gladys";
    var taurusUrl = "http://103.28.22.109/sipat";
    var localUrl = "http://localhost/sipatpln";
    var usedUrl = "";
    var GEMINI = "GEMINI";
    var GEMINITESTING = "GEMINITESTING";
    var TAURUSTESTING = "TAURUSTESTING";
    var TAURUS = "TAURUS";
    var TAURUSGLADYS = "TAURUSGLADYS";
    var LOCAL = "LOCAL";
    var useConfig = GEMINI;


    function doLogin(username, password) {
        cy.wait(3000);
        cy.contains("Sign In");
        cy.get("#emailaddress")
            .type(username)
            .should("have.value", username);
        cy.get("#password")
            .type(password)
            .should("have.value", password);
        cy.contains("Sign In").click();
        cy.url().should("include", "public/");

        //TODO: nanti hapus ini jika sudah tidak ada error yg muncul ke console
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false;
        });
    }

    function doLogout() {
        cy.get("#btnLogout").click({force: true});
    }

    beforeEach(function () {
        // cy.visit("http://localhost/sipatpln/public/login")
        if(useConfig == TAURUSTESTING || useConfig == GEMINITESTING) {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_testing_session");
        }
        else if(useConfig == TAURUS || useConfig == TAURUSGLADYS){
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_git_session");
        }
        else {
            Cypress.Cookies.preserveOnce("XSRF-TOKEN", "pln_sipat_session");
        }
        // Cypress.Cookies.preserveOnce();
        cy.viewport(1366,768);
    });

    before(function () {
        cy.clearCookies();
        switch (useConfig) {
            case TAURUSTESTING:
                usedUrl = taurusTestingUrl;
                break;
            case TAURUS:
                usedUrl = taurusUrl;
                break;
            case TAURUSGLADYS:
                usedUrl = taurusGladysUrl;
                break;
            case GEMINI:
            default:
                usedUrl = geminiUrl;
                break;
            case GEMINITESTING:
                usedUrl = geminiTestingUrl;
                break;
            case LOCAL:
                usedUrl = localUrl;
                break;
        }
    });

    var nomorAnggaran = "12/R/AO-KIT/UIW.ACEH/2019-LSA";
    var nomorNotaDinas = "1234";
    var nomorPR = "1001001";
    var nomorEPROC = "1002001";
    var nomorPO = "1003001";
    var nomorKontrak = "123";
    var nomorPilihPrkNotaDinas = "";
    var nomorSE = "SE0001";

    describe("PENAGIHAN AREA", function () {
        it(".should() - success login as EDDI SAPUTRA (Manager AREA)", function () {
            cy.visit(usedUrl + "/public/login");
            cy.document().then((doc) => {
                if(doc.getElementById("btnLogout"))
                    doLogout();
            });
            doLogin("EDDI.S", "123");
        });
        it(".should() - success DISPOSISI", function () {
            /*cy.get("li span").contains("MANAGER AREA").click();
            cy.contains("ANGGARAN MASUK").click();
            cy.url().should('include', '/pengadaan/anggaranmsk');*/
            cy.visit(usedUrl + "/public/pengadaan/anggaranmsk");
            cy.wait(3000);
            cy.contains(nomorAnggaran).click();
            cy.get("input[value='RENC']").check(); //untuk ceklis pilihan disposisi bagian(RENC=Perencanaan)
            cy.get("#keterangan").type("TEST").should("have.value", "TEST");
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
            // cy.screenshot();
        });

        it(".should() - logout dan login as BIAN (PIC AREA)", function () { //untuk Kota Langsa Bagian Perencanaan
            doLogout();
            doLogin("BIAN.W", "123");
        });
        it(".should() - success INPUT NOTA DINAS", function () {
            /*cy.get("li span").contains("MY SECTION").click();
            cy.contains("EXECUTION").click();
            cy.contains("NOTA DINAS").click();
            cy.url().should('include', '/pengadaan/tbh_kontrak');*/
            cy.visit(usedUrl + "/public/pengadaan/tbh_kontrak");
            cy.wait(3000);
            cy.contains(nomorAnggaran).click();
            cy.get("a").contains("Input Kegiatan").click();
            cy.wait(2000);
            cy.contains("PILIH PRK").click();
            cy.wait(2000); //wait for program to get value
            cy.document().then((doc) => {
                nomorPilihPrkNotaDinas = doc.querySelector("#pilihprk > option:nth-child(2)").value;
                cy.get("#pilihprk").select(nomorPilihPrkNotaDinas, {force:true});
                cy.wait(2000); //wait for program to get value
                cy.get("#pakai").type(10000).should("have.value", 10000);
                cy.get("button[type='submit']").contains("SIMPAN").click();
                cy.get("button.swal2-confirm").contains("Ya").click();
                cy.contains("Berhasil");
            });
        });
        it(".should() - success NOTA DINAS IJIN PRINSIP", function () {
            // cy.wait(3000); //tunggu masukan data ke select
            // cy.get("button span").contains("NOTA DINAS IJIN PRINSIP").click();
            cy.get("#dropDownKegiatan").select("EPROC", {force: true});
            cy.wait(2000); //wait for program to get value
            cy.get("#form-EPROC input[name='no']").type(nomorNotaDinas).should("have.value", nomorNotaDinas);
            cy.get("#form-EPROC input[name='tgl']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("#form-EPROC input[name='sifat']").type("PENTING").should("have.value", "PENTING");
            cy.get("#form-EPROC input[name='lampiran']").type("PERIHAL").should("have.value", "PERIHAL");
            cy.get("#form-EPROC select[name='jenis_kontrak']").select("JASA");
            cy.wait(2000); //wait for program to get value
            cy.get("#form-EPROC input[name='perihal']").type("TEST").should("have.value", "TEST");
            cy.get("#form-EPROC select[name='metode']").select("Pengadaan Langsung");
            cy.get("#form-EPROC select[name='sbr_pendanaan']").select("APLN");
            cy.fixture("image.png").as("file");
            cy.get("#form-EPROC input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });
            cy.get("#form-EPROC select[name='jns_pekerjaan']").select("RUTIN");
            cy.get("#form-EPROC textarea[name='isi_nd']").type("URAIAN NOTA DINAS").should("have.value", "URAIAN NOTA DINAS");
            //masukkan html belum bisa :)
            cy.get("#form-EPROC button[type='submit']").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - logout dan login as EDDI SAPUTRA (Manager AREA)", function () {
            doLogout();
            doLogin("EDDI.S", "123");
        });
        it(".should() - success APPROVAL NOTA DINAS", function () {
            // cy.wait(3000); //tunggu masukan data ke select
            // cy.get("li span").contains("MANAGER AREA").click();
            // cy.contains("APPROVAL").click();
            // cy.get("#table-2 > tbody > tr:nth-child(10) > td:nth-child(2) > a").click();
            cy.visit(usedUrl + "/public/pengadaan/approval_ma");
            cy.wait(2000);
            cy.get("a").contains(nomorNotaDinas).click();
            //note: #table-2: id, tr:nth-child(9): baris keberapa, td:nth-child(2): kolom keberapa
            //cara mudah: inspect-klik kanan-copy selector :)
            cy.get("a").contains("APPROVE").click();
            cy.wait(2000);
            cy.get("select[name='METODE']").select("Pengadaan Langsung");
            cy.get("button[type='submit']").contains("APPROVE").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - logout dan login as BIAN (PIC AREA)", function () { //untuk Kota Langsa Bagian Perencanaan
            cy.visit(usedUrl + "/public/login");
            doLogout();
            doLogin("BIAN.W", "123");
        });
        it(".should() - success UPLOAD RKS", function () {
            // cy.wait(3000); //tunggu masukan data ke select
            // cy.get("li span").contains("MY SECTION").click();
            // cy.contains("EXECUTION").click();
            // cy.contains("INPUT RKS").click();
            // cy.url().should('include', '/pengadaan/rks');
            cy.visit(usedUrl + "/public/pengadaan/rks");
            cy.contains(nomorNotaDinas).click();
            //tr:nth-child(2)=tabel ke 2, td:nth-child(2)=baris kedua
            cy.wait(2000);
            cy.get("a").contains("RENCANA KERJA DAN SYARAT").click(); //apabila ada tulisan yang ganda
            cy.get("input[name='nomor']").type("TS-01").should("have.value", "TS-01");
            cy.get("input[name='tgl']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='pel_peng1']").type("OCTI DWI").should("have.value", "OCTI DWI");
            cy.get("input[name='pel_peng2']").type("EDDI SAPUTRA").should("have.value", "EDDI SAPUTRA");
            cy.get("input[name='menyetujui']").type("DEVARIZA").should("have.value", "DEVARIZA");
            //seharusnya ini yang di upload pdf, tapi di cypress upload image(png) berhasil :)
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });
            cy.get("button[type='submit']").contains("SIMPAN RKS").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });
        it(".should() - success UPLOAD HPE", function () {
            cy.wait(3000); //tunggu masukan data ke select
            cy.contains("INPUT HPE").click();
            cy.get("select[name='metode']").select("Pengadaan Langsung", {force:true});
            cy.get("button[type='submit']").contains("SIMPAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.wait(3000); //tunggu masukan data ke select).click();
            cy.get("a").contains("HARGA PERKIRAAN ENGINEERING").click();
            //note: kalau ada button 2 yang sama, masukkan id form sebelum buttonnya
            cy.fixture("image.png").as("file");
            cy.get("#modal-upload_hpe input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                    });
            });
            cy.get("button[type='submit']").contains("UPLOAD").click();
            cy.contains("Berhasil");
        });
        it(".should() - success SIMPAN DATA", function () {
            cy.get("button[type='submit']").contains("DISPOSISI KE ASRENC").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - logout dan login as RULI RIZALUDDIN (ASMAN APPROVAL)", function () {
            doLogout();
            doLogin("RULI", "123");
        });
        it(".should() - success APPROVAL UPLOAD RKS & HPE", function () {
            /*cy.wait(3000); //tunggu masukan data ke select
            cy.get("li span").contains("ASMAN").click();
            cy.contains("EXECUTION").click({force:true});
            cy.contains("APPROVAL").click({force:true});*/
            cy.visit(usedUrl + "/public/pengadaan/approverksarea");
            cy.wait(3000);
            cy.get("#approval-table > tbody > tr > td.sorting_1 > a").click(); 
            //note: #approval-table: id, td.sorting_1: sorting pertama
            //cara mudah: inspect-klik kanan-copy selector :)
            cy.get("button[type='submit']").contains("APPROVE").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - logout dan login as RIDWAN SURYALESMANA (LAKDAN AREA)", function () {
            doLogout();
            doLogin("RIDWAN", "123");
        });
        it(".should() - berhasil melakukan Input PR", function () {
            // cy.get("a").contains("PENGADAAN").click();
            // cy.contains("INPUT NOMOR").click({force: true});
            // cy.url().should("include", "/public/pengadaan/integrasi");
            cy.visit(usedUrl + "/public/pengadaan/integrasi");
            cy.wait(3000);
            // cy.get("li.paginate_button").contains("3").click();
            // cy.wait(2000);
            cy.get("#approval-table > tbody > tr:nth-child(1) #btnpredit").click();
            cy.get("input[name='nomor']").type(nomorPR).should("have.value", nomorPR);
            cy.get("#modal-nomor button").contains("PILIH").click();
            cy.contains("Berhasil");
        });
        // it(".should() - berhasil melakukan Input EPROC", function () {
        //     cy.wait(3000);
        //     cy.get("li.paginate_button").contains("3").click();
        //     cy.wait(2000);
        //     cy.get("#approval-table > tbody > tr:nth-child(1) #btneprocedit").click();
        //     cy.get("input[name='nomor']").type(nomorEPROC).should("have.value", nomorEPROC);
        //     cy.get("#modal-nomor button").contains("PILIH").click();
        //     cy.contains("Berhasil");
        // });
        // it(".should() - berhasil melakukan Input PO", function () {
        //     cy.get("li.paginate_button").contains("3").click();
        //     cy.wait(2000);
        //     cy.get("#approval-table > tbody > tr:nth-child(1) #listpo").click();
        //     cy.get("body > div.wrapper > div.content-wrapper > section.content > div > div > div.btn-group").click();
        //     cy.get("input[name='nomor']").type(nomorPO).should("have.value", nomorPO);
        //     cy.get("#modal-nomor button").contains("PILIH").click();
        //     cy.get("button.swal2-confirm").contains("Ya").click();
        //     cy.contains("Berhasil");
        // });
        it(".should() - success UPLOAD/PEMBUATAN KONTRAK", function () {
            // cy.contains("INPUT SPK/KONTRAK").click({force: true});
            // cy.url().should('include', '/pengadaan/spk');
            cy.visit(usedUrl + "/public/pengadaan/spk");
            cy.wait(3000);
            // cy.get("li.paginate_button").contains("3").click();
            // cy.wait(3000);
            cy.contains(nomorNotaDinas).click();

            //insert rp kontrak
            cy.get("#sendspk table a>i[class='fa fa-cog']").click();
            cy.wait(2000);
            cy.get("#formupdateprk > button").contains("SIMPAN").click({force:true});

            //Input Perjanjian/SPK
            cy.get("#sendspk a").contains("PERJANJIAN / SPK").click({force:true});
            cy.get("a").contains("UPLOAD PERJANJIAN").click({force:true});
            cy.get("input[name='lokasi']").type("Aceh Langsa").should("have.value", "Aceh Langsa");
            cy.get("select[name='vendor_id']").select("PT. VEGANA ELEKTRIK UTAMA", {force:true});
            cy.get("input[name='nomor']").type(nomorKontrak).should("have.value", nomorKontrak);
            cy.fixture("image.png").as("file");
            cy.get("input[name='file']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                });
            });
            cy.get("input[name='no_penawaran']").type("123").should("have.value", "123");
            cy.get("input[name='no_skma']").type("321").should("have.value", "321");
            cy.get("input[name='tgl0']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='tgl1']").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today
            cy.get("input[name='tgl2']").click();
            cy.get("div[class='datepicker-days'] th.next").click(); //next month
            cy.get("tr td.day").contains("25").click(); //tgl 25 next month
            cy.get("input[name='nm_pihaki']").type("PIHAK PERTAMA").should("have.value", "PIHAK PERTAMA");
            cy.get("input[name='nm_pihakii']").type("DEVARIZA").should("have.value", "DEVARIZA");
            cy.wait(3000); //wait all xhr
            cy.get("button[type='submit']").contains("SIMPAN PERJANJIAN").click({force:true});
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");

            //Input jumlah termin dan kirim
            cy.get("#jenis_pembayaran").select("TERMIN",{force:true});
            cy.get("input[name='jumlahtermin']").type('2').should("have.value", '2', {force:true});
            cy.contains("Jika tombol tidak tampil").click({force:true});
            cy.get("input[name='raportvendor[1]']").check();
            cy.get("button[type='submit']").contains("KIRIM PROGRESS KE ASMAN").click({force:true});
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });

        it(".should() - logout dan login as BIAN (PIC AREA)", function () { //untuk Kota Langsa Bagian Perencanaan
            cy.visit(usedUrl + "/public/login");
            doLogout();
            doLogin("BIAN.W", "123");
        });
        it(".should() - success INPUT RAPORT", function () {
            // cy.get("li span").contains("MY SECTION").click();
            // cy.contains("PENILAIAN").click({force: true});
            // cy.contains("ADD RAPORT").click({force: true});
            // cy.url().should('include', '/public/penilaian');
            cy.visit(usedUrl + "/public/penilaian/history");
            cy.wait(3000);
            cy.get("#raport-table tr:nth-child(1) a").contains("edit").click();
            cy.wait(3000);
            //note: nilai yang dikasih
            cy.get("#R_mut_3").click();
            cy.get("#R_wakt_4u").click();
            cy.get("#R_lingkungan_4").click();
            cy.get("#R_ketaatan_dokume_4n").click();
            cy.get("#R_komunikas_4i").click();
            cy.get("input[value='Update Nilai Vendor'").click({force:true});
        });
        it(".should() - success INPUT PENAGIHAN", function () {
            /*cy.contains("PENAGIHAN").click({force: true});
            cy.contains("KONTRAK").click({force: true});
            cy.url().should('include', '/public/penagihan/tagihan');*/
            cy.visit(usedUrl + "/public/penagihan/tagihan");
            cy.wait(3000);
            cy.contains("INPUT TAGIHAN").click();
            cy.wait(3000);
            cy.get("#nokontrak").select("123-TEST", {force:true});
            cy.wait(5000); //NOTE: Di aplikasi, loading agak lama
            cy.get("select[name='termin']").select("1", {force:true});
            cy.wait(5000); //NOTE: Di aplikasi, loading agak lama
            cy.get("#listPrk1").type("10000").should("have.value", 10000);
            cy.get("textarea[name='ket_tagihan']").type("TES PENAGIHAN").should("have.value", "TES PENAGIHAN");
            cy.get("#nopo").select(nomorPR + "PO", {force:true});
            // cy.get("select[name='nose']").select("SE0002", {force:true}); //NOTE: TIDAK WAJIB!
            cy.get("button[type='submit'").contains("SIMPAN").click();
            cy.contains("Berhasil");
        });

        it(".should() - logout dan login as YESI (Name: HERLINA)", function () { //untuk Kota Langsa Bagian Perencanaan
            cy.visit(usedUrl + "/public/login");
            doLogout();
            doLogin("YESI", "123");
        });
        it(".should() - success VERIFIKASI PENAGIHAN", function () {
            /*cy.get("li span").contains("VERIFIKASI").click();
            cy.contains("KONTRAK").click();
            cy.url().should('include', '/public/verifikasi/siap_bayar');*/
            cy.visit(usedUrl + "/public/verifikasi/siap_bayar");
            cy.wait(3000);
            cy.get("#datatable-siap_bayar > tbody > tr > td:nth-child(2) > a").click();
            cy.get("a").contains("FORM VERIFIKASI").click();
            cy.wait(2000);
            cy.get("input[name='nose']").type(nomorSE).should("have.value", nomorSE);
            cy.get("button[type='submit'").contains("VERIFIKASI").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");

            //surat permohonan pembayaran
            cy.get("input[name='pajak[status_permohonan]").check({force: true});
            cy.fixture("image.png").as("file");
            cy.get("input[name='file_permohonan']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                });
            });
            cy.get("input[name='pajak[no_permohonan]").type("NO.SPP-01").should("have.value", "NO.SPP-01");
            cy.get("#spp_tgl").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today

            //kwitansi
            cy.get("input[name='pajak[status_kwitansi]").check({force: true});
            cy.fixture("image.png").as("file");
            cy.get("input[name='file_kwitansi']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                });
            });
            cy.get("input[name='pajak[no_kwitansi]").type("NO.KWITANSI-01").should("have.value", "NO.KWITANSI-01");
            cy.get("#kwitansi_tgl").click();
            cy.get("tr td.today").contains((new Date()).getDate()).click(); //today

            //BA serah terima pekerjaan
            cy.get("input[name='pajak[status_bast]").check({force: true});
            cy.fixture("image.png").as("file");
            cy.get("input[name='file_bast']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                });
            });
            cy.get("input[name='pajak[no_bast]").type("NO.BA-01").should("have.value", "NO.BA-01");
            cy.get("#ba_tgl").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today

            //faktur pajak
            cy.get("input[name='pajak[status_faktur]").check({force: true});
            cy.fixture("image.png").as("file");
            cy.get("input[name='file_faktur']").then(subject => {
                return Cypress.Blob.base64StringToBlob(this.file, "image/png").then(blob => {
                        const el = subject[0];
                        const testFile = new File([blob], 'image.png', { type: 'image/png' });
                        const dataTransfer = new DataTransfer();
                        dataTransfer.items.add(testFile);
                        el.files = dataTransfer.files
                });
            });
            cy.get("input[name='pajak[no_faktur]").type("NO.Faktur-01").should("have.value", "NO.Faktur-01");
            cy.get("#fp_tgl").click();
            cy.get("tr td.day").contains((new Date()).getDate()).click(); //today

            //copy kontrak
            cy.get("input[name='pajak[copy_kontrak]").check({force: true});

            //rekapitulasi rincian biaya
            cy.get("input[name='pajak[rekapitulasi]").check({force: true});

            //bank garansi
            //cy.get("select[name='pajak[status_garansi]']").select("TIDAK PERLU", {force:true});
            cy.get("button[type='submit'").contains("APPROVE TAGIHAN").click();
            cy.get("button.swal2-confirm").contains("Ya").click();
            cy.contains("Berhasil");
        });
    });
});