<?php
namespace App\Mail;


use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class PurchaseOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $headerPo;
    public $detailPo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($headerPo, $detailPo)
    {
        $this->headerPo = $headerPo;
        $this->detailPo = $detailPo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view("emails/purchaseorder");
    }

}
