<?php
namespace App\Helpers;

class Registry
{
    const NAMA_PERUSAHAAN = "ZC01";

    const THEME_PARTS = array(
        "box" => array("box", "panel panel-dark"),
        "head" => array("box-header", "panel-heading"),
        "body" => array("box-body", "panel-body"),
        "foot" => array("box-footer", "panel-footer"),
    );

}

