<?php

namespace App\Models\Sys;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'sys_role';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $guarded = [];
}
