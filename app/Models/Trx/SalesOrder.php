<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model
{
    protected $table = 'trx_salesorder';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $guarded = [];

    public function updateTotal($soid)
    {
        $sum = SalesOrderItem::where("soid", $soid)->sum("total");
        $this->where("id", $soid)->update(array("total" => $sum));
    }

    public function updateStatus($soid)
    {
        $sum = SalesOrderItem::where("soid", $soid)->count("id");
        $sumApprove = SalesOrderItem::where("soid", $soid)->where("status", "APPROVED")->count("id");
        if($sum == $sumApprove) $this->where("id", $soid)->update(array("status" => "DONE"));
    }

    public function generateCode()
    {
        $lastId = $this->orderBy("id", "desc")->first();
        if($lastId) $lastId = $lastId->id + 1;
        else $lastId = 1;
        $kode = "SO-".str_pad($lastId, 5, "0", STR_PAD_LEFT);
        return $kode;
    }

    public function gruping($param)
    {
        $res = $this->select($param)->groupBy($param)->get();
        return $res;
    }

    public function filter($request)
    {
        $res = $this->select("trx_salesorder.*")
            ->leftJoin('trx_salesorder_item','trx_salesorder.id','trx_salesorder_item.soid');
        if($request->bahanid != 'all') $res->where('trx_salesorder_item.bahanid', $request->bahanid);
        if($request->tgl_awal != null) $res->where('trx_salesorder.tanggal','>=', $request->tgl_awal);
        if($request->tgl_akhir != null) $res->where('trx_salesorder.tanggal', '<=', $request->tgl_akhir);
        return $res->groupBy('id')->get();
    }
}
