<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class SalesReturItem extends Model
{
    protected $table = 'trx_salesretur_item';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $guarded = [];

    public function getByReturId($id)
    {
        return $this->where("returid", $id)->get();
    }

    public function getByReturDanPenjualan($id, $soid)
    {
        return $this->where("returid", $id)->where("soitemid", $soid)->orderBy("id", "desc")->first();
    }
}
