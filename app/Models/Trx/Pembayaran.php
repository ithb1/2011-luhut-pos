<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'trx_pembayaran';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $guarded = [];

    public function getByPo($id)
    {
        return $this->where("poid", $id)->get();
    }

    public function generateCode()
    {
        $lastId = $this->orderBy("id", "desc")->first();
        if($lastId) $lastId = $lastId->id + 1;
        else $lastId = 1;
        $kode = "PU-".str_pad($lastId, 5, "0", STR_PAD_LEFT);
        return $kode;
    }

    public function gruping($param)
    {
        $res = $this->select($param)->groupBy($param)->get();
        return $res;
    }

    public function filter($request)
    {
        $res = $this->select("trx_pembayaran.*")
            ->leftJoin('trx_pembayaran_item','trx_pembayaran_item.pembayaranid','trx_pembayaran.id')
            ->leftJoin('trx_purchaseorder','trx_purchaseorder.id','trx_pembayaran_item.poid');
        if($request->supplier_id != 'all') $res->where('trx_purchaseorder.supplier_id', $request->supplier_id);
        if($request->tgl_awal != null) $res->where('trx_pembayaran.tanggal','>=', $request->tgl_awal);
        if($request->tgl_akhir != null) $res->where('trx_pembayaran.tanggal', '<=', $request->tgl_akhir);
        return $res->groupBy('id')->get();
    }
}
