<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $table = 'trx_purchaseorder';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $guarded = [];

    public function updateTotal($poid)
    {
        $sum = PurchaseOrderItem::where("poid", $poid)->sum("total");
        $this->where("id", $poid)->update(array("total" => $sum));
    }

    public function updateStatusPenerimaan($poid)
    {
//        compare poitem dan penerimaan item
//        poitem
        $poitem = (new PurchaseOrderItem())->where("poid",$poid)->get();
        $penerimaanitem = (new PenerimaanItem())->where("poid",$poid)->where("status",1)->get();
        if(count($poitem) == count($penerimaanitem)){
            $this->where("id", $poid)->update(array("status_penerimaan" => "DONE"));
        }
    }

    public function updateStatusPembayaran($poid, $status)
    {
        $this->where("id", $poid)->update(array("status_pembayaran" => $status));
    }

    public function getNotLunas()
    {
        return $this->where("status_pembayaran", "UNPAID")->get();
    }

    public function generateCode()
    {
        $lastId = $this->orderBy("id", "desc")->first();
        if($lastId) $lastId = $lastId->id + 1;
        else $lastId = 1;
        $kode = "PO-".str_pad($lastId, 5, "0", STR_PAD_LEFT);
        return $kode;
    }

    public function gruping($param)
    {
        $res = $this->select($param)->groupBy($param)->get();
        return $res;
    }

    public function filter($request)
    {
        $res = $this->select("*");
        if($request->supplier_id != 'all') $res->where('supplier_id', $request->supplier_id);
        if($request->tgl_awal != null) $res->where('tanggal','>=', $request->tgl_awal);
        if($request->tgl_akhir != null) $res->where('tanggal', '<=', $request->tgl_akhir);
        return $res->get();
    }
}
