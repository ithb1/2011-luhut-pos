<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderItem extends Model
{
    protected $table = 'trx_purchaseorder_item';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $guarded = [];

    public function getByPo($id)
    {
        return $this->where("poid", $id)->get();
    }
}
