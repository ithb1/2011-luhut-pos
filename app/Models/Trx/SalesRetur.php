<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class SalesRetur extends Model
{
    protected $table = 'trx_salesretur';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $guarded = [];

    public function updateTotal($returid)
    {
        $sum = SalesReturItem::where("returid", $returid)->sum("total");
        $this->where("id", $returid)->update(array("total" => $sum));
    }

    public function generateCode()
    {
        $lastId = $this->orderBy("id", "desc")->first();
        if($lastId) $lastId = $lastId->id + 1;
        else $lastId = 1;
        $kode = "SR-".str_pad($lastId, 5, "0", STR_PAD_LEFT);
        return $kode;
    }

    public function gruping($param)
    {
        $res = $this->select($param)->groupBy($param)->get();
        return $res;
    }

    public function filter($request)
    {
        $res = $this->select("trx_salesretur.*")
            ->leftJoin('trx_salesretur_item','trx_salesretur.id','trx_salesretur_item.soid')
            ->leftJoin('trx_salesorder_item','trx_salesorder_item.id','trx_salesretur_item.soitemid');
        if($request->bahanid != 'all') $res->where('trx_salesorder_item.bahanid', $request->bahanid);
        if($request->tgl_awal != null) $res->where('trx_salesretur.tanggal','>=', $request->tgl_awal);
        if($request->tgl_akhir != null) $res->where('trx_salesretur.tanggal', '<=', $request->tgl_akhir);
        return $res->groupBy('id')->get();
    }
}
