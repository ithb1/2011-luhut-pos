<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class PenerimaanItem extends Model
{
    protected $table = 'trx_penerimaan_item';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $guarded = [];

    public function getByPenerimaan($id)
    {
        return $this->where("penerimaanid", $id)->get();
    }

    public function getByPenerimaanDanPo($id, $poid)
    {
        return $this->where("penerimaanid", $id)->where("poitemid", $poid)->orderBy("id", "desc")->first();
    }
}
