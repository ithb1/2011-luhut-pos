<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class PembayaranItem extends Model
{
    protected $table = 'trx_pembayaran_item';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $guarded = [];

    public function getByPembayaran($id)
    {
        return $this->where("pembayaranid", $id)->get();
    }

    public function getSumtotalByPembayaran($id)
    {
        return $this
            ->leftJoin('trx_purchaseorder', 'trx_purchaseorder.id', $this->table.'.poid')
            ->where($this->table.".pembayaranid", $id)
            ->sum('trx_purchaseorder.total');
    }
}
