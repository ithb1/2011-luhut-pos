<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class SalesOrderItem extends Model
{
    protected $table = 'trx_salesorder_item';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $guarded = [];

    public function getByPenjualan($id)
    {
        return $this->where("soid", $id)->get();
    }
}
