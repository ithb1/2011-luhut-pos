<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Lab404\Impersonate\Models\Impersonate;

class User extends Authenticatable
{
    use Notifiable, Impersonate;

    protected $table = 'sys_user';

    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*------------ relations to other model -----------------*/
    public function tRole()
    {
        return $this->hasOne(Role::class, "id","roleid");
    }

    /*------------- end : relations to other model --------------------*/


    public function login(string $username, $password)
    {
        return Auth::attempt(["username" => $username, "password" => $password]);
    }

    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
            parent::setAttribute($key, $value);
        }
    }

    public static function getById($id){
        $user = new self;

        return $user->where("id", "=", $id)->first();
    }

    public static function getByUsername($username){
        return self::where('username', '=', $username)->first();
    }

}
