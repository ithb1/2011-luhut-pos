<?php

namespace App\Models\Mst;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bahan extends Model
{
    protected $table = 'mst_bahan';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $guarded = [];

    public function generateCode()
    {
        $lastId = $this->orderBy("id", "desc")->first();
        if($lastId) $lastId = $lastId->id + 1;
        else $lastId = 1;
        $kode = "SB-".str_pad($lastId, 5, "0", STR_PAD_LEFT);
        return $kode;
    }

    public function gruping($param)
    {
        $res = $this->select($param)->groupBy($param)->get();
        return $res;
    }

    public function filter($request)
    {
        $res = $this->select("*");
        if($request->nama != 'all') $res->where('nama', $request->nama);
        if($request->jenis_benang != 'all') $res->where('jenis_benang', $request->jenis_benang);
        if($request->warna != 'all') $res->where('warna', $request->warna);
        return $res->get();
    }
}
