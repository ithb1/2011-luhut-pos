<?php

namespace App\Http\Controllers;

use App\Models\Mst\Bahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data = array(
            "user" => Auth::user(),
            "rowKrisis" => Bahan::where("stok","<=", 50)->get()
        );

        return view('home', $data);
    }
}
