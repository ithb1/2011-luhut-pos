<?php

namespace App\Http\Controllers\Trx;

use App\Http\Controllers\Controller;
use App\Models\Mst\Bahan;
use App\Models\Trx\SalesOrder;
use App\Models\Trx\SalesOrderItem;
use App\Models\Trx\SalesRetur;
use App\Models\Trx\SalesReturItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReturController extends Controller
{
    protected $so;
    protected $soi;
    protected $retur;
    protected $returItem;
    protected $bahan;

    public function __construct()
    {
        $this->so = new SalesOrder();
        $this->soi = new SalesOrderItem();
        $this->retur = new SalesRetur();
        $this->returItem = new SalesReturItem();
        $this->bahan = new Bahan();
        parent::__construct();
    }

    public function index(Request $request){
        $data = array(
            "user" => Auth::user(),
            "rowData" => $this->retur->get(),
            "rowBahan" => $this->bahan->get()
        );

        return view('trx/retur/index', $data);
    }

    public function show($id)
    {
        $data = array(
            "rowBahan" => $this->bahan->get(),
            "data" => $this->retur->find($id),
            "dataSo" => $this->so->find($this->retur->find($id)->soid) ?? [],
            "rowSoItem" => $this->soi->getByPenjualan($this->retur->find($id)->soid) ?? []
        );

        return view('trx/retur/detail', $data);
    }

    public function create()
    {
        $data = array(
            "rowSo" => $this->so->where("status", "DONE")->get(),
            "kode" => $this->retur->generateCode()
        );

        return view('trx/retur/create', $data);
    }

    public function store(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $this->retur->$k = $v;
            }

            $this->retur->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\ReturController@edit', ['id' => $this->retur->id]);
    }

    public function edit($id)
    {
        $data = array(
            "user" => Auth::user(),
            "rowBahan" => $this->bahan->get(),
            "data" => $this->retur->find($id),
            "dataSo" => $this->so->find($this->retur->find($id)->soid) ?? [],
            "rowSoItem" => $this->soi->getByPenjualan($this->retur->find($id)->soid) ?? []
        );

        return view('trx/retur/edit', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            $model = $this->retur->findOrFail($id);
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $model->$k = $v;
            }

            $model->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\ReturController@index');
    }

    public function delete($id){
        $v = redirect()->action('Trx\ReturController@index');
        try{
            $modelItem = $this->returItem->where("returid",$id);
            $modelItem->delete();
            $model = $this->retur->findOrFail($id);
            $model->delete();
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }
        return $v->with("successmessage", "Data berhasil dihapus!");
    }

    public function detailJson($id)
    {
        header('Content-Type: application/json');
        $rowData['so'] = $this->so->selectRaw("trx_salesorder.*")->where("trx_salesorder.id", $id)->first();
        $rowData['soi'] = $this->soi->where("soid", $id)->get();
        echo json_encode( $rowData );
    }


//    ================================================================

    public function updateItem(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

//            insert or update item
            if($data['id']) $this->returItem = $this->returItem->find($data['id']);
            foreach ($data as $k => $v){
                $this->returItem->$k = $v;
            }
            $this->returItem->save();

//            update total
            $this->retur->updateTotal($data['returid']);
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\ReturController@edit', ['id' => $data['returid']]);
    }
}
