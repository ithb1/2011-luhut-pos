<?php

namespace App\Http\Controllers\Trx;

use App\Http\Controllers\Controller;
use App\Models\Mst\Bahan;
use App\Models\Mst\Supplier;
use App\Models\Trx\Penerimaan;
use App\Models\Trx\PenerimaanItem;
use App\Models\Trx\PurchaseOrder;
use App\Models\Trx\PurchaseOrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PenerimaanController extends Controller
{
    protected $po;
    protected $poi;
    protected $supplier;
    protected $penerimaan;
    protected $penerimaanItem;
    protected $bahan;

    public function __construct()
    {
        $this->po = new PurchaseOrder();
        $this->poi = new PurchaseOrderItem();
        $this->supplier = new Supplier();
        $this->penerimaan = new Penerimaan();
        $this->penerimaanItem = new PenerimaanItem();
        $this->bahan = new Bahan();
        parent::__construct();
    }

    public function index(Request $request){
        $data = array(
            "user" => Auth::user(),
            "rowData" => $this->penerimaan->get(),
            "rowSupplier" => $this->supplier->get()
        );

        return view('trx/penerimaan/index', $data);
    }

    public function show($id)
    {
        $penerimaan = $this->penerimaan->find($id);
        $po = $this->po->find($penerimaan->poid);
        $supplier = $this->supplier->find($po->supplier_id);
        $data = array(
            "rowSupplier" => $this->supplier->get(),
            "rowBahan" => $this->bahan->get(),
            "data" => $penerimaan,
//            "rowItem" => $this->penerimaanItem->getByPenerimaan($id) ?? [],
            "dataPo" => $po ?? [],
            "dataSupplier" => $supplier,
            "rowPoItem" => $this->poi->getByPo($this->penerimaan->find($id)->poid) ?? []
        );

        return view('trx/penerimaan/detail', $data);
    }

    public function create()
    {
        $data = array(
            "rowSupplier" => $this->supplier->get(),
            "rowPo" => $this->po->where("status_penerimaan", "PROGRESS")->get(),
            "kode" => $this->penerimaan->generateCode()
        );

        return view('trx/penerimaan/create', $data);
    }

    public function store(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $this->penerimaan->$k = $v;
            }

            $this->penerimaan->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PenerimaanController@edit', ['id' => $this->penerimaan->id]);
    }

    public function edit($id)
    {
        $penerimaan = $this->penerimaan->find($id);
        $po = $this->po->find($penerimaan->poid);
        $supplier = $this->supplier->find($po->supplier_id);
        $data = array(
            "user" => Auth::user(),
            "rowSupplier" => $this->supplier->get(),
            "rowBahan" => $this->bahan->get(),
            "data" => $penerimaan,
//            "rowItem" => $this->penerimaanItem->getByPenerimaan($id) ?? [],
            "dataPo" => $po ?? [],
            "dataSupplier" => $supplier,
            "rowPoItem" => $this->poi->getByPo($this->penerimaan->find($id)->poid) ?? []
        );

        return view('trx/penerimaan/edit', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            $model = $this->po->findOrFail($id);
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $model->$k = $v;
            }

            $model->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PenerimaanController@index');
    }

    public function delete($id){
        $v = redirect()->action('Trx\PenerimaanController@index');
        try{
            $modelItem = $this->penerimaanItem->where("penerimaanid",$id);
            $modelItem->delete();
            $model = $this->penerimaan->findOrFail($id);
            $model->delete();
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }
        return $v->with("successmessage", "Data berhasil dihapus!");
    }

    public function detailJson($id)
    {
        header('Content-Type: application/json');
        $rowData['po'] = $this->po->selectRaw("trx_purchaseorder.*, mst_supplier.nama as nama_supplier")->leftJoin("mst_supplier","mst_supplier.id", "=", "trx_purchaseorder.supplier_id")->where("trx_purchaseorder.id", $id)->first();
        $rowData['poi'] = $this->poi->where("poid", $id)->get();
        echo json_encode( $rowData );
    }


//    ================================================================

    public function updateItem(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

//            insert item
            foreach ($data as $k => $v){
                $this->penerimaanItem->$k = $v;
            }
            $this->penerimaanItem->save();

//            update penerimaan
            $this->po->updateStatusPenerimaan($data['poid']);

//            tambah stok
            $poitemid = $data['poitemid'];
            $poitem = PurchaseOrderItem::find($poitemid);
            $bahan = Bahan::find($poitem->bahanid);
            $qty = $data['qty'] * 25;
            $bahan->stok += $qty;
            $bahan->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PenerimaanController@edit', ['id' => $data['penerimaanid']]);
    }
}
