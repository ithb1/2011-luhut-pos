<?php

namespace App\Http\Controllers\Trx;

use App\Http\Controllers\Controller;
use App\Models\Mst\Bahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StokController extends Controller
{
    protected $bahan;

    public function __construct()
    {
        $this->bahan = new Bahan();
        parent::__construct();
    }

    public function index(Request $request){
        $data = array(
            "user" => Auth::user(),
            "rowData" => $this->bahan->get(),
            "rowNama" => $this->bahan->gruping("nama"),
            "rowBenang" => $this->bahan->gruping("jenis_benang"),
            "rowWarna" => $this->bahan->gruping("warna")
        );

        return view('trx/stok/index', $data);
    }
}
