<?php

namespace App\Http\Controllers\Trx;

use App\Http\Controllers\Controller;
use App\Models\Mst\Bahan;
use App\Models\Mst\Supplier;
use App\Models\Trx\Pembayaran;
use App\Models\Trx\PembayaranItem;
use App\Models\Trx\PurchaseOrder;
use App\Models\Trx\PurchaseOrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PembayaranUtangController extends Controller
{
    protected $po;
    protected $poi;
    protected $supplier;
    protected $bahan;
    protected $pembayaran;
    protected $pembayaranItem;

    public function __construct()
    {
        $this->po = new PurchaseOrder();
        $this->poi = new PurchaseOrderItem();
        $this->supplier = new Supplier();
        $this->bahan = new Bahan();
        $this->pembayaran = new Pembayaran();
        $this->pembayaranItem = new PembayaranItem();
        parent::__construct();
    }

    public function index(Request $request){
        $data = array(
            "user" => Auth::user(),
            "rowData" => $this->pembayaran->get(),
            "rowSupplier" => $this->supplier->get()
        );

        return view('trx/pembayaranutang/index', $data);
    }

    public function show($id)
    {
        $data = array(
            "data" => $this->pembayaran->find($id),
            "rowPo" => $this->po->getNotLunas() ?? [],
            "rowItem" => $this->pembayaranItem->getByPembayaran($id) ?? []
        );

        return view('trx/pembayaranutang/detail', $data);
    }

    public function create()
    {
        $data = array(
            "rowPo" => $this->po->get(),
            "kode" => $this->pembayaran->generateCode()
        );

        return view('trx/pembayaranutang/create', $data);
    }

    public function store(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $this->pembayaran->$k = $v;
            }

            $this->pembayaran->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PembayaranUtangController@edit', ['id' => $this->pembayaran->id]);
    }

    public function edit($id)
    {
        $data = array(
            "user" => Auth::user(),
            "data" => $this->pembayaran->find($id),
            "rowPo" => $this->po->getNotLunas() ?? [],
            "rowItem" => $this->pembayaranItem->getByPembayaran($id) ?? [],
            "maxTotal" => $this->pembayaranItem->getSumtotalByPembayaran($id) ?? 0
        );

        return view('trx/pembayaranutang/edit', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            $model = $this->pembayaran->findOrFail($id);
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $model->$k = $v;
            }

            $model->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PembayaranUtangController@index');
    }

    public function delete($id){
        $v = redirect()->action('Trx\PembayaranUtangController@index');
        try{
            $modelItem = $this->pembayaranItem->where("pembayaranid",$id);
            $modelItem->delete();
            $model = $this->pembayaran->findOrFail($id);
            $model->delete();
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }
        return $v->with("successmessage", "Data berhasil dihapus!");
    }

    public function detailJson($id)
    {
        header('Content-Type: application/json');
        $rowData = $this->pembayaranItem->find($id);
        echo json_encode( $rowData );
    }


//    ================================================================

    public function addItem(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

//            insert or update item
            if($data['id']) $this->pembayaranItem = $this->pembayaranItem->find($data['id']);
            foreach ($data as $k => $v){
                $this->pembayaranItem->$k = $v;
            }
            $this->pembayaranItem->save();

//            update pembayaran
            $this->po->updateStatusPembayaran($data['poid'], "PAID");
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PembayaranUtangController@edit', ['id' => $data['pembayaranid']]);
    }

    public function deleteItem($id){
        $v = redirect()->action('Trx\PembayaranUtangController@index');
        try{
            $model = $this->pembayaranItem->findOrFail($id);
            $pembayaranid = $model->pembayaranid;
            $poid = $model->poid;
            $model->delete();

//            update pembayaran
            $this->po->updateStatusPembayaran($poid, "UNPAID");
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }

        return redirect()->action('Trx\PembayaranUtangController@edit', ['id' => $pembayaranid]);
    }
}
