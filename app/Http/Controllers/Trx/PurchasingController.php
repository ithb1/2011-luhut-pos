<?php

namespace App\Http\Controllers\Trx;

use App\Helpers\Registry;
use App\Http\Controllers\Controller;
use App\Models\Mst\Bahan;
use App\Models\Mst\Supplier;
use App\Models\Trx\PurchaseOrder;
use App\Models\Trx\PurchaseOrderItem;
use App\Services\EmailService;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PurchasingController extends Controller
{
    protected $po;
    protected $poi;
    protected $supplier;
    protected $bahan;

    public function __construct()
    {
        $this->po = new PurchaseOrder();
        $this->poi = new PurchaseOrderItem();
        $this->supplier = new Supplier();
        $this->bahan = new Bahan();
        parent::__construct();
    }

    public function index(Request $request){
        $data = array(
            "user" => Auth::user(),
            "rowData" => $this->po->get(),
            "rowSupplier" => $this->supplier->get()
        );

        return view('trx/purchasing/index', $data);
    }

    public function show($id)
    {
        $data = array(
            "data" => $this->po->find($id),
            "rowSupplier" => $this->supplier->get(),
            "rowItem" => $this->poi->getByPo($id)
        );

        return view('trx/purchasing/detail', $data);
    }

    public function create()
    {
        $data = array(
            "rowSupplier" => $this->supplier->get(),
            "kode" => $this->po->generateCode()
        );

        return view('trx/purchasing/create', $data);
    }

    public function store(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $this->po->$k = $v;
            }

            $this->po->status_pembayaran = "UNPAID";
            $this->po->status_penerimaan = "PROGRESS";

            $this->po->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PurchasingController@edit', ['id' => $this->po->id]);
    }

    public function edit($id)
    {
        $data = array(
            "user" => Auth::user(),
            "data" => $this->po->find($id),
            "rowSupplier" => $this->supplier->get(),
            "rowBahan" => $this->bahan->get(),
            "rowItem" => $this->poi->getByPo($id) ?? []
        );

        return view('trx/purchasing/edit', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            $model = $this->po->findOrFail($id);
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $model->$k = $v;
            }

            $model->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PurchasingController@index');
    }

    public function delete($id){
        $v = redirect()->action('Trx\PurchasingController@index');
        try{
            $modelItem = $this->poi->where("poid",$id);
            $modelItem->delete();
            $model = $this->po->findOrFail($id);
            $model->delete();
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }
        return $v->with("successmessage", "Data berhasil dihapus!");
    }

    public function headerJson($id)
    {
        header('Content-Type: application/json');
        $rowData = $this->po->find($id);
        echo json_encode( $rowData );
    }

    public function detailJson($id)
    {
        header('Content-Type: application/json');
        $rowData = $this->poi->find($id);
        echo json_encode( $rowData );
    }

    public function sendEmail($id)
    {
        $v = redirect()->action('Trx\PurchasingController@index');
        $user = Auth::user();
        $headerPo = $this->po->find($id);
        $detailPo = $this->poi->getByPo($id);
        $supplier = $this->supplier->find($headerPo->supplier_id);
        $email = $supplier->email;
        try {
            Mail::send([], [], function ($message) use ($email, $headerPo,$detailPo){
                $message
                    ->to($email)
                    ->subject("PURCHASE ORDER ". Registry::NAMA_PERUSAHAAN)
                    ->setBody((new \App\Mail\PurchaseOrder($headerPo, $detailPo))->render(), 'text/html')
                    ->setFrom(config("mail.from.address"), config("mail.from.name"))
                ;
            });
//            return (new \App\Mail\PurchaseOrder())->render();
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }
        return $v->with("successmessage", "Email Sent");
    }

//    ================================================================

    public function addItem(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

//            insert or update item
            if($data['id']) $this->poi = $this->poi->find($data['id']);
            foreach ($data as $k => $v){
                $this->poi->$k = $v;
            }
            $this->poi->save();

//            update total
            $this->po->updateTotal($data['poid']);
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PurchasingController@edit', ['id' => $data['poid']]);
    }

    public function deleteItem($id){
        $v = redirect()->action('Trx\PurchasingController@index');
        try{
            $model = $this->poi->findOrFail($id);
            $poid = $model->poid;
            $model->delete();

//            update total
            $this->po->updateTotal($poid);
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }

        return redirect()->action('Trx\PurchasingController@edit', ['id' => $poid]);
    }
}
