<?php

namespace App\Http\Controllers\Trx;

use App\Http\Controllers\Controller;
use App\Models\Mst\Bahan;
use App\Models\Trx\SalesOrder;
use App\Models\Trx\SalesOrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PenjualanController extends Controller
{
    protected $bahan;
    protected $penjualan;
    protected $penjualanItem;

    public function __construct()
    {
        $this->bahan = new Bahan();
        $this->penjualan = new SalesOrder();
        $this->penjualanItem = new SalesOrderItem();
        parent::__construct();
    }

    public function index(Request $request){
        $data = array(
            "user" => Auth::user(),
            "rowData" => $this->penjualan->get(),
            "rowBahan" => $this->bahan->get()
        );

        return view('trx/penjualan/index', $data);
    }

    public function show($id)
    {
        $data = array(
            "data" => $this->penjualan->find($id),
            "rowBahan" => $this->bahan->get() ?? [],
            "rowItem" => $this->penjualanItem->getByPenjualan($id) ?? []
        );

        return view('trx/penjualan/detail', $data);
    }

    public function create()
    {
        $data = array(
            "kode" => $this->penjualan->generateCode()
        );
        return view('trx/penjualan/create', $data);
    }

    public function store(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $this->penjualan->$k = $v;
            }

            $this->penjualan->status = "WAITING APPROVE";
            $this->penjualan->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PenjualanController@edit', ['id' => $this->penjualan->id]);
    }

    public function edit($id)
    {
        $data = array(
            "user" => Auth::user(),
            "data" => $this->penjualan->find($id),
            "rowBahan" => $this->bahan->get() ?? [],
            "rowItem" => $this->penjualanItem->getByPenjualan($id) ?? []
        );

        return view('trx/penjualan/edit', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            $model = $this->penjualan->findOrFail($id);
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $model->$k = $v;
            }

            $model->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PenjualanController@index');
    }

    public function delete($id){
        $v = redirect()->action('Trx\PenjualanController@index');
        try{
            $modelItem = $this->penjualanItem->where("soid",$id);
            $modelItem->delete();
            $model = $this->penjualan->findOrFail($id);
            $model->delete();
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }
        return $v->with("successmessage", "Data berhasil dihapus!");
    }

    public function detailJson($id)
    {
        header('Content-Type: application/json');
        $rowData = $this->penjualanItem->find($id);
        echo json_encode( $rowData );
    }


//    ================================================================

    public function addItem(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method', 'stok']);

//            insert or update item
            if($data['id']) $this->penjualanItem = $this->penjualanItem->find($data['id']);
            foreach ($data as $k => $v){
                $this->penjualanItem->$k = $v;
            }
            $this->penjualanItem->status = "WAITING APPROVE";
            $this->penjualanItem->save();

//            update total
            $this->penjualan->updateTotal($data['soid']);
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Trx\PenjualanController@edit', ['id' => $data['soid']]);
    }

    public function deleteItem($id){
        $v = redirect()->action('Trx\PenjualanController@index');
        try{
            $model = $this->penjualanItem->findOrFail($id);
            $soid = $model->soid;
            $model->delete();

//            update total
            $this->penjualan->updateTotal($soid);
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }

        return redirect()->action('Trx\PenjualanController@edit', ['id' => $soid]);
    }

    public function approveItem($id, $status){
        $v = redirect()->action('Trx\PenjualanController@index');
        try{
            $model = $this->penjualanItem->findOrFail($id);
            $soid = $model->soid;
            $model->status = $status;
            $model->approved_at = now();
            $model->approved_by = Auth::user()->nama;
            $model->save();

//            update total
            $this->penjualan->updateStatus($soid);

            if($status == "APPROVED"){
//            kurangi stok
                $bahan = Bahan::find($model->bahanid);
                $qty = $model->qty;
                $bahan->stok -= $qty;
                $bahan->save();
            }
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }

        return redirect()->action('Trx\PenjualanController@edit', ['id' => $soid]);
    }
}
