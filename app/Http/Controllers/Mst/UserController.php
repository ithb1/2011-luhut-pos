<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\Mst\User;
use App\Models\Sys\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $user;
    protected $role;

    public function __construct()
    {
        $this->user = new User();
        $this->role = new Role();
        parent::__construct();
    }

    public function index(Request $request){
        $data = array(
            "rowData" => $this->user->get()
        );

        return view('mst/user/index', $data);
    }

    public function show($id)
    {
        $data = array(
            "data" => $this->user->find($id),
            "rowRole" => $this->role->get()
        );

        return view('mst/user/detail', $data);
    }

    public function create()
    {
        $data = array(
            "rowRole" => $this->role->get()
        );

        return view('mst/user/create', $data);
    }

    public function store(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                if($k == "password") $this->user->$k = Hash::make($v);
                else $this->user->$k = $v;
            }

            $this->user->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Mst\UserController@index');
    }

    public function edit($id)
    {
        $data = array(
            "data" => $this->user->find($id),
            "rowRole" => $this->role->get()
        );

        return view('mst/user/edit', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            $model = $this->user->findOrFail($id);
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                if($k == "password" && $model->password != "") $model->$k = Hash::make($v);
                else $model->$k = $v;
            }

            $model->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Mst\UserController@index');
    }

    public function delete($id){
        $v = redirect()->action('Mst\UserController@index');
        try{
            $model = $this->user->findOrFail($id);
            $model->delete();
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }
        return $v->with("successmessage", "Data berhasil dihapus!");
    }
}
