<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\Mst\Supplier;
use App\Models\Mst\User;
use App\Models\Sys\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SupplierController extends Controller
{
    protected $supplier;

    public function __construct()
    {
        $this->supplier = new Supplier();
        parent::__construct();
    }

    public function index(Request $request){
        $data = array(
            "rowData" => $this->supplier->get()
        );

        return view('mst/supplier/index', $data);
    }

    public function show($id)
    {
        $data = array(
            "data" => $this->supplier->find($id)
        );

        return view('mst/supplier/detail', $data);
    }

    public function create()
    {
        return view('mst/supplier/create');
    }

    public function store(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $this->supplier->$k = $v;
            }

            $this->supplier->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Mst\SupplierController@index');
    }

    public function edit($id)
    {
        $data = array(
            "data" => $this->supplier->find($id)
        );

        return view('mst/supplier/edit', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            $model = $this->supplier->findOrFail($id);
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $model->$k = $v;
            }

            $model->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Mst\SupplierController@index');
    }

    public function delete($id){
        $v = redirect()->action('Mst\SupplierController@index');
        try{
            $model = $this->supplier->findOrFail($id);
            $model->delete();
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }
        return $v->with("successmessage", "Data berhasil dihapus!");
    }
}
