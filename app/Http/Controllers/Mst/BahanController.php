<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\Mst\Bahan;
use Illuminate\Http\Request;

class BahanController extends Controller
{
    protected $bahan;

    public function __construct()
    {
        $this->bahan = new Bahan();
        parent::__construct();
    }

    public function index(Request $request){
        $data = array(
            "rowData" => $this->bahan->get()
        );

        return view('mst/bahan/index', $data);
    }

    public function show($id)
    {
        $data = array(
            "data" => $this->bahan->find($id)
        );

        return view('mst/bahan/detail', $data);
    }

    public function create()
    {
        $data = array(
            "kode" => $this->bahan->generateCode()
        );
        return view('mst/bahan/create', $data);
    }

    public function store(Request $request)
    {
        try {
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $this->bahan->$k = $v;
            }

            $this->bahan->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Mst\BahanController@index');
    }

    public function edit($id)
    {
        $data = array(
            "data" => $this->bahan->find($id)
        );

        return view('mst/bahan/edit', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            $model = $this->bahan->findOrFail($id);
            $data = $request->except(['_token', '_method']);

            foreach ($data as $k => $v){
                $model->$k = $v;
            }

            $model->save();
        }
        catch (\Exception $e) {
            return redirect()->back()->with('errormessage', $e->getMessage());
        }

        return redirect()->action('Mst\BahanController@index');
    }

    public function delete($id){
        $v = redirect()->action('Mst\BahanController@index');
        try{
            $model = $this->bahan->findOrFail($id);
            $model->delete();
        }
        catch (\Exception $e) {
            return $v->with("errormessage", $e->getMessage());
        }
        return $v->with("successmessage", "Data berhasil dihapus!");
    }

    public function detailJson($id)
    {
        header('Content-Type: application/json');
        $rowData = $this->bahan->find($id);
        echo json_encode( $rowData );
    }
}
