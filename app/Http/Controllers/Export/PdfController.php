<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\Mst\Bahan;
use App\Models\Mst\Supplier;
use App\Models\Trx\Pembayaran;
use App\Models\Trx\PembayaranItem;
use App\Models\Trx\Penerimaan;
use App\Models\Trx\PurchaseOrder;
use App\Models\Trx\PurchaseOrderItem;
use App\Models\Trx\SalesOrder;
use App\Models\Trx\SalesOrderItem;
use App\Models\Trx\SalesRetur;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function stok(Request $request)
    {
        $rowData = (new Bahan())->filter($request);

        $pdf = PDF::loadview('export/stok',['rowData'=>$rowData])->setPaper('a4', 'landscape');
        return $pdf->stream('laporan-stok.pdf');
    }

    public function purchasing(Request $request)
    {
        $rowData = (new PurchaseOrder())->filter($request);

        $pdf = PDF::loadview('export/purchasing',['rowData'=>$rowData,'filter'=>$request])->setPaper('a4', 'landscape');
        return $pdf->stream('laporan-purchasing.pdf');
    }

    public function penerimaan(Request $request)
    {
        $rowData = (new Penerimaan())->filter($request);

        $pdf = PDF::loadview('export/penerimaan',['rowData'=>$rowData,'filter'=>$request])->setPaper('a4', 'landscape');
        return $pdf->stream('laporan-penerimaan.pdf');
    }

    public function pembayaranutang(Request $request)
    {
        $rowData = (new Pembayaran())->filter($request);

        $pdf = PDF::loadview('export/pembayaranutang',['rowData'=>$rowData,'filter'=>$request])->setPaper('a4', 'landscape');
        return $pdf->stream('laporan-pembayaranutang.pdf');
    }

    public function penjualan(Request $request)
    {
        $rowData = (new SalesOrder())->filter($request);

        $pdf = PDF::loadview('export/penjualan',['rowData'=>$rowData])->setPaper('a4', 'landscape');
        return $pdf->stream('laporan-penjualan.pdf');
    }

    public function retur(Request $request)
    {
        $rowData = (new SalesRetur())->filter($request);

        $pdf = PDF::loadview('export/retur',['rowData'=>$rowData])->setPaper('a4', 'landscape');
        return $pdf->stream('laporan-retur.pdf');
    }

//    =====================================================================================================
//      DETAIL
//    =====================================================================================================

    public function purchasingDetail($id)
    {
        $rowData = PurchaseOrder::find($id);
        $rowsItem = (new PurchaseOrderItem())->getByPo($id);
        $rowSupplier = Supplier::find($rowData->supplier_id);
        $pdf = PDF::loadview('export/purchasingdetail',['rowData'=>$rowData, 'rowsItem'=>$rowsItem, 'rowSupplier'=>$rowSupplier])->setPaper('a4', 'landscape');
        return $pdf->stream('printout-purchasing.pdf');
    }

    public function returDetail($id)
    {
        $rowData = SalesRetur::find($id);
        $dataSo = SalesOrder::find($rowData->soid);
        $rowsItem = (new SalesOrderItem())->getByPenjualan($rowData->soid);
        $pdf = PDF::loadview('export/returdetail',['rowData'=>$rowData, 'dataSo'=>$dataSo, 'rowsItem'=>$rowsItem])->setPaper('a4', 'landscape');
        return $pdf->stream('printout-retur.pdf');
    }

    public function penjualanDetail($id)
    {
        $rowData = SalesOrder::find($id);
        $rowsItem = (new SalesOrderItem)->getByPenjualan($id);
        $pdf = PDF::loadview('export/penjualandetail',['rowData'=>$rowData, 'rowsItem'=>$rowsItem])->setPaper('a4', 'landscape');
        return $pdf->stream('printout-penjualan.pdf');
    }

    public function penerimaanDetail($id)
    {

        $rowData = Penerimaan::find($id);
        $rowPo = PurchaseOrder::find($rowData->poid);
        $rowSupplier = Supplier::find($rowPo->supplier_id);
        $rowsItem = (new PurchaseOrderItem())->getByPo($rowData->poid);
        $pdf = PDF::loadview('export/penerimaandetail',['rowData'=>$rowData, 'rowsItem'=>$rowsItem, 'rowPo'=>$rowPo, 'rowSupplier'=>$rowSupplier])->setPaper('a4', 'landscape');
        return $pdf->stream('printout-penerimaan.pdf');
    }

    public function pembayaranDetail($id)
    {
        $rowData = Pembayaran::find($id);
        $rowsItem = (new PembayaranItem())->getByPembayaran($id);
        $pdf = PDF::loadview('export/pembayarandetail',['rowData'=>$rowData, 'rowsItem'=>$rowsItem])->setPaper('a4', 'landscape');
        return $pdf->stream('printout-pembayaran.pdf');
    }
}
