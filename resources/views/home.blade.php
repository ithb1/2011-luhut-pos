@extends('layouts.backend')
@section('pagetitle', strtoupper("Beranda"))
@section('style')
    <style>
        @import url({{asset('roboto.css')}});
        .status-panel{
            padding: 20px;
        }
        .status-row{
            margin-top: 15px;
        }
        .input-group > .input-group-addon{
            vertical-align: top;
        }
        .calendar-icon{
            color: #ffffff;
        }
        .daterangepicker .input-mini{
            padding: 0 6px 0 28px !important;
        }
        .applyBtn {
            background: #4caf50;
        }
        .cancelBtn {
            background: #f44336;
        }
        .btn:not(.btn-raised).btn-success, .input-group-btn .btn:not(.btn-raised).btn-success,
        .btn:not(.btn-raised), .input-group-btn .btn:not(.btn-raised), .btn:not(.btn-raised).btn-default, .input-group-btn .btn:not(.btn-raised).btn-default{
            color: #ffffff;
        }
        .title > h3{
            font-size: 35px;
            font-family: 'Roboto Condensed', sans-serif;
            font-weight: 700;
        }
        .title > p{
            font-family: 'Roboto Condensed', sans-serif;
            font-weight: 400;
        }
        .small-box {
            padding-bottom: 10px;
        }
        .small-box .icon {
            font-size: 90px;
        }
        .small-box p>small {
            display: block;
            color: #f9f9f9;
            font-size: 10px;
            margin-top: 3px;
        }
        .info-box-number{
            font-size:30px;
        }
        .stat-box {
            position: relative;
            display: block;
            margin-bottom: 20px;
            box-shadow: 0 1px 6px 0 rgba(0,0,0,0.12), 0 1px 6px 0 rgba(0,0,0,0.12);
            border-radius: 2px;
            border: 0;
            padding: 10px 15px 10px 10px;
        }
    </style>
@endsection

@section('content')

    @if($rowKrisis && $user->roleid == 3)
        @foreach ($rowKrisis as $row)
        <div class="alert alert-{{ ($row->stok < 25)?"danger":"warning" }} fade in m-b-15">
            Stok <strong>{{ $row->kode." - ".$row->nama." - ".$row->warna }}</strong> tersisa <strong> {{ $row->stok }} </strong>. Segera melakukan restock.
            <span class="close" data-dismiss="alert">×</span>
        </div>
        @endforeach
    @endif

@endsection
<!-- SCRIPT JS  -->
@section('script')
<script type="text/javascript">
</script>

@endsection
