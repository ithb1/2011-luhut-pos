<div style="display:inline-block;max-width:172px;{{ $style ?? '' }}">
    <div class="btn btn-raised btn-dark btn-file {{ $disabled ?? '' }}">
        <span class="file-placeholder"><i class='fa fa-arrow-circle-o-up'></i>&nbsp;Browse Files</span>
        <input type="file" name="{{ $name ?? 'file' }}" id="{{ $id ?? '' }}" class="{{ ($class)??'file-upload' }}" {{isset($file)?'':(isset($required)?'':'required')}} accept={{ $accept ?? '' }} />
    </div>
    <div class="file-name" style="display:unset;white-space:wrap;overflow:hidden;">
        <a href="{{ $url ?? '' }}" target="_blank">{{ $file ?? '' }}</a>
    </div>
</div>
