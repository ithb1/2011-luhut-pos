<a href="{{ $href }}" target="_blank"
   class="{{ $disabled ?? ''}} btn btn-raised {{ $btn ?? '' }} btn-dark {{ $label ?? 'btn icon icon-left' }} ">@icon(["class" => "fa fa-" . $icon])@endicon
    {{--<i class="{{ config('view.theme') ? 'fa fa' : 'entypo' }}-{{$icon ?? "download"}}"></i>--}}&nbsp{{ $label ?? ''}}</a>
