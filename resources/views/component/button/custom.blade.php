<a href="{{ $href }}" class="btn btn-raised btn-{{ $color ?? 'primary' }} btn-icon icon-left">@icon(["class" => "fa fa-gear"])@endicon &nbsp{{ $label ?? "custom" }}</a>
