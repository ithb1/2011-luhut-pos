<a href="{{ $href }}" class="btn btn-raised btn-danger {{ $btn ?? '' }}btn-icon icon-left">@icon(["class" => "fa fa-trash"])@endicon &nbsp{{ $label ?? "DELETE" }}</a>
