<a href="{{ $href }}"
class="btn btn-raised btn-success btn-icon icon-left">@icon(["class" => $class ?? 'fa fa-plus' ])@endicon &nbsp{{ $label ?? 'TAMBAH' }}</a>
