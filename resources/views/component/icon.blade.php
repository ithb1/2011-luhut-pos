@php
    if(config('view.theme') != "material"){
       $exploded = explode("-", $class);
       $exploded[0] = "entypo";

       $class = "$exploded[0]-$exploded[1]";
   }
@endphp
<i class="{{$class}}"></i>