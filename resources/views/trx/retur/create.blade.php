@extends('layouts.backend')

@section('pagetitle', 'Tambah Retur')

@section('content')
    <form action="{{ url("trx/retur/") }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Penjualan</label>
            <div class="col-sm-4">
                <select name="soid" id="soid" required class="form-control" onchange="getDetailSo()">
                    <option value="">- Sales Order -</option>
                    @foreach($rowSo as $row)
                        <option value="{{ $row->id }}" >{{ $row->nomor }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Pemesanan</label>
            <div class="col-sm-4">
                <input id="tanggal_penjualan" name="tanggal_penjualan" type="text" class="form-control" placeholder="tanggal_penjualan" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-4">
                <input id="keterangan" name="keterangan" type="text" class="form-control" placeholder="keterangan" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Retur</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $kode }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Retur</label>
            <div class="col-sm-4">
                <input name="tanggal" type="date" class="form-control" placeholder="tanggal" >
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/retur")])@endcomponent
            @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
        </div>
    </form>
@endsection

@section('script')
    <script>

        function clearForm() {
            $('#id').val("");
            $('#qty').val(0);
            $('#harga').val(0);
            $('#total').val(0);
        }

        function getDetailSo() {
            var id = $('#soid').val();
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('trx/retur/detailjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    console.log(data.so);
                    $('#tanggal_penjualan').val(data.so.tanggal);
                    $('#keterangan').val(data.so.keterangan);
                }
            });
        }

        function getDetail(id) {
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('trx/penjualan/detailjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    $('#id').val(data.id);
                    $('#bahanid').val(data.bahanid);
                    $('#qty').val(data.qty);
                    $('#harga').val(data.harga);
                    $('#total').val(data.total);
                }
            });
        }

        function calculate() {
            var harga = $('#harga').val();
            var qty = $('#qty').val();
            $('#total').val(harga * qty);
        }

    </script>
@endsection
