@extends('layouts.backend')

@section('pagetitle', 'Detail Retur')

@section('content')
    <form action="{{ url("trx/retur/".$data->id) }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf
        @method('PUT')

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Penjualan</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $dataSo->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Pemesanan</label>
            <div class="col-sm-4">
                <input id="tanggal" name="tanggal_pemesanan" type="text" class="form-control" placeholder="tanggal_pemesanan" value="{{ $dataSo->tanggal }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-4">
                <input id="keterangan" name="keterangan" type="text" class="form-control" placeholder="keterangan" value="{{ $dataSo->keterangan }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Retur</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $data->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Retur</label>
            <div class="col-sm-4">
                <input name="tanggal" type="date" class="form-control" placeholder="tanggal" value="{{ $data->tanggal }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Grand Total</label>
            <div class="col-sm-4">
                <input name="total" type="text" class="form-control" placeholder="total" value="{{ number_format($data->total, 0 ,',','.') }}" disabled>
            </div>
        </div>


        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/retur")])@endcomponent
            @component("component.button.custom",["href"=>url("export/retur/".$data->id), "color"=>"default", "label"=>"PRINT"])@endcomponent
        </div>
    </form>
    <hr>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Bahan</th>
            <th>Qty Penjualan (Kg)</th>
            <th>Qty Retur (Kg)</th>
            <th>Total</th>
            <th>Keterangan Retur</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowSoItem as $index => $row)
            @php
                $bahanModel = \App\Models\Mst\Bahan::find($row->bahanid);
                $returItem = (new \App\Models\Trx\SalesReturItem())->getByReturDanPenjualan($data->id, $row->id);
                $id = ($returItem) ? $returItem->id : '';
                $bahan = $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna;
            @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $bahan }}</td>
                <td>{{ number_format($row->qty, 0 ,',','.') }}</td>
                <td>{{ ($returItem) ? number_format($returItem->qty, 0 ,',','.') : 0 }}</td>
                <td>{{ ($returItem) ? number_format($returItem->total, 0 ,',','.') : 0 }}</td>
                <td>{{ ($returItem) ? $returItem->keterangan : "" }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('script')
    <script>
    </script>
@endsection
