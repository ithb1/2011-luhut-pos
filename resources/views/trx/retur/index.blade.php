@extends('layouts.backend')

@section('pagetitle', strtoupper("Daftar Retur"))

@section('content')
    <div class="button-list">
        @if($user->roleid != 5)
        @component("component.button.add",["href"=>url('trx/retur/create')])@endcomponent
        @endif
        @if($user->roleid == 1 || $user->roleid == 5)
        @component("component.button.openmodal",["datatarget"=>"#modalForm", "onclick"=>"clearForm()" , "label"=>"export", "color"=>"default" ])@endcomponent
        @endif
    </div>
    <br>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Nomor Penjualan</th>
            <th>Tanggal Penjualan</th>
            <th>Nomor Retur</th>
            <th>Tanggal Retur</th>
            <th>Keterangan</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            @php
                $dataSo = \App\Models\Trx\SalesOrder::find($row->soid);
            @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $dataSo->nomor }}</td>
                <td>{{ $dataSo->tanggal }}</td>
                <td>{{ $row->nomor }}</td>
                <td>{{ $row->tanggal }}</td>
                <td>{{ $row->keterangan }}</td>
                <td class="text-center">
                    @component("component.button.view",["href"=>url("trx/retur/".$row->id)])@endcomponent
                    @if($user->roleid != 5)
                    @component("component.button.edit",["href"=>url("trx/retur/".$row->id."/edit")])@endcomponent
                    @component("component.button.delete",["href"=>url("trx/retur/delete/".$row->id)])@endcomponent
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <!-- Modal -->
    <div id="modalForm" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Filter</h4>
                </div>
                <form action="{{ url("export/retur") }}" method="post" role="form" class="form-horizontal form-groups-bordered">
                    @csrf
                    <div class="modal-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Tanggal Awal</label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" id="tgl_awal" placeholder="tgl_awal" name="tgl_awal" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Tanggal Akhir</label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" id="tgl_akhir" placeholder="tgl_akhir" name="tgl_akhir" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Bahan</label>
                                        <div class="col-sm-8">
                                            <select name="bahanid" id="bahanid" required class="form-control">
                                                <option value="all">- semua -</option>
                                                @foreach($rowBahan as $row)
                                                    <option value="{{ $row->id }}" >{{ $row->nama.' - '.$row->jenis_benang.' - '.$row->warna }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        @component('component.button.submit', ["label" => "export"]) @endcomponent
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>

    </script>
@endsection
