@extends('layouts.backend')

@section('pagetitle', 'Edit Retur')

@section('content')
    <form action="{{ url("trx/retur/".$data->id) }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf
        @method('PUT')

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Penjualan</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $dataSo->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Pemesanan</label>
            <div class="col-sm-4">
                <input id="tanggal" name="tanggal_pemesanan" type="text" class="form-control" placeholder="tanggal_pemesanan" value="{{ $dataSo->tanggal }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-4">
                <input id="keterangan" name="keterangan" type="text" class="form-control" placeholder="keterangan" value="{{ $dataSo->keterangan }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Retur</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $data->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Retur</label>
            <div class="col-sm-4">
                <input name="tanggal" type="date" class="form-control" placeholder="tanggal" value="{{ $data->tanggal }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Grand Total</label>
            <div class="col-sm-4">
                <input name="total" type="text" class="form-control" placeholder="total" value="{{ number_format($data->total, 0 ,',','.') }}" disabled>
            </div>
        </div>


        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/retur")])@endcomponent
            @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
        </div>
    </form>
    <hr>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Bahan</th>
            <th>Qty Penjualan (Kg)</th>
            <th>Qty Retur (Kg)</th>
            <th>Total</th>
            <th>Keterangan Retur</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowSoItem as $index => $row)
            @php
                $bahanModel = \App\Models\Mst\Bahan::find($row->bahanid);
                $returItem = (new \App\Models\Trx\SalesReturItem())->getByReturDanPenjualan($data->id, $row->id);
                $id = ($returItem) ? $returItem->id : '';
                $bahan = $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna;
            @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $bahan }}</td>
                <td>{{ number_format($row->qty, 0 ,',','.') }}</td>
                <td>{{ ($returItem) ? number_format($returItem->qty, 0 ,',','.') : 0 }}</td>
                <td>{{ ($returItem) ? number_format($returItem->total, 0 ,',','.') : 0 }}</td>
                <td>{{ ($returItem) ? $returItem->keterangan : "" }}</td>
                <td class="text-center">
                    @component("component.button.openmodal",["datatarget"=>"#modalForm", "onclick"=>"getDetail(this)" , "color"=>"danger" , "label"=>($row->qty < 25) ? "kurang dari 25Kg" : "RETUR" , "data"=>"data-soid=$row->soid data-soitemid=$row->id data-qty=$row->qty data-bahan=$bahanModel->kode data-harga=$bahanModel->harga_jual data-id=$id", "disabled"=>($row->qty < 25) ? "disabled" : "" ])@endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <!-- Modal -->
    <div id="modalForm" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Konfirmasi Item</h4>
                </div>
                <form action="{{ url("trx/retur_item/") }}" method="post" role="form" class="form-horizontal form-groups-bordered">
                    @csrf
                    <div class="modal-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Bahan</label>
                                        <div class="col-sm-8">
                                            <input type="hidden" class="form-control" id="id" placeholder="id" name="id">
                                            <input type="hidden" class="form-control" id="returid" placeholder="returid" name="returid" value="{{ $data->id }}">
                                            <input type="hidden" class="form-control" id="soitemid" placeholder="soitemid" name="soitemid" >
                                            <input type="hidden" class="form-control" id="soid" placeholder="soid" name="soid" >
                                            <input type="text" class="form-control" id="bahan" placeholder="bahan" name="bahan" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Qty Penjualan (Kg)</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="qtySo" placeholder="qtySo" name="qtySo" value="" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Qty Retur (Kg)</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="qty" placeholder="qty" name="qty" value=""  onchange="calculate()">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Harga Bahan</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="harga" placeholder="harga" value="" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Total</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="total" placeholder="total" name="total" value="" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Keterangan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="keterangan" placeholder="keterangan" name="keterangan" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <div class="modal-footer">
                        @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>
        function getDetail(ini) {
            var id = $(ini).attr('data-id');
            var soid = $(ini).attr('data-soid');
            var soitemid = $(ini).attr('data-soitemid');
            var bahan = $(ini).attr('data-bahan');
            var harga = $(ini).attr('data-harga');
            var qty = $(ini).attr('data-qty');
            $('#id').val(id);
            $('#soid').val(soid);
            $('#soitemid').val(soitemid);
            $('#bahan').val(bahan);
            $('#qtySo').val(qty);
            $('#harga').val(harga);
            $('#qty').val(0);
        }

        function calculate() {
            var harga = $('#harga').val();
            var qty = $('#qty').val();
            $('#total').val(harga * qty);
        }
    </script>
@endsection
