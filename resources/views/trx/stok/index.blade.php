@extends('layouts.backend')

@section('pagetitle', strtoupper("Daftar Stok Barang"))

@section('content')
    <div class="button-list">
        @if($user->roleid == 1 || $user->roleid == 5)
            @component("component.button.openmodal",["datatarget"=>"#modalForm", "onclick"=>"clearForm()" , "label"=>"export", "color"=>"default" ])@endcomponent
        @endif
    </div>
    <br>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Jenis Benang</th>
            <th>Warna</th>
            <th>Stok (Kg)</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $row->kode }}</td>
                <td>{{ $row->nama }}</td>
                <td>{{ $row->jenis_benang }}</td>
                <td>{{ $row->warna }}</td>
                <td>{{ number_format($row->stok, 0, ',', '.') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <!-- Modal -->
    <div id="modalForm" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Filter</h4>
                </div>
                <form action="{{ url("export/stok") }}" method="post" role="form" class="form-horizontal form-groups-bordered">
                    @csrf
                    <div class="modal-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Nama</label>
                                        <div class="col-sm-8">
                                            <select name="nama" id="nama" required class="form-control">
                                                <option value="all">- semua -</option>
                                                @foreach($rowNama as $row)
                                                    <option value="{{ $row->nama }}" >{{ $row->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Jenis Benang</label>
                                        <div class="col-sm-8">
                                            <select name="jenis_benang" id="jenis_benang" required class="form-control">
                                                <option value="all">- semua -</option>
                                                @foreach($rowBenang as $row)
                                                    <option value="{{ $row->jenis_benang }}" >{{ $row->jenis_benang }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Warna</label>
                                        <div class="col-sm-8">
                                            <select name="warna" id="warna" required class="form-control">
                                                <option value="all">- semua -</option>
                                                @foreach($rowWarna as $row)
                                                    <option value="{{ $row->warna }}" >{{ $row->warna }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        @component('component.button.submit', ["label" => "export"]) @endcomponent
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>

    </script>
@endsection
