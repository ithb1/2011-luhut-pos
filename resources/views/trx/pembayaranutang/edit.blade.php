@extends('layouts.backend')

@section('pagetitle', 'Edit Pembayaran Utang')

@section('content')
    <form action="{{ url("trx/pembayaran/".$data->id) }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf
        @method('PUT')

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Pembayaran</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $data->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Pembayaran</label>
            <div class="col-sm-4">
                <input name="tanggal" type="date" class="form-control" placeholder="tanggal"  value="{{ $data->tanggal }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Total</label>
            <div class="col-sm-4">
                <input name="total" type="number" class="form-control" placeholder="total"  value="{{ $maxTotal ?? $data->total }}" min=0 max={{ $maxTotal }}>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Jenis</label>
            <div class="col-sm-4">
                <select name="jenis" id="jenis" required class="form-control">
                    <option value="">- Jenis Pembayaran -</option>
                    <option value="CASH" @if($data->jenis == "CASH") selected @endif>CASH</option>
                    <option value="TRANSFER" @if($data->jenis == "TRANSFER") selected @endif>TRANSFER</option>
                </select>
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/pembayaran")])@endcomponent
            @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
        </div>
    </form>
    <hr>
    <div class="button-list">
        @component("component.button.openmodal",["datatarget"=>"#modalForm", "onclick"=>"clearForm()" , "label"=>"tambah" ])@endcomponent
    </div>
    <br>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Nomor PO</th>
            <th>Supplier</th>
            <th>Total</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowItem as $index => $row)
            @php
                $poModel = \App\Models\Trx\PurchaseOrder::find($row->poid);
                $supplierModel = \App\Models\Mst\Supplier::find($poModel->supplier_id);
            @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $poModel->nomor }}</td>
                <td>{{ $supplierModel->nama }}</td>
                <td>{{ $poModel->total }}</td>
                <td class="text-center">
                    @component("component.button.openmodal",["datatarget"=>"#modalForm", "onclick"=>"getDetail(this)" , "color"=>"warning" , "label"=>"edit",  "data"=>" data-id=$row->id data-poid=$row->poid data-total=$poModel->total "  ])@endcomponent
                    @component("component.button.delete",["href"=>url("trx/pembayaran_item/delete/".$row->id)])@endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <!-- Modal -->
    <div id="modalForm" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Konfirmasi Item</h4>
                </div>
                <form action="{{ url("trx/pembayaran_item/") }}" method="post" role="form" class="form-horizontal form-groups-bordered">
                    @csrf
                    <div class="modal-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Nomor Purchase Order</label>
                                        <div class="col-sm-8">
                                            <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                                            <input type="hidden" class="form-control" id="pembayaranid" placeholder="pembayaranid" name="pembayaranid" value="{{ $data->id }}">
                                            <select name="poid" id="poid" required class="form-control" onchange="getDetailPo()">
                                                <option value="">- Purchase Order -</option>
                                                @foreach($rowPo as $row)
                                                    <option value="{{ $row->id }}" >{{ $row->nomor }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Total</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="totalPo" placeholder="totalPo" name="totalPo" value="" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <div class="modal-footer">
                        @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>

        function clearForm() {
            $('#id').val("");
            $('#poid').val("");
            $('#totalPo').val(0);
        }

        function getDetail(ini) {
            var id = $(ini).attr('data-id');
            var poid = $(ini).attr('data-poid');
            var total = $(ini).attr('data-total');
            $('#id').val(id);
            $('#poid').val(poid);
            $('#totalPo').val(total);
        }

        function getDetailPo() {
            var id = $('#poid').val();
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('trx/purchasing/headerjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    $('#totalPo').val(data.total);
                }
            });
        }
    </script>
@endsection
