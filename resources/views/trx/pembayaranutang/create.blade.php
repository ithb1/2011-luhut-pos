@extends('layouts.backend')

@section('pagetitle', 'Tambah Pembayaran Utang')

@section('content')
    <form action="{{ url("trx/pembayaran/") }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Pembayaran</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $kode }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Pembayaran</label>
            <div class="col-sm-4">
                <input name="tanggal" type="date" class="form-control" placeholder="tanggal" >
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Total</label>
            <div class="col-sm-4">
                <input name="total" type="number" class="form-control" placeholder="total" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Jenis</label>
            <div class="col-sm-4">
                <select name="jenis" id="jenis" required class="form-control" onchange="getDetailPo()">
                    <option value="">- Jenis Pembayaran -</option>
                    <option value="CASH">CASH</option>
                    <option value="TRANSFER">TRANSFER</option>
                </select>
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/pembayaran")])@endcomponent
            @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
        </div>
    </form>
@endsection

@section('script')
    <script>

        function clearForm() {
            $('#id').val("");
            $('#qty').val(0);
            $('#harga').val(0);
            $('#total').val(0);
        }

        function getDetailPo() {
            var id = $('#poid').val();
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('trx/pembayaran/detailjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    console.log(data.po);
                    $('#nama_supplier').val(data.po.nama_supplier);
                    $('#tanggal_pemesanan').val(data.po.tanggal);
                    $('#keterangan').val(data.po.keterangan);
                }
            });
        }

        function getDetail(id) {
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('trx/purchasing/detailjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    $('#id').val(data.id);
                    $('#bahanid').val(data.bahanid);
                    $('#qty').val(data.qty);
                    $('#harga').val(data.harga);
                    $('#total').val(data.total);
                }
            });
        }

        function calculate() {
            var harga = $('#harga').val();
            var qty = $('#qty').val();
            $('#total').val(harga * qty);
        }

    </script>
@endsection
