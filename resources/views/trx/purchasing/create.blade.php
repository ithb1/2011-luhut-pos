@extends('layouts.backend')

@section('pagetitle', 'Tambah Purchasing')

@section('content')
			<form action="{{ url("trx/purchasing") }}" method="post" role="form" class="form-horizontal form-groups-bordered" id="formbidang">
				@csrf

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Nomor</label>
					<div class="col-sm-4">
                        <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $kode }}" readonly>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Supplier</label>
					<div class="col-sm-4">
						<select name="supplier_id" id="supplier_id" class="select2 form-control">
                            <option selected disabled>Pilih Supplier</option>
                            @foreach($rowSupplier as $row)
                                <option value="{{ $row->id }}">{{ $row->nama }}</option>
                            @endforeach
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Tanggal Pemesanan</label>
					<div class="col-sm-4">
                        <input name="tanggal" type="date" class="form-control" placeholder="tanggal" >
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Keterangan</label>
					<div class="col-sm-4">
                        <input name="keterangan" type="text" class="form-control" placeholder="keterangan">
					</div>
				</div>

				<br>
				<div class="button-list">
					@component('component.button.back', ['href' => url("trx/purchasing")])@endcomponent
					@component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
				</div>
			</form>
@endsection

@section('script')
	<script>

    </script>
@endsection
