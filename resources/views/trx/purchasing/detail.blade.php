@extends('layouts.backend')

@section('pagetitle', 'Detail Purchasing')

@section('content')
    <form action="{{ url("trx/purchasing/".$data->id) }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf
        @method('PUT')


        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $data->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Supplier</label>
            <div class="col-sm-4">
                <select name="supplier_id" id="supplier_id" class="select2 form-control" disabled>
                    <option selected disabled>Pilih Supplier</option>
                    @foreach($rowSupplier as $row)
                        <option value="{{ $row->id }}" @if($data->supplier_id == $row->id) selected @endif>{{ $row->nama }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Pemesanan</label>
            <div class="col-sm-4">
                <input name="tanggal" type="text" class="form-control" placeholder="tanggal" value="{{ $data->tanggal }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-4">
                <input name="keterangan" type="text" class="form-control" placeholder="keterangan" value="{{ $data->keterangan }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Grand Total</label>
            <div class="col-sm-4">
                <input name="total" type="text" class="form-control" placeholder="total" value="{{ number_format($data->total, 0 ,',','.') }}" disabled>
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/purchasing")])@endcomponent
            @component("component.button.custom",["href"=>url("export/purchasing/".$data->id), "color"=>"default", "label"=>"PRINT"])@endcomponent
        </div>
    </form>
    <hr>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Bahan</th>
            <th>Qty (roll)</th>
            <th>Harga</th>
            <th>Total</th>
            <th>Keterangan</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowItem as $index => $row)
            @php $bahanModel = \App\Models\Mst\Bahan::find($row->bahanid) @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna }}</td>
                <td>{{ number_format($row->qty, 0 ,',','.') }}</td>
                <td>{{ number_format($row->harga, 0 ,',','.') }}</td>
                <td>{{ number_format($row->total, 0 ,',','.') }}</td>
                <td>{{ $row->keterangan }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('script')
    <script>

        function clearForm() {
            $('#id').val("");
            $('#qty').val(0);
            $('#harga').val(0);
            $('#total').val(0);
        }

        function getDetailBahan() {
            var id = $('#bahanid').val();
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('mst/bahan/detailjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    $('#harga').val(data.harga_beli);
                }
            });
        }

        function getDetail(id) {
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('trx/purchasing/detailjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    $('#id').val(data.id);
                    $('#bahanid').val(data.bahanid);
                    $('#qty').val(data.qty);
                    $('#harga').val(data.harga);
                    $('#total').val(data.total);
                }
            });
        }

        function calculate() {
            var harga = $('#harga').val();
            var qty = $('#qty').val();
            $('#total').val(harga * qty);
        }

    </script>
@endsection
