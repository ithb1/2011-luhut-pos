@extends('layouts.backend')

@section('pagetitle', 'Tambah Penjualan')

@section('content')
    <form action="{{ url("trx/penjualan") }}" method="post" role="form" class="form-horizontal form-groups-bordered" id="formbidang">
        @csrf

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Penjualan</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $kode }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Penjualan</label>
            <div class="col-sm-4">
                <input name="tanggal" type="date" class="form-control" placeholder="tanggal" >
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-4">
                <input name="keterangan" type="text" class="form-control" placeholder="keterangan">
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/penjualan")])@endcomponent
            @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
        </div>
    </form>
@endsection

@section('script')
    <script>

    </script>
@endsection
