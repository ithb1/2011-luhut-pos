@extends('layouts.backend')

@section('pagetitle', 'Ubah Penjualan')

@section('content')
    <form action="{{ url("trx/penjualan/".$data->id) }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf
        @method('PUT')

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Penjualan</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $data->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Penjualan</label>
            <div class="col-sm-4">
                <input name="tanggal" type="date" class="form-control" placeholder="tanggal" value="{{ $data->tanggal }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-4">
                <input name="keterangan" type="text" class="form-control" placeholder="keterangan" value="{{ $data->keterangan }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Grand Total</label>
            <div class="col-sm-4">
                <input name="total" type="text" class="form-control" placeholder="total" value="{{ number_format($data->total, 0 ,',','.') }}" disabled>
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/penjualan")])@endcomponent
            @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
        </div>
    </form>
    <hr>
    <div class="button-list">
        @if($user->roleid == 4)
            @component("component.button.openmodal",["datatarget"=>"#modalForm", "onclick"=>"clearForm()" , "label"=>"tambah" ])@endcomponent
        @endif
    </div>
    <br>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Bahan</th>
            <th>Qty (Kg)</th>
            <th>Harga</th>
            <th>Total</th>
            <th>Status</th>
            <th>Keterangan</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowItem as $index => $row)
            @php $bahanModel = \App\Models\Mst\Bahan::find($row->bahanid) @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna }}</td>
                <td>{{ number_format($row->qty, 0 ,',','.') }}</td>
                <td>{{ number_format($row->harga, 0 ,',','.') }}</td>
                <td>{{ number_format($row->total, 0 ,',','.') }}</td>
                <td>
                    @if($row->status == 'APPROVED')
                        <div class='label label-success input-md'><strong>APPROVED</strong></div>
                    @elseif($row->status == 'REJECTED')
                        <div class='label label-danger input-md'><strong>REJECTED</strong></div>
                    @else
                        <div class='label label-warning input-md'><strong>WAITING APPROVE</strong></div>
                    @endif
                </td>
                <td>{{ $row->keterangan }}</td>
                <td class="text-center">
                    @if($row->status == 'WAITING APPROVE' && $user->roleid == 2)
                        @component("component.button.custom",["href"=>url("trx/penjualan_item/approve/".$row->id."/APPROVED"), "color"=>"success" , "label"=>"APPROVE" ])@endcomponent
                        @component("component.button.custom",["href"=>url("trx/penjualan_item/approve/".$row->id."/REJECTED"), "color"=>"danger" , "label"=>"REJECT" ])@endcomponent
                        |
                    @elseif($user->roleid == 4)
                        @component("component.button.openmodal",["datatarget"=>"#modalForm", "onclick"=>"getDetail($row->id)" , "color"=>"warning" , "label"=>"edit" ])@endcomponent
                        @component("component.button.delete",["href"=>url("trx/penjualan_item/delete/".$row->id)])@endcomponent
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <!-- Modal -->
    <div id="modalForm" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Item</h4>
                </div>
                <form action="{{ url("trx/penjualan_item/") }}" method="post" role="form" class="form-horizontal form-groups-bordered">
                    @csrf
                    <div class="modal-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Bahan</label>
                                        <div class="col-sm-8">
                                            <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                                            <input type="hidden" class="form-control" id="soid" placeholder="soid" name="soid" value="{{ $data->id }}">
                                            <select name="bahanid" id="bahanid" required class="form-control" onchange="getDetailBahan()">
                                                <option value="">- Bahan -</option>
                                                @foreach($rowBahan as $row)
                                                    <option value="{{ $row->id }}" >{{ $row->kode." - ".$row->nama." - ".$row->warna }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Qty (Kg)</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="qty" placeholder="qty" name="qty" value="" onchange="calculate()">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Harga</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="harga" placeholder="harga" name="harga" value="" readonly>
                                        </div>
                                    </div>
{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-sm-4 col-form-label">Stok</label>--}}
{{--                                        <div class="col-sm-8">--}}
{{--                                            <input type="number" class="form-control" id="stok" placeholder="stok" name="stok" value="" readonly>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Total</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="total" placeholder="total" name="total" value="" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Keterangan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="keterangan" placeholder="keterangan" name="keterangan" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <div class="modal-footer">
                        @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>

        function clearForm() {
            $('#id').val("");
            $('#qty').val(0);
            $('#harga').val(0);
            $('#stok').val(0);
            $('#total').val(0);
        }

        function getDetailBahan() {
            var id = $('#bahanid').val();
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('mst/bahan/detailjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    $('#harga').val(data.harga_jual);
                    // $('#stok').val(data.stok);
                }
            });
        }

        function getDetail(id) {
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('trx/penjualan/detailjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    $('#id').val(data.id);
                    $('#bahanid').val(data.bahanid);
                    $('#qty').val(data.qty);
                    $('#harga').val(data.harga);
                    $('#total').val(data.total);
                }
            });
        }

        function calculate() {
            var harga = $('#harga').val();
            var qty = $('#qty').val();
            $('#total').val(harga * qty);
        }

    </script>
@endsection
