@extends('layouts.backend')

@section('pagetitle', 'Detail Penerimaan')

@section('content')
    <form action="{{ url("trx/penerimaan/".$data->id) }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Purchasing</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $dataPo->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Supplier</label>
            <div class="col-sm-4">
                <input id="nama_supplier" name="nama_supplier" type="text" class="form-control" placeholder="nama_supplier" value="{{ $dataSupplier->nama }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Pemesanan</label>
            <div class="col-sm-4">
                <input id="tanggal" name="tanggal_pemesanan" type="text" class="form-control" placeholder="tanggal_pemesanan" value="{{ $dataPo->tanggal }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-4">
                <input id="keterangan" name="keterangan" type="text" class="form-control" placeholder="keterangan" value="{{ $dataPo->keterangan }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Penerimaan</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $data->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Penerimaan</label>
            <div class="col-sm-4">
                <input name="tanggal" type="date" class="form-control" placeholder="tanggal" value="{{ $data->tanggal }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Status Penerimaan</label>
            <div class="col-sm-4">
                <input name="status" type="text" class="form-control" placeholder="status" value="{{ $dataPo->status_penerimaan }}" disabled>
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/penerimaan")])@endcomponent
            @component("component.button.custom",["href"=>url("export/penerimaan/".$data->id), "color"=>"default", "label"=>"PRINT"])@endcomponent
        </div>
    </form>
    <hr>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Bahan</th>
            <th>Qty Pemesanan (roll)</th>
            <th>Qty Penerimaan (roll)</th>
            <th>Status</th>
            <th>Keterangan Pemesanan</th>
            <th>Keterangan Penerimaan</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowPoItem as $index => $row)
            @php
                $bahanModel = \App\Models\Mst\Bahan::find($row->bahanid);
                $penerimaanItem = (new \App\Models\Trx\PenerimaanItem)->getByPenerimaanDanPo($data->id, $row->id);
                $bahan = $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna;
            @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $bahan }}</td>
                <td>{{ number_format($row->qty, 0 ,',','.') }}</td>
                <td>{{ ($penerimaanItem) ? number_format($penerimaanItem->qty, 0 ,',','.') : 0 }}</td>
                <td>{{ ($penerimaanItem && $penerimaanItem->status) ? "SESUAI" : "BELUM SESUAI" }}</td>
                <td>{{ $row->keterangan }}</td>
                <td>{{ ($penerimaanItem) ? $penerimaanItem->keterangan : "" }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('script')
    <script>
    </script>
@endsection
