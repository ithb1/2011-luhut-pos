@extends('layouts.backend')

@section('pagetitle', strtoupper("Daftar Penerimaan"))

@section('content')
    <div class="button-list">
        @if($user->roleid != 5)
        @component("component.button.add",["href"=>url('trx/penerimaan/create')])@endcomponent
        @endif
        @if($user->roleid == 1 || $user->roleid == 5)
        @component("component.button.openmodal",["datatarget"=>"#modalForm", "onclick"=>"clearForm()" , "label"=>"export", "color"=>"default" ])@endcomponent
        @endif
    </div>
    <br>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Nomor</th>
            <th>Supplier</th>
            <th>Tanggal Pemesanan</th>
            <th>Tanggal Penerimaan</th>
            <th>Keterangan</th>
            <th>Status Penerimaan</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            @php
                $dataPo = \App\Models\Trx\PurchaseOrder::find($row->poid);
            @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $row->nomor }}</td>
                <td>{{ \App\Models\Mst\Supplier::find($dataPo->supplier_id)->nama }}</td>
                <td>{{ $dataPo->tanggal }}</td>
                <td>{{ $row->tanggal }}</td>
                <td>{{ $row->keterangan }}</td>
                <td>
                    @if($dataPo->status_penerimaan != 'DONE')
                        <div class='label label-warning input-md'><strong>PROGRESS</strong></div>
                    @else
                        <div class='label label-success input-md'><strong>DONE</strong></div>
                    @endif
                </td>
                <td class="text-center">
                    @component("component.button.view",["href"=>url("trx/penerimaan/".$row->id)])@endcomponent
                    @if($user->roleid != 5)
                    @component("component.button.edit",["href"=>url("trx/penerimaan/".$row->id."/edit")])@endcomponent
                    @component("component.button.delete",["href"=>url("trx/penerimaan/delete/".$row->id)])@endcomponent
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <!-- Modal -->
    <div id="modalForm" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Filter</h4>
                </div>
                <form action="{{ url("export/penerimaan") }}" method="post" role="form" class="form-horizontal form-groups-bordered">
                    @csrf
                    <div class="modal-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Tanggal Awal</label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" id="tgl_awal" placeholder="tgl_awal" name="tgl_awal" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Tanggal Akhir</label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" id="tgl_akhir" placeholder="tgl_akhir" name="tgl_akhir" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Supplier</label>
                                        <div class="col-sm-8">
                                            <select name="supplier_id" id="supplier_id" required class="form-control">
                                                <option value="all">- semua -</option>
                                                @foreach($rowSupplier as $row)
                                                    <option value="{{ $row->id }}" >{{ $row->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        @component('component.button.submit', ["label" => "export"]) @endcomponent
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>

    </script>
@endsection
