@extends('layouts.backend')

@section('pagetitle', 'Edit Penerimaan')

@section('content')
    <form action="{{ url("trx/penerimaan/".$data->id) }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf
        @method('PUT')

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Purchasing</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $dataPo->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Supplier</label>
            <div class="col-sm-4">
                <input id="nama_supplier" name="nama_supplier" type="text" class="form-control" placeholder="nama_supplier" value="{{ $dataSupplier->nama }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Pemesanan</label>
            <div class="col-sm-4">
                <input id="tanggal" name="tanggal_pemesanan" type="text" class="form-control" placeholder="tanggal_pemesanan" value="{{ $dataPo->tanggal }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-4">
                <input id="keterangan" name="keterangan" type="text" class="form-control" placeholder="keterangan" value="{{ $dataPo->keterangan }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Penerimaan</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $data->nomor }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Penerimaan</label>
            <div class="col-sm-4">
                <input name="tanggal" type="date" class="form-control" placeholder="tanggal" value="{{ $data->tanggal }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Status Penerimaan</label>
            <div class="col-sm-4">
                <input name="status" type="text" class="form-control" placeholder="status" value="{{ $dataPo->status_penerimaan }}" disabled>
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/penerimaan")])@endcomponent
            @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
        </div>
    </form>
    <hr>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Bahan</th>
            <th>Qty Pemesanan (roll)</th>
            <th>Qty Penerimaan (roll)</th>
            <th>Status</th>
            <th>Keterangan Pemesanan</th>
            <th>Keterangan Penerimaan</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowPoItem as $index => $row)
            @php
                $bahanModel = \App\Models\Mst\Bahan::find($row->bahanid);
                $penerimaanItem = (new \App\Models\Trx\PenerimaanItem)->getByPenerimaanDanPo($data->id, $row->id);
                $bahan = $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna;
            @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $bahan }}</td>
                <td>{{ number_format($row->qty, 0 ,',','.') }}</td>
                <td>{{ ($penerimaanItem) ? number_format($penerimaanItem->qty, 0 ,',','.') : 0 }}</td>
                <td>{{ ($penerimaanItem && $penerimaanItem->status) ? "SESUAI" : "BELUM SESUAI" }}</td>
                <td>{{ $row->keterangan }}</td>
                <td>{{ ($penerimaanItem) ? $penerimaanItem->keterangan : "" }}</td>
                <td class="text-center">
                    @component("component.button.openmodal",["datatarget"=>"#modalForm", "onclick"=>"getDetail(this)" , "color"=>"success" , "label"=>"check" , "data"=>"data-poid=$row->poid data-poitemid=$row->id data-qty=$row->qty data-bahan=$bahanModel->kode" ])@endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <!-- Modal -->
    <div id="modalForm" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Konfirmasi Item</h4>
                </div>
                <form action="{{ url("trx/penerimaan_item/") }}" method="post" role="form" class="form-horizontal form-groups-bordered">
                    @csrf
                    <div class="modal-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Bahan</label>
                                        <div class="col-sm-8">
                                            <input type="hidden" class="form-control" id="penerimaanid" placeholder="penerimaanid" name="penerimaanid" value="{{ $data->id }}">
                                            <input type="hidden" class="form-control" id="poitemid" placeholder="poitemid" name="poitemid" >
                                            <input type="hidden" class="form-control" id="poid" placeholder="poid" name="poid" >
                                            <input type="text" class="form-control" id="bahan" placeholder="bahan" name="bahan" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Qty Pemesanan (roll)</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="qtyPo" placeholder="qtyPo" name="qtyPo" value="" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Qty Penerimaan (roll)</label>
                                        <div class="col-sm-8">
                                            <input type="number" class="form-control" id="qty" placeholder="qty" name="qty" value="" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Status</label>
                                        <div class="col-sm-8">
                                            <select name="status" id="status" required class="form-control">
                                                <option value="1">Sesuai</option>
                                                <option value="0">Belum Sesuai</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-form-label">Keterangan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="keterangan" placeholder="keterangan" name="keterangan" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <div class="modal-footer">
                        @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>
        function getDetail(ini) {
            var poid = $(ini).attr('data-poid');
            var poitemid = $(ini).attr('data-poitemid');
            var bahan = $(ini).attr('data-bahan');
            var qty = $(ini).attr('data-qty');
            $('#poid').val(poid);
            $('#poitemid').val(poitemid);
            $('#bahan').val(bahan);
            $('#qtyPo').val(qty);
            $('#qty').val(qty);
        }
    </script>
@endsection
