@extends('layouts.backend')

@section('pagetitle', 'Tambah Penerimaan')

@section('content')
    <form action="{{ url("trx/penerimaan/") }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Purchasing</label>
            <div class="col-sm-4">
                <select name="poid" id="poid" required class="form-control" onchange="getDetailPo()">
                    <option value="">- Purchase Order -</option>
                    @foreach($rowPo as $row)
                        <option value="{{ $row->id }}" >{{ $row->nomor }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Supplier</label>
            <div class="col-sm-4">
                <input id="nama_supplier" name="nama_supplier" type="text" class="form-control" placeholder="nama_supplier" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Pemesanan</label>
            <div class="col-sm-4">
                <input id="tanggal_pemesanan" name="tanggal_pemesanan" type="text" class="form-control" placeholder="tanggal_pemesanan" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-4">
                <input id="keterangan" name="keterangan" type="text" class="form-control" placeholder="keterangan" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Penerimaan</label>
            <div class="col-sm-4">
                <input name="nomor" type="text" class="form-control" placeholder="nomor" value="{{ $kode }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Penerimaan</label>
            <div class="col-sm-4">
                <input name="tanggal" type="date" class="form-control" placeholder="tanggal" >
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("trx/penerimaan")])@endcomponent
            @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
        </div>
    </form>
@endsection

@section('script')
    <script>

        function clearForm() {
            $('#id').val("");
            $('#qty').val(0);
            $('#harga').val(0);
            $('#total').val(0);
        }

        function getDetailPo() {
            var id = $('#poid').val();
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('trx/penerimaan/detailjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    console.log(data.po);
                    $('#nama_supplier').val(data.po.nama_supplier);
                    $('#tanggal_pemesanan').val(data.po.tanggal);
                    $('#keterangan').val(data.po.keterangan);
                }
            });
        }

        function getDetail(id) {
            $.ajax({
                type: 'GET',
                contentType: "application/json",
                dataType: "json",
                url: "{{ url('trx/purchasing/detailjson') }}/"+id,
                success: function (data) {
                    //Do stuff with the JSON data
                    console.log(data);
                    $('#id').val(data.id);
                    $('#bahanid').val(data.bahanid);
                    $('#qty').val(data.qty);
                    $('#harga').val(data.harga);
                    $('#total').val(data.total);
                }
            });
        }

        function calculate() {
            var harga = $('#harga').val();
            var qty = $('#qty').val();
            $('#total').val(harga * qty);
        }

    </script>
@endsection
