@extends('layouts.backend')

@section('pagetitle', strtoupper("Daftar Supplier"))

@section('content')
<div class="button-list">
    @component("component.button.add",["href"=>url('mst/supplier/create')])@endcomponent
</div>
<br>

<table class="table dataTable" id="data-table">
    <thead>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Lokasi</th>
        <th>Bank</th>
        <th>No Rekening</th>
        <th>Penanggungjawab</th>
        <th>Email</th>
        <th class="text-center">Aksi</th>
    </tr>
    </thead>
    <tbody>
    @foreach($rowData as $index => $row)
        <tr>
            <td>{{ $index+1 }}</td>
            <td>{{ $row->nama }}</td>
            <td>{{ $row->lokasi }}</td>
            <td>{{ $row->bank }}</td>
            <td>{{ $row->norekening }}</td>
            <td>{{ $row->penanggungjawab }}</td>
            <td>{{ $row->email }}</td>
            <td class="text-center">
                @component("component.button.view",["href"=>url("mst/supplier/".$row->id)])@endcomponent
                @component("component.button.edit",["href"=>url("mst/supplier/".$row->id."/edit")])@endcomponent
                @component("component.button.delete",["href"=>url("mst/supplier/delete/".$row->id)])@endcomponent
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection

@section('script')
<script>

</script>
@endsection
