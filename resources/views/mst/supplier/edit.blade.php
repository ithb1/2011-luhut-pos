@extends('layouts.backend')

@section('pagetitle', 'Ubah Supplier')

@section('content')
    <form action="{{ url("mst/supplier/".$data->id) }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf
        @method('PUT')

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-4">
                <input name="nama" type="text" class="form-control" placeholder="nama" value="{{ $data->nama }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Lokasi</label>
            <div class="col-sm-4">
                <input name="lokasi" type="text" class="form-control" placeholder="lokasi" value="{{ $data->lokasi }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Bank</label>
            <div class="col-sm-4">
                <input name="bank" type="text" class="form-control" placeholder="bank" value="{{ $data->bank }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Rekening</label>
            <div class="col-sm-4">
                <input name="norekening" type="text" class="form-control" placeholder="norekening" value="{{ $data->norekening }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Penanggung Jawab</label>
            <div class="col-sm-4">
                <input name="penanggungjawab" type="text" class="form-control" placeholder="penanggungjawab" value="{{ $data->penanggungjawab }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-4">
                <input name="email" type="text" class="form-control" placeholder="email" value="{{ $data->email }}">
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("mst/supplier")])@endcomponent
            @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
        </div>
    </form>
@endsection

@section('script')
    <script>

    </script>
@endsection
