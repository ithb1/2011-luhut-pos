@extends('layouts.backend')

@section('pagetitle', 'Lihat Bahan')

@section('content')
    <form action="{{ url("mst/bahan") }}" method="post" role="form" class="form-horizontal form-groups-bordered" id="formbidang">
        @csrf

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Kode</label>
            <div class="col-sm-4">
                <input name="kode" type="text" class="form-control" placeholder="kode" value="{{ $data->kode }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-4">
                <input name="nama" type="text" class="form-control" placeholder="nama" value="{{ $data->nama }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Jenis Benang</label>
            <div class="col-sm-4">
                <input name="jenis_benang" type="text" class="form-control" placeholder="jenis_benang" value="{{ $data->jenis_benang }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Warna</label>
            <div class="col-sm-4">
                <input name="warna" type="text" class="form-control" placeholder="warna" value="{{ $data->warna }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Harga Beli (Kg)</label>
            <div class="col-sm-4">
                <input name="harga_beli" type="text" class="form-control" placeholder="harga_beli" value="{{ $data->harga_beli }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Harga Jual (Kg)</label>
            <div class="col-sm-4">
                <input name="harga_jual" type="text" class="form-control" placeholder="harga_jual" value="{{ $data->harga_jual }}" readonly>
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("mst/bahan")])@endcomponent
        </div>
    </form>
@endsection

@section('script')
    <script>

    </script>
@endsection
