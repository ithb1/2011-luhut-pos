@extends('layouts.backend')

@section('pagetitle', strtoupper("Daftar Bahan"))

@section('content')
    <div class="button-list">
        @component("component.button.add",["href"=>url('mst/bahan/create')])@endcomponent
    </div>
    <br>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Jenis Benang</th>
            <th>Warna</th>
            <th>Harga Beli (Kg)</th>
            <th>Harga Jual (Kg)</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $row->kode }}</td>
                <td>{{ $row->nama }}</td>
                <td>{{ $row->jenis_benang }}</td>
                <td>{{ $row->warna }}</td>
                <td>{{ number_format($row->harga_beli, 0, ',', '.') }}</td>
                <td>{{ number_format($row->harga_jual, 0, ',', '.') }}</td>
                <td class="text-center">
                    @component("component.button.view",["href"=>url("mst/bahan/".$row->id)])@endcomponent
                    @component("component.button.edit",["href"=>url("mst/bahan/".$row->id."/edit")])@endcomponent
                    @component("component.button.delete",["href"=>url("mst/bahan/delete/".$row->id)])@endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('script')
    <script>

    </script>
@endsection
