@extends('layouts.backend')

@section('pagetitle', 'Ubah Bahan')

@section('content')
    <form action="{{ url("mst/bahan/".$data->id) }}" method="post" role="form" class="form-horizontal form-groups-bordered">
        @csrf
        @method('PUT')

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Kode</label>
            <div class="col-sm-4">
                <input name="kode" type="text" class="form-control" placeholder="kode" value="{{ $data->kode }}" disabled>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-4">
                <input name="nama" type="text" class="form-control" placeholder="nama" value="{{ $data->nama }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Jenis Benang</label>
            <div class="col-sm-4">
                <select name="jenis_benang" id="jenis_benang" class="select2 form-control" required>
                    <option disabled>Pilih Jenis</option>
                    <option {{ ($data->jenis_benang == "Poly") ? "selected" : "" }} value="Poly">Poly</option>
                    <option {{ ($data->jenis_benang == "Poly Ester") ? "selected" : "" }} value="Poly Ester">Poly Ester</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Warna</label>
            <div class="col-sm-4">
                <input name="warna" type="text" class="form-control" placeholder="warna" value="{{ $data->warna }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Harga Beli (Kg)</label>
            <div class="col-sm-4">
                <input name="harga_beli" type="number" class="form-control" placeholder="harga_beli" value="{{ $data->harga_beli }}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Harga Jual (Kg)</label>
            <div class="col-sm-4">
                <input name="harga_jual" type="number" class="form-control" placeholder="harga_jual" value="{{ $data->harga_jual }}">
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("mst/bahan")])@endcomponent
            @component('component.button.submit', ["onclick" => "return window.validateAndConfirm('#formbidang','Apakah Anda Yakin?')"]) @endcomponent
        </div>
    </form>
@endsection

@section('script')
    <script>

    </script>
@endsection
