@extends('layouts.backend')

@section('pagetitle', strtoupper("Daftar User"))

@section('content')
    <div class="button-list">
        @component("component.button.add",["href"=>url('mst/user/create')])@endcomponent
    </div>
    <br>

    <table class="table dataTable" id="data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>Username</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>Role Name</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $row->username }}</td>
                <td>{{ $row->nama }}</td>
                <td>{{ $row->jabatan }}</td>
                <td>{{ \App\Models\Sys\Role::find($row->roleid)->nama }}</td>
                <td class="text-center">
                    @component("component.button.view",["href"=>url("mst/user/".$row->id)])@endcomponent
                    @component("component.button.edit",["href"=>url("mst/user/".$row->id."/edit")])@endcomponent
                    @component("component.button.delete",["href"=>url("mst/user/delete/".$row->id)])@endcomponent
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section("script")
    <script>

    </script>
@endsection
