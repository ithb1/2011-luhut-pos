@extends('layouts.backend')

@section('pagetitle', 'Lihat User')

@section('content')
    <form action="{{ url("mst/user") }}" method="post" role="form" class="form-horizontal form-groups-bordered" id="formbidang">
        @csrf

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Username</label>
            <div class="col-sm-4">
                <input name="username" type="text" class="form-control" placeholder="username" value="{{ $data->username }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-4">
                <input name="nama" type="text" class="form-control" placeholder="nama" value="{{ $data->nama }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Jabatan</label>
            <div class="col-sm-4">
                <input name="jabatan" type="text" class="form-control" placeholder="jabatan" value="{{ $data->jabatan }}" readonly>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Role</label>
            <div class="col-sm-4">
                <select name="roleid" id="roleid" class="select2 form-control" disabled>
                    <option selected disabled>Pilih Role</option>
                    @foreach($rowRole as $row)
                        <option value="{{ $row->id }}" @if($data->roleid == $row->id) selected @endif>{{ $row->nama }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <br>
        <div class="button-list">
            @component('component.button.back', ['href' => url("mst/user")])@endcomponent
        </div>
    </form>
@endsection

@section('script')
    <script>

    </script>
@endsection
