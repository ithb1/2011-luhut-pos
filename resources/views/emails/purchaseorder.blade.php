@include('emails.layouts_header')
<div id="judul"><b>PURCHASE ORDER</b></div>
<br>
<table class="table">
    <tr><td class="c1">NOMOR</td><td class="c2">:</td><td class="c3">{{ $headerPo->nomor }}</td></tr>
    <tr><td class="c1">TANGGAL PEMESANAN</td><td class="c2">:</td><td class="c3">{{ $headerPo->tanggal }}</td></tr>
    <tr><td class="c1">KETERANGAN</td><td class="c2">:</td><td class="c3">{{ $headerPo->keterangan }}</td></tr>
</table>
<br>
<br>
    <table class="table" border="1" cellpadding="5">
        <tr>
            <td class="c1">NO</td>
            <td class="c1">BAHAN</td>
            <td class="c2">JUMLAH</td>
        </tr>
        @foreach($detailPo as $index => $dt)
            @php $bahan = \App\Models\Mst\Bahan::find($dt->bahanid); @endphp
        <tr>
            <td class="c1">{{ $index+1 }}</td>
            <td class="c1">{{ $bahan->nama." ".$bahan->warna }}</td>
            <td class="c2">{{ $dt->qty." roll" }}</td>
        </tr>
        @endforeach
    </table>
@include('emails.layouts_footer')
