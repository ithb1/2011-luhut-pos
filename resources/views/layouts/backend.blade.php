<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>.: POS :.</title>

{{--    <link rel="shortcut icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">--}}
{{--    <link rel="icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">--}}

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ url('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('bower_components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('bower_components/Ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ url('adminltematerial/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ url('adminltematerial/css/bootstrap-material-design.min.css') }}">
    <link rel="stylesheet" href="{{ url('adminltematerial/css/ripples.min.css') }}">
    <link rel="stylesheet" href="{{ url('adminltematerial/css/MaterialAdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ url('adminltematerial/css/skins/all-md-skins.min.css') }}">
    <link rel="stylesheet" href="{{ url('bower_components/morris.js/morris.css') }}">
    {{--    <link rel="stylesheet" href="{{ url('bower_components/jvectormap/jquery-jvectormap.css') }}">--}}
    <link rel="stylesheet" href="{{ url('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ url('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ url('bower_components/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ url('adminltematerial/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src=https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js></script>
    <script src=https://oss.maxcdn.com/respond/1.4.2/respond.min.js></script>-->
    {{--<![endif]-->--}}

    <link rel="stylesheet" href=https://cdn.jsdelivr.net/npm/sweetalert2@8.8.7/dist/sweetalert2.min.css>

    <link rel="stylesheet" href="{{ url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('bower_components/datatables.net/css/jquery.dataTables.min.css') }}">

    <link rel="stylesheet" href="{{ url('ibmplex/regular/stylesheet.css') }}">
    <style>
        * { font-family: 'IBMPlexSans-Regular','Helvetica Neue', Arial, sans-serif; !important; }
        textarea:invalid { border: 1px solid red !important; }
        input:invalid { border: 1px solid red !important; }
        select:invalid { border: 1px solid red !important; }
        textarea { padding: 5px 5px !important; }
        input { padding: 5px 5px !important; }
        input:read-only, textarea:read-only { cursor: default; }
        input:read-only:active, input:focus:read-only:focus, textarea:read-only:active, textarea:focus:read-only:focus { background-image:linear-gradient(#D2D2D2,#D2D2D2) !important; }
        select { padding: 5px 5px !important; }
        .has-error .select2-selection { border: 1px solid red !important; }
        .dataTables_wrapper .dataTables_processing { background:none;box-shadow:none; }
        .btn.btn-dark { color:#fff;background-color:#343a40;border-color:#343a40; }
        .longText { width: 100%; max-width: 200px; display:block; /*word-wrap: break-word; word-break: break-all;*/ text-overflow: ellipsis; white-space: nowrap; overflow: hidden;}
        .text-left { text-align:left !important; }
        .btn.btn-up { margin: 0 1px !important;}
        .form-group-modal { margin: 0 -15px 0 -15px  !important; }
        .btn.btn-table {     margin-top: 0; margin-right: 1px; margin-bottom: 10px; margin-left: 1px; !important;}
        .form-control.loading { background-color: #FFF;background-image: url("../images/24.gif")!important;background-size: 15px 15px;background-position:right center;background-repeat: no-repeat; }
        .content-header { margin-top:10px; }
        .text-wrap { word-wrap:break-word;white-space:normal; }
        /*.width-spc { width:150px; }
        .navbar-nav>.user-menu>.dropdown-menu>li.user-header { height:auto;padding:10px;text-align:center; }*/
        .dataTables_scroll { overflow: auto; clear: both;}
        table .btn { margin:0; }
        .form-control.form-up{ height: 22px; font-size: 14px; }
        .form-control.form-font{ font-size: 14px; }
        .form-group.form-upper{ margin: 0 0 0 0 !important;}
        .label-upper { margin-top: 0 !important; }
        .padding-spc { padding-left: 5px;}
        [class*='col-md'] .btn { margin:0; }
        [class*='col-md'] .checkbox { padding-top:0; }
        [class*='col-md'] label { margin:0; }

        #loading {
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: fixed;
            display: block;
            opacity: 0.5;
            background-color: #fff;
            text-align: center;
            z-index: 9999;
        }

        #spinner{
            position: fixed;
            top: 45%;
            left: 45%;
            transform: translate(-45%, -45%);
        }

        .fitcontentmoney{
            width: fit-content;
        }
    </style>

    @yield('style')
</head>

<body class="hold-transition skin-blue-light sidebar-mini fixed">
<div id="loading" style="display: none">
    <i id="spinner" class="fa fa-cog fa-spin fa-5x fa-fw"></i>
</div>
<div class="wrapper">
@include('layouts.header')

<!-- Left side column. contains the logo and sidebar -->
@include('layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield("pagetitle")
                <small>@yield("pagedescription")</small>
            </h1>
            {{--<ol class="breadcrumb">
                <li><a href="#"><i class="{{ config('view.theme') ? 'fa fa' : 'entypo' }}-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>--}}
        </section>

    @yield("freezone")

    <!-- Main content -->
        <section class="content">
            <div class="{{ container("box") }}">
                <div class="{{ container("body") }}">
                    @yield('content')
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@include('layouts.footer')

<!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ url('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ url('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ url('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Material Design -->
<script src="{{ url('adminltematerial/js/material.min.js') }}"></script>
<script src="{{ url('adminltematerial/js/ripples.min.js') }}"></script>
<script>
    $.material.init();
</script>
<!-- DataTables -->
<script src="{{ url('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script src="{{ url('js/js_eksport/jquery.base64.js') }}"></script>
<script src="{{ url('js/js_eksport/jquery.btechco.excelexport.js') }}"></script>
<!-- Select2 -->
<script src="{{ url('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ url('bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ url('bower_components/morris.js/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
{{--<script src="{{ url('adminltematerial/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ url('adminltematerial/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>--}}
<!-- jQuery Knob Chart -->
<script src="{{ url('bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ url('bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ url('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
{{--<script src="{{ url('adminltematerial/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>--}}
<!-- Slimscroll -->
<script src="{{ url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Inputmask -->
<script src="{{ url('bower_components/inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ url('bower_components/chart.js/Chart.bundle.js') }}"></script>
<!-- FastClick -->
<script src="{{ url('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ url('adminltematerial/js/adminlte.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.8.7/dist/sweetalert2.all.min.js"></script>

{{--<script src="{{ url('js/jquery.dataTables.min.js') }}"></script>--}}
{{--<link rel="stylesheet" href="http://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">--}}

<script src="{{ url('js/toastr.js') }}"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{ url('adminltematerial/js/pages/dashboard.js') }}"></script>--}}

<!-- AdminLTE for demo purposes -->
<script src="{{ url('adminltematerial/js/demo.js') }}"></script>

@yield('script')
<script>
    $(document).ready( function () {
        $('#data-table').DataTable();
    } );

    @if(Session::get("errormessage"))
        toastr.options = {
        closeButton: true,
        timeOut: 0,
        extendedTimeOut: 0,
    };

    @php $text = str_replace("\n", '', Session::get("errormessage")); @endphp
    toastr.error("{{$text}}", "Error", {});

    @elseif(Session::get("successmessage"))

    toastr.success("{{Session::get("successmessage")}}", "Berhasil", {});

    @elseif(Session::get("popupmessage"))
    Swal.fire("", "{{Session::get("popupmessage")}}", "warning");
    @endif

</script>
</body>
</html>
