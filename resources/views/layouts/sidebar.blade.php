@php
    $user = \Illuminate\Support\Facades\Auth::user();
    $roleid = $user->roleid;
@endphp
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="padding-bottom:5px !important;padding-top:10px !important;">
            <div class="pull-left image">
                <img src="{{ url('adminltematerial/img/avatar5.png') }}" class="img-circle" alt="User Image">
            </div>

            <div class="pull-left info">
                <p>{{Auth::user()->nama}}</p>
                <p>{{Auth::user()->jabatan}}</p>
                {{--<a href="#"><i class="{{ config('view.theme') ? 'fa fa' : 'entypo' }}-circle text-success"></i> Online</a>--}}
            </div>
        </div>
        <!-- search form -->
        {{--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="{{ config('view.theme') ? 'fa fa' : 'entypo' }}-search"></i>
                </button>
              </span>
            </div>
        </form>--}}
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu tree" data-widget="tree" style="text-transform: capitalize">
            <li>
                <a href="{{ url('') }}">
                    <i class="fa fa-dashboard"></i><span>BERANDA</span>
                </a>
            </li>
            @if($roleid == 1 || $roleid == 2 || $roleid == 3)
            <li class="treeview">
                <a href="http://localhost/siwa/luhut_pos/public">
                    <i class="fa fa-gear"></i><span>MASTER</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    @if($roleid == 1)
                    <li>
                        <a href="{{ url('mst/user') }}">
                            <i class="fa fa-dashboard"></i><span>USER</span>
                        </a>
                    </li>
                    @endif
                    @if($roleid == 1 || $roleid == 3)
                    <li>
                        <a href="{{ url('mst/supplier') }}">
                            <i class="fa fa-dashboard"></i><span>SUPPLIER</span>
                        </a>
                    </li>
                    @endif
                    @if($roleid == 1 || $roleid == 2)
                    <li>
                        <a href="{{ url('mst/bahan') }}">
                            <i class="fa fa-dashboard"></i><span>BAHAN</span>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif
            @if($roleid == 1 || $roleid == 2 || $roleid == 3 || $roleid == 5)
            <li>
                <a href="{{url('trx/stok')}}">
                    <i class="fa fa-barcode"></i><span>STOK BARANG</span>
                </a>
            </li>
            @endif
            @if($roleid == 1 || $roleid == 3 || $roleid == 5)
            <li>
                <a href="{{url('trx/purchasing')}}">
                    <i class="fa fa-shopping-bag"></i><span>PURCHASING</span>
                </a>
            </li>
            @endif
            @if($roleid == 1 || $roleid == 2 || $roleid == 5)
            <li>
                <a href="{{url('trx/penerimaan')}}">
                    <i class="fa fa-truck"></i><span>PENERIMAAN</span>
                </a>
            </li>
            @endif
            @if($roleid == 1 || $roleid == 3 || $roleid == 5)
            <li>
                <a href="{{url('trx/pembayaran')}}">
                    <i class="fa fa-dollar"></i><span>PEMBAYARAN UTANG</span>
                </a>
            </li>
            @endif
            @if($roleid == 1 || $roleid == 2 || $roleid == 4 || $roleid == 5)
            <li>
                <a href="{{url('trx/penjualan')}}">
                    <i class="fa fa-shopping-cart"></i><span>PENJUALAN</span>
                </a>
            </li>
            @endif
            @if($roleid == 1 || $roleid == 4 || $roleid == 5)
            <li>
                <a href="{{url('trx/retur')}}">
                    <i class="fa fa-recycle"></i><span>RETUR</span>
                </a>
            </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
