<header class="main-header">
    <!-- Logo -->
    <a href="{{url('')}}" class="logo" style="font-size: 16px">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"></span>
        <!-- logo for regular state and mobile devices -->
{{--        <img src="{{ url('images/logo_pth_itam.png') }}" width="140" alt="TOKO HOUSE OF KNITTING" />--}}
        TOKO HOUSE OF KNITTING
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="userDropdown">
                        <img src="{{ url('adminltematerial/img/avatar5.png') }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{Auth::user()->nama}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="height-auto user-header ">
                            <img src="{{ url('adminltematerial/img/avatar5.png') }}" class="img-circle" alt="User Image">

                            <p>Nama: {{Auth::user()->nama}}</p>
                            <p>Jabatan: {{Auth::user()->jabatan}}</p>
                        </li>
                        <!-- Menu Body -->
                       {{-- <li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                            <!-- /.row -->
                        </li>--}}
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="text-center">
                                <form action="{{url("logout")}}" method="post" role="form">
                                    @csrf
                                    <button accesskey="q" type="submit" id="btnLogout" onclick="return confirm('anda akan logout.?')" class="btn btn-danger btn-icon">Log Out <i class='{{ config('view.theme') ? 'fa fa' : 'entypo' }}-logout right'></i> </button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>
    </nav>
</header>

<style>
    .modal-backdrop {
        display:none !important;
    }
    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-tooltip-content{
        /*left: 30% !important;*/
    }
    .modal-tooltip-body{
        max-height: calc(100vh - 200px);
        max-width: calc(100vh - 200px);
        overflow-x: auto;
        overflow-y: auto;
    }
    .all-scroll {
        cursor: all-scroll;
    }

</style>

{{--<script type="text/javascript">--}}
<script type="text/javascript" src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="js/jquery-ui-1.8.16.custom.min.js" type="type/javascript"></script>
<script src="js/ui/jquery.ui.draggable.js" type="type/javascript"></script>
<script type="text/javascript">
    $("#myModal").draggable({
        handle: ".modal-header"
    });
</script>
