@include("export/header")
    <div class="text-center">
        <h4>Detail Purchasing</h4>
    </div>
    <br/>

    <table class="">
        <tr>
            <td>Nomor</td>
            <td>: {{ $rowData->nomor }}</td>
        </tr>
        <tr>
            <td>Supplier</td>
            <td>: {{ $rowSupplier->nama }}</td>
        </tr>
        <tr>
            <td>Tanggal Pemesanan</td>
            <td>: {{ $rowData->tanggal }}</td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>: {{ $rowData->keterangan }}</td>
        </tr>
        <tr>
            <td>Grand Total</td>
            <td>: {{ number_format($rowData->total, 0 ,',','.') }}</td>
        </tr>
    </table>

    <hr>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>No</th>
            <th>Bahan</th>
            <th>Qty (roll)</th>
            <th>Harga</th>
            <th>Total</th>
            <th>Keterangan</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowsItem as $index => $row)
            @php $bahanModel = \App\Models\Mst\Bahan::find($row->bahanid) @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna }}</td>
                <td>{{ number_format($row->qty, 0 ,',','.') }}</td>
                <td>{{ number_format($row->harga, 0 ,',','.') }}</td>
                <td>{{ number_format($row->total, 0 ,',','.') }}</td>
                <td>{{ $row->keterangan }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@include("export/footer")
