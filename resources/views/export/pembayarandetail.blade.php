@include("export/header")
    <div class="text-center">
        <h4>Detail Pembayaran</h4>
    </div>
    <br/>

    <table class="">
        <tr>
            <td>Nomor Pembayaran</td>
            <td>: {{ $rowData->nomor }}</td>
        </tr>
        <tr>
            <td>Tanggal Pembayaran</td>
            <td>: {{ $rowData->tanggal }}</td>
        </tr>
        <tr>
            <td>Grand Total</td>
            <td>: {{ number_format($rowData->total, 0 ,',','.') }}</td>
        </tr>
        <tr>
            <td>Jenis</td>
            <td>: {{ $rowData->jenis }}</td>
        </tr>
    </table>

    <hr>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>No</th>
            <th>Nomor PO</th>
            <th>Supplier</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowsItem as $index => $row)
            @php
                $poModel = \App\Models\Trx\PurchaseOrder::find($row->poid);
                $supplierModel = \App\Models\Mst\Supplier::find($poModel->supplier_id);
            @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $poModel->nomor }}</td>
                <td>{{ $supplierModel->nama }}</td>
                <td>{{ $poModel->total }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@include("export/footer")
