<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" crossorigin="anonymous">

    <style>
        @page { margin: 10px }
        body { margin: 10px}
        .container{
            margin-left: unset;margin-right: unset; max-width: unset;
        }
        header { position: fixed; top: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }
        footer { position: fixed; bottom: 20px; text-align: right; font-size: 8pt }
        p { page-break-after: always; }
        p:last-child { page-break-after: never; }
        table { font-size: 10pt }
    </style>
</head>
<body>

<div class="container">

