@include("export/header")
    <div class="text-center">
        <h4>Laporan Purchasing</h4>
        {{ $filter->tgl_awal ?? '-' }} s/d {{ $filter->tgl_akhir ?? '-' }}
    </div>
    <br/>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>No</th>
            <th>Nomor</th>
            <th>Supplier</th>
            <th>Tanggal Pemesanan</th>
            <th>Bahan</th>
            <th>Harga</th>
            <th>Jumlah</th>
            <th>Total</th>
            <th>Grand Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            @php
                $item = 1;
                $dataPoItem = ( new \App\Models\Trx\PurchaseOrderItem)->getByPo($row->id);
                $jmlItem = count($dataPoItem);
            @endphp
            @foreach($dataPoItem as $rowPo)
                @php $bahanModel = \App\Models\Mst\Bahan::find($rowPo->bahanid) @endphp
            <tr>
                @if($item == 1)
                <td rowspan="{{ $jmlItem }}">{{ $index+1 }}</td>
                <td rowspan="{{ $jmlItem }}">{{ $row->nomor }}</td>
                <td rowspan="{{ $jmlItem }}">{{ \App\Models\Mst\Supplier::find($row->supplier_id)->nama }}</td>
                <td rowspan="{{ $jmlItem }}">{{ $row->tanggal }}</td>
                @endif
                <td>{{ $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna }}</td>
                <td>{{ number_format($rowPo->qty, 0 ,',','.') }}</td>
                <td>{{ number_format($rowPo->harga, 0 ,',','.') }}</td>
                <td>{{ number_format($rowPo->total, 0 ,',','.') }}</td>

                @if($item == 1)
                <td rowspan="{{ $jmlItem }}"><b>{{ number_format($row->total, 0 ,',','.') }}</b></td>
                @endif
            </tr>
                @php($item++)
            @endforeach
        @endforeach
        </tbody>
    </table>
@include("export/footer")
