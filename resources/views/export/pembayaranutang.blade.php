@include("export/header")
    <div class="text-center">
        <h4>Laporan Pembayaran Utang</h4>
        {{ $filter->tgl_awal ?? '-' }} s/d {{ $filter->tgl_akhir ?? '-' }}
    </div>
    <br/>
    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>No</th>
            <th>Nomor Pembayaran</th>
            <th>Tanggal Pembayaran</th>
            <th>Jenis Pembayaran</th>
            <th>Nomor PO</th>
            <th>Total</th>
            <th>Grand Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            @php
                $item = 1;
                $dataItem = ( new \App\Models\Trx\PembayaranItem())->getByPembayaran($row->id);
                $jmlItem = count($dataItem);
            @endphp
            @foreach($dataItem as $rowItem)
                @php
                    $dataPo = \App\Models\Trx\PurchaseOrder::find($rowItem->poid);
                @endphp
                <tr>
                    @if($item == 1)
                        <td rowspan="{{ $jmlItem }}">{{ $index+1 }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $row->nomor }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $row->tanggal }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $row->jenis }}</td>
                    @endif
                    <td>{{ $dataPo->nomor }}</td>
                    <td>{{ number_format($dataPo->total, 0 ,',','.') }}</td>
                    @if($item == 1)
                        <td rowspan="{{ $jmlItem }}">{{ number_format($row->total, 0 ,',','.') }}</td>
                    @endif
                </tr>
                @php($item++)
            @endforeach
        @endforeach
        </tbody>
    </table>
@include("export/footer")
