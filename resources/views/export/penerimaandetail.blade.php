@include("export/header")
    <div class="text-center">
        <h4>Detail Penerimaan</h4>
    </div>
    <br/>

    <table class="">
        <tr>
            <td>Nomor Purchasing</td>
            <td>: {{ $rowPo->nomor }}</td>
        </tr>
        <tr>
            <td>Supplier</td>
            <td>: {{ $rowSupplier->nama }}</td>
        </tr>
        <tr>
            <td>Tanggal Pemesanan</td>
            <td>: {{ $rowPo->tanggal }}</td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>: {{ $rowPo->keterangan }}</td>
        </tr>
        <tr>
            <td>Nomor Penerimaan</td>
            <td>: {{ $rowData->nomor }}</td>
        </tr>
        <tr>
            <td>Tanggal Penerimaan</td>
            <td>: {{ $rowData->tanggal }}</td>
        </tr>
        <tr>
            <td>Status Penerimaan</td>
            <td>: {{ $rowPo->status_penerimaan }}</td>
        </tr>
    </table>

    <hr>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>No</th>
            <th>Bahan</th>
            <th>Qty Pemesanan (roll)</th>
            <th>Qty Penerimaan (roll)</th>
            <th>Status</th>
            <th>Keterangan Pemesanan</th>
            <th>Keterangan Penerimaan</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowsItem as $index => $row)
            @php
                $bahanModel = \App\Models\Mst\Bahan::find($row->bahanid);
                $penerimaanItem = (new \App\Models\Trx\PenerimaanItem)->getByPenerimaanDanPo($rowData->id, $row->id);
                $bahan = $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna;
            @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $bahan }}</td>
                <td>{{ number_format($row->qty, 0 ,',','.') }}</td>
                <td>{{ ($penerimaanItem) ? number_format($penerimaanItem->qty, 0 ,',','.') : 0 }}</td>
                <td>{{ ($penerimaanItem && $penerimaanItem->status) ? "SESUAI" : "BELUM SESUAI" }}</td>
                <td>{{ $row->keterangan }}</td>
                <td>{{ ($penerimaanItem) ? $penerimaanItem->keterangan : "" }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@include("export/footer")
