@include("export/header")
    <div class="text-center">
        <h4>Laporan Penerimaan</h4>
        {{ $filter->tgl_awal ?? '-' }} s/d {{ $filter->tgl_akhir ?? '-' }}
    </div>
    <br/>
    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>No</th>
            <th>Nomor PO</th>
            <th>Nomor Penerimaan</th>
            <th>Supplier</th>
            <th>Tanggal Pemesanan</th>
            <th>Tanggal Penerimaan</th>
            <th>Keterangan</th>
            <th>Bahan</th>
            <th>Jumlah Pemesanan</th>
            <th>Jumlah Penerimaan</th>
            <th>Status Penerimaan</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            @php
                $item = 1;
                $dataPo = ( new \App\Models\Trx\PurchaseOrder)->find($row->poid);
                $dataPoItem = ( new \App\Models\Trx\PurchaseOrderItem)->getByPo($row->poid);
                $jmlItem = count($dataPoItem);
            @endphp
            @foreach($dataPoItem as $rowPoItem)
                @php
                    $dataPenerimaanItem = (new \App\Models\Trx\PenerimaanItem)->getByPenerimaanDanPo($row->id, $rowPoItem->id);
                    $bahanModel = \App\Models\Mst\Bahan::find($rowPoItem->bahanid);
                @endphp
                <tr>
                    @if($item == 1)
                        <td rowspan="{{ $jmlItem }}">{{ $index+1 }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $dataPo->nomor }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $row->nomor }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ \App\Models\Mst\Supplier::find($dataPo->supplier_id)->nama }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $dataPo->tanggal }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $row->tanggal }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $row->keterangan }}</td>
                    @endif
                    <td>{{ $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna }}</td>
                    <td>{{ number_format($rowPoItem->qty, 0 ,',','.') }}</td>
                    <td>{{ number_format($dataPenerimaanItem->qty, 0 ,',','.') }}</td>
                    <td>{{ ($dataPenerimaanItem->status) ? "SESUAI" : "BELUM SESUAI" }}</td>
                </tr>
                @php($item++)
            @endforeach
        @endforeach
        </tbody>
    </table>
@include("export/footer")
