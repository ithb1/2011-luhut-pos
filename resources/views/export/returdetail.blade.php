@include("export/header")
    <div class="text-center">
        <h4>Detail Retur</h4>
    </div>
    <br/>

    <table class="">
        <tr>
            <td>Nomor Penjualan</td>
            <td>: {{ $dataSo->nomor }}</td>
        </tr>
        <tr>
            <td>Tanggal Pemesanan</td>
            <td>: {{ $dataSo->tanggal }}</td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>: {{ $dataSo->keterangan }}</td>
        </tr>
        <tr>
            <td>Nomor Retur</td>
            <td>: {{ $rowData->nomor }}</td>
        </tr>
        <tr>
            <td>Tanggal Retur</td>
            <td>: {{ $rowData->tanggal }}</td>
        </tr>
        <tr>
            <td>Grand Total</td>
            <td>: {{ number_format($rowData->total, 0 ,',','.') }}</td>
        </tr>
    </table>

    <hr>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>No</th>
            <th>Bahan</th>
            <th>Qty Penjualan (Kg)</th>
            <th>Qty Retur (Kg)</th>
            <th>Total</th>
            <th>Keterangan Retur</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowsItem as $index => $row)
            @php $bahanModel = \App\Models\Mst\Bahan::find($row->bahanid) @endphp
            @php $returItem = (new \App\Models\Trx\SalesReturItem())->getByReturDanPenjualan($rowData->id, $row->id); @endphp
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna }}</td>
                <td>{{ number_format($row->qty, 0 ,',','.') }}</td>
                <td>{{ ($returItem) ? number_format($returItem->qty, 0 ,',','.') : 0 }}</td>
                <td>{{ ($returItem) ? number_format($returItem->total, 0 ,',','.') : 0 }}</td>
                <td>{{ ($returItem) ? $returItem->keterangan : "" }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@include("export/footer")
