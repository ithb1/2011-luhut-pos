@include("export/header")
    <div class="text-center">
        <h4>Laporan Penjualan</h4>
        {{ $filter->tgl_awal ?? '-' }} s/d {{ $filter->tgl_akhir ?? '-' }}
    </div>
    <br/>
    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>No</th>
            <th>Nomor Penjualan</th>
            <th>Tanggal Penjualan</th>
            <th>Bahan</th>
            <th>Harga</th>
            <th>Jumlah</th>
            <th>Status</th>
            <th>Total</th>
            <th>Grand Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            @php
                $item = 1;
                $dataItem = ( new \App\Models\Trx\SalesOrderItem())->getByPenjualan($row->id);
                $jmlItem = count($dataItem);
            @endphp
            @foreach($dataItem as $rowItem)
                @php $bahanModel = \App\Models\Mst\Bahan::find($rowItem->bahanid) @endphp
                <tr>
                    @if($item == 1)
                        <td rowspan="{{ $jmlItem }}">{{ $index+1 }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $row->nomor }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $row->tanggal }}</td>
                    @endif
                        <td>{{ $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna }}</td>
                        <td>{{ number_format($rowItem->harga, 0 ,',','.') }}</td>
                        <td>{{ number_format($rowItem->qty, 0 ,',','.') }}</td>
                        <td>{{ $rowItem->status }}</td>
                        <td>{{ number_format($rowItem->total, 0 ,',','.') }}</td>

                    @if($item == 1)
                        <td rowspan="{{ $jmlItem }}"><b>{{ number_format($row->total, 0 ,',','.') }}</b></td>
                    @endif
                </tr>
                @php($item++)
            @endforeach
        @endforeach
        </tbody>
    </table>
@include("export/footer")
