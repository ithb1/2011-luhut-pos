@include("export/header")
    <div class="text-center">
        <h4>Laporan Stok Bahan Baku</h4>
    </div>
    <br/>
    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Jenis Benang</th>
            <th>Warna</th>
            <th>Stok (Kg)</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{$row->kode}}</td>
                <td>{{$row->nama}}</td>
                <td>{{$row->jenis_benang}}</td>
                <td>{{$row->warna}}</td>
                <td>{{$row->stok}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@include("export/footer")
