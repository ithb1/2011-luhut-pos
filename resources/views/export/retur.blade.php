@include("export/header")
    <div class="text-center">
        <h4>Laporan Retur</h4>
        {{ $filter->tgl_awal ?? '-' }} s/d {{ $filter->tgl_akhir ?? '-' }}
    </div>
    <br/>
    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>No</th>
            <th>Nomor Penjualan</th>
            <th>Tanggal Penjualan</th>
            <th>Nomor Retur</th>
            <th>Tanggal Retur</th>
            <th>Bahan</th>
            <th>Qty Penjualan (Kg)</th>
            <th>Qty Retur (Kg)</th>
            <th>Total</th>
            <th>Keterangan Retur</th>
        </tr>
        </thead>
        <tbody>
        @foreach($rowData as $index => $row)
            @php
                $item = 1;
                $dataItem = ( new \App\Models\Trx\SalesReturItem())->getByReturId($row->id);
                $jmlItem = count($dataItem);
            @endphp
            @foreach($dataItem as $rowItem)
                @php
                    $dataSo = \App\Models\Trx\SalesOrder::find($rowItem->soid);
                    $dataSoItem = \App\Models\Trx\SalesOrderItem::find($rowItem->soitemid);
                    $bahanModel = \App\Models\Mst\Bahan::find($dataSoItem->bahanid)
                @endphp
                <tr>
                    @if($item == 1)
                        <td rowspan="{{ $jmlItem }}">{{ $index+1 }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $dataSo->nomor }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $dataSo->tanggal }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $row->nomor }}</td>
                        <td rowspan="{{ $jmlItem }}">{{ $row->tanggal }}</td>
                    @endif
                    <td>{{ $bahanModel->kode." - ".$bahanModel->nama." - ".$bahanModel->warna }}</td>
                    <td>{{ number_format($dataSoItem->qty, 0 ,',','.') }}</td>
                    <td>{{ number_format($rowItem->qty, 0 ,',','.') }}</td>
                    <td>{{ number_format($rowItem->total, 0 ,',','.') }}</td>
                    <td>{{ $rowItem->keterangan }}</td>
                </tr>
                @php($item++)
            @endforeach
        @endforeach
        </tbody>
    </table>
@include("export/footer")
