<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>TOKO HOUSE OF KNITTING</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="">

    <!-- App favicon -->
    <!--link rel="shortcut icon" href="images/favicon.ico" -->
    <link rel="shortcut icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">

    <!-- App css -->
    <link rel="stylesheet" href="{{ url('logindata/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('logindata/bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('logindata/icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('logindata/metismenu.min.css') }}" type="text/css" />
    {{--<link rel="stylesheet" href="{{ url('css/font-icons/entypo/css/entypo.css') }}" type="text/css">--}}
    <link rel="stylesheet" href="{{ url('bower_components/font-awesome/css/font-awesome.min.css') }}">

    <script src="{{ url('logindata/modernizr.min.js') }}"></script>
    <script src="{{ url('logindata/jquery-1.10.2.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('#show').click(function(){
                if (document.querySelector("#password").type === "password") {
                  document.querySelector("#password").type = "text"
                } else {
                  document.querySelector("#password").type = "password";
                }
            });
        });
    </script>

</head>


<body class="account-pages">

<!-- Begin page -->
<div class="accountbg" style="background: url('images/bg-0.jpg');background-size: cover;"></div>

<div class="wrapper-page account-page-full">

    <div class="card">
        <div class="card-block">

            <div class="account-box">

                <div class="card-box p-5">
                    <h2 class="text-uppercase text-center pb-4">
                        <a href="#" class="text-success">
                            <span><img src=<?php echo e(asset('images/sipatnewBG.png')); ?> alt="" height="56"></span>
                        </a>
                    </h2>
                    <!-- title-->
                    <h4 class="mt-0 text-center" id="div-id">TOKO HOUSE OF KNITTING</h4>
                    <p class="text-muted mb-4"></p>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <form action="{{url("login")}}" method="POST">
                        @csrf
                        <div class="form-group m-b-20 row">
                            <div class="col-12">
                                <label for="emailaddress">Email address / Username</label>
                                <input class="form-control" name="username" type="text" id="emailaddress" required placeholder="Enter your email / Username" tabindex="1">
                            </div>
                        </div>

                        <div class="form-group row m-b-20">
                            <div class="col-12">
                                <a href="#" class="text-muted pull-right"><small>Forgot your password?</small></a>
                                <label for="password">Password</label>
                                <div class="input-group">
                                    <input class="form-control" name="password" type="password" required="" id="password" placeholder="Enter your password" tabindex="2">
                                    <span class="input-group-btn">
                                        <button class="btn btn-custom" type="button" id="show">
                                            <i class='{{ config('view.theme') ? 'fa fa' : 'entypo' }}-eye'></i>
                                        </button>
                                    </span>
                                </div>
                                <div class="icon-left"></div>
                            </div>
                        </div>

                        @if(Session::has("error"))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            Username / Password salah !
                        </div>
                        @endif

                        <div class="form-group row text-center m-t-10">
                            <div class="col-12">
                                <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Sign In</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="m-t-40 text-center">
        <p class="account-copyright">2020 © <a href="http://rachman.id" target="_blank">www.rachman.id</a></p>
    </div>

</div>



<!-- jQuery  -->
<script src="{{ url('logindata/jquery.min.js') }}"></script>
<script src="{{ url('logindata/popper.min.js') }}"></script>
<script src="{{ url('logindata/bootstrap.min.js') }}"></script>
<script src="{{ url('logindata/metisMenu.min.js') }}"></script>
<script src="{{ url('logindata/waves.js') }}"></script>
<script src="{{ url('logindata/jquery.slimscroll.js') }}"></script>

<!-- App js -->
<script src="{{ url('logindata/jquery.core.js') }}"></script>
<script src="{{ url('logindata/jquery.app.js') }}"></script>

</body>
</html>
