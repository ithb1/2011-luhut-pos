/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : 2011_luhut_pos

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 09/12/2020 19:34:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mst_bahan
-- ----------------------------
DROP TABLE IF EXISTS `mst_bahan`;
CREATE TABLE `mst_bahan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_benang_id` int(11) NULL DEFAULT NULL,
  `supplier_id` int(11) NULL DEFAULT NULL,
  `warna` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `stok` int(255) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jenis_benang_id`(`jenis_benang_id`) USING BTREE,
  INDEX `supplier_id`(`supplier_id`) USING BTREE,
  CONSTRAINT `mst_bahan_ibfk_1` FOREIGN KEY (`jenis_benang_id`) REFERENCES `mst_jenisbenang` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `mst_bahan_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `mst_supplier` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mst_bahan
-- ----------------------------

-- ----------------------------
-- Table structure for mst_jenisbenang
-- ----------------------------
DROP TABLE IF EXISTS `mst_jenisbenang`;
CREATE TABLE `mst_jenisbenang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mst_jenisbenang
-- ----------------------------

-- ----------------------------
-- Table structure for mst_supplier
-- ----------------------------
DROP TABLE IF EXISTS `mst_supplier`;
CREATE TABLE `mst_supplier`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lokasi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `norekening` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `penanggungjawab` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mst_supplier
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'Superadmin');
INSERT INTO `sys_role` VALUES (2, 'Gudang');
INSERT INTO `sys_role` VALUES (3, 'Purchasing');
INSERT INTO `sys_role` VALUES (4, 'Kasir');
INSERT INTO `sys_role` VALUES (5, 'Admin Umum');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jabatan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `roleid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `roleid`(`roleid`) USING BTREE,
  CONSTRAINT `sys_user_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `sys_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '$2y$10$EIVvz7/P8dl81tQthxM4ZOcx1JYZFAiOKYNPXYyo/iKmzo/H.6mua', 'admin', 'wow', 2);
INSERT INTO `sys_user` VALUES (2, 'gudang', '$2y$10$IeoTtLQO4DF5.nAOX9cRr.5llQsf0dej8eYf5pU8waYXzmg1Gdf6G', 'gudang', 'ss', 3);

-- ----------------------------
-- Table structure for trx_purchaseorder
-- ----------------------------
DROP TABLE IF EXISTS `trx_purchaseorder`;
CREATE TABLE `trx_purchaseorder`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` int(11) NULL DEFAULT NULL,
  `supplier_id` int(11) NULL DEFAULT NULL,
  `tanggal_pemesanan` date NULL DEFAULT NULL,
  `tanggal_penerimaan` date NULL DEFAULT NULL,
  `tanggal_pembayaran` date NULL DEFAULT NULL,
  `total` int(255) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status_pembayaran` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'PESAN, LUNAS',
  `status_penerimaan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'PESAN, DONE',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `supplier_id`(`supplier_id`) USING BTREE,
  CONSTRAINT `trx_purchaseorder_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `mst_supplier` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_purchaseorder
-- ----------------------------

-- ----------------------------
-- Table structure for trx_purchaseorder_item
-- ----------------------------
DROP TABLE IF EXISTS `trx_purchaseorder_item`;
CREATE TABLE `trx_purchaseorder_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poid` int(11) NULL DEFAULT NULL,
  `bahanid` int(11) NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'sesuai, tidak sesuai',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `poid`(`poid`) USING BTREE,
  INDEX `bahanid`(`bahanid`) USING BTREE,
  CONSTRAINT `trx_purchaseorder_item_ibfk_1` FOREIGN KEY (`poid`) REFERENCES `trx_purchaseorder` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `trx_purchaseorder_item_ibfk_2` FOREIGN KEY (`bahanid`) REFERENCES `mst_bahan` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_purchaseorder_item
-- ----------------------------

-- ----------------------------
-- Table structure for trx_salesorder
-- ----------------------------
DROP TABLE IF EXISTS `trx_salesorder`;
CREATE TABLE `trx_salesorder`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` int(11) NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `total` int(255) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'PESAN, LUNAS',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_salesorder
-- ----------------------------

-- ----------------------------
-- Table structure for trx_salesorder_item
-- ----------------------------
DROP TABLE IF EXISTS `trx_salesorder_item`;
CREATE TABLE `trx_salesorder_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soid` int(11) NULL DEFAULT NULL,
  `bahanid` int(11) NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'approve, reject oleh gudang',
  `approved_at` datetime(0) NULL DEFAULT NULL,
  `approved_by` int(11) NULL DEFAULT NULL COMMENT 'userid',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `poid`(`soid`) USING BTREE,
  INDEX `bahanid`(`bahanid`) USING BTREE,
  CONSTRAINT `trx_salesorder_item_ibfk_2` FOREIGN KEY (`bahanid`) REFERENCES `mst_bahan` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `trx_salesorder_item_ibfk_3` FOREIGN KEY (`soid`) REFERENCES `trx_salesorder` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_salesorder_item
-- ----------------------------

-- ----------------------------
-- Table structure for trx_salesretur
-- ----------------------------
DROP TABLE IF EXISTS `trx_salesretur`;
CREATE TABLE `trx_salesretur`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soid` int(11) NULL DEFAULT NULL,
  `nomor` int(11) NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `total` int(255) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'PESAN, LUNAS',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `soid`(`soid`) USING BTREE,
  CONSTRAINT `trx_salesretur_ibfk_1` FOREIGN KEY (`soid`) REFERENCES `trx_salesorder` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_salesretur
-- ----------------------------

-- ----------------------------
-- Table structure for trx_salesretur_item
-- ----------------------------
DROP TABLE IF EXISTS `trx_salesretur_item`;
CREATE TABLE `trx_salesretur_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soid` int(11) NULL DEFAULT NULL,
  `bahanid` int(11) NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `poid`(`soid`) USING BTREE,
  INDEX `bahanid`(`bahanid`) USING BTREE,
  CONSTRAINT `trx_salesretur_item_ibfk_1` FOREIGN KEY (`bahanid`) REFERENCES `mst_bahan` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `trx_salesretur_item_ibfk_2` FOREIGN KEY (`soid`) REFERENCES `trx_salesorder` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_salesretur_item
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
