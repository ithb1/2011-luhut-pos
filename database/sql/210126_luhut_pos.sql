/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : 2011_luhut_pos

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 26/01/2021 19:11:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mst_bahan
-- ----------------------------
DROP TABLE IF EXISTS `mst_bahan`;
CREATE TABLE `mst_bahan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_benang` enum('Poly','Poly Ester') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '\'Poly\',\'Poly Ester\'',
  `warna` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `stok` int(255) NULL DEFAULT NULL,
  `harga_beli` int(255) NULL DEFAULT NULL,
  `harga_jual` int(255) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jenis_benang_id`(`jenis_benang`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mst_bahan
-- ----------------------------
INSERT INTO `mst_bahan` VALUES (1, 'SB-0001', 'Diadora', 'Poly', 'Kelabu', 10, 15000, 15000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (2, 'SB-0002', 'Diadora', 'Poly', 'Putih', 150, 25000, 25000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (3, 'SB-0003', 'Diadora', 'Poly', 'Biru', 40, 13000, 13000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (4, 'SB-0004', 'Scubba', 'Poly', 'Hitam', 80, 12000, 12000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (5, 'SB-0005', 'Scubba', 'Poly', 'Putih', 60, 15000, 15000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (6, 'SB-0006', 'Scubba', 'Poly', 'Kuning', 60, 14000, 14000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (7, 'SB-0007', 'Lycra', 'Poly Ester', 'Hitam', 60, 16000, 16000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (8, 'SB-0008', 'Lycra', 'Poly Ester', 'Putih', 125, 18000, 18000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (9, 'SB-0009', 'Lycra', 'Poly Ester', 'Kelabu', 60, 20000, 20000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (10, 'SB-0010', 'Diadora', 'Poly', 'Hijau', 60, 51000, 51000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (11, 'SB-0011', 'Diadora', 'Poly', 'Merah', 60, 16000, 16000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (12, 'SB-0012', 'Diadora', 'Poly', 'Kuning', 60, 14000, 14000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (13, 'SB-0013', 'Scubba', 'Poly', 'Kelabu', 60, 15000, 15000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (14, 'SB-0014', 'Scubba', 'Poly', 'Biru', 60, 15000, 15000, NULL, NULL);
INSERT INTO `mst_bahan` VALUES (15, 'SB-0015', 'Lycra', 'Poly Ester', 'Hijau', 60, 13000, 13000, NULL, NULL);

-- ----------------------------
-- Table structure for mst_supplier
-- ----------------------------
DROP TABLE IF EXISTS `mst_supplier`;
CREATE TABLE `mst_supplier`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `lokasi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `norekening` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `penanggungjawab` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mst_supplier
-- ----------------------------
INSERT INTO `mst_supplier` VALUES (1, 'PT.Kahatex', 'Jl. Cijerah no.20', 'BCA', '7339765242', 'Yayan Susanto');
INSERT INTO `mst_supplier` VALUES (2, 'Yomatex', 'jl. pangsor no25', 'Mandiri', '0700654000635', 'Yasha Shin');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'Superadmin');
INSERT INTO `sys_role` VALUES (2, 'Gudang');
INSERT INTO `sys_role` VALUES (3, 'Purchasing');
INSERT INTO `sys_role` VALUES (4, 'Kasir');
INSERT INTO `sys_role` VALUES (5, 'Admin Umum');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jabatan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `roleid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `roleid`(`roleid`) USING BTREE,
  CONSTRAINT `sys_user_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `sys_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'yudi123', '$2y$10$EIVvz7/P8dl81tQthxM4ZOcx1JYZFAiOKYNPXYyo/iKmzo/H.6mua', 'yudi', 'Admin Umum', 5);
INSERT INTO `sys_user` VALUES (2, 'yayan123', '$2y$10$EIVvz7/P8dl81tQthxM4ZOcx1JYZFAiOKYNPXYyo/iKmzo/H.6mua', 'Yayan', 'Gudang', 2);
INSERT INTO `sys_user` VALUES (3, 'yenny123', '$2y$10$EIVvz7/P8dl81tQthxM4ZOcx1JYZFAiOKYNPXYyo/iKmzo/H.6mua', 'Yenny', 'Manajer', 1);
INSERT INTO `sys_user` VALUES (4, 'yoyo123', '$2y$10$EIVvz7/P8dl81tQthxM4ZOcx1JYZFAiOKYNPXYyo/iKmzo/H.6mua', 'Yoyo', 'Purchasing', 3);
INSERT INTO `sys_user` VALUES (5, 'Yosi123', '$2y$10$EIVvz7/P8dl81tQthxM4ZOcx1JYZFAiOKYNPXYyo/iKmzo/H.6mua', 'Yosi', 'Kasir', 4);

-- ----------------------------
-- Table structure for trx_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `trx_pembayaran`;
CREATE TABLE `trx_pembayaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `jenis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'TRANSFER, CASH',
  `total` int(255) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_pembayaran
-- ----------------------------
INSERT INTO `trx_pembayaran` VALUES (9, 'PU01', '2020-12-03', 'TRANSFER', 123, '2020-12-21 05:37:15', '2020-12-28 07:59:53');
INSERT INTO `trx_pembayaran` VALUES (10, 'PU-22', '2020-12-22', 'TRANSFER', 123123, '2020-12-28 07:59:26', '2020-12-28 08:00:51');

-- ----------------------------
-- Table structure for trx_pembayaran_item
-- ----------------------------
DROP TABLE IF EXISTS `trx_pembayaran_item`;
CREATE TABLE `trx_pembayaran_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poid` int(11) NULL DEFAULT NULL,
  `pembayaranid` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `poid`(`poid`) USING BTREE,
  INDEX `bahanid`(`pembayaranid`) USING BTREE,
  CONSTRAINT `trx_pembayaran_item_ibfk_1` FOREIGN KEY (`poid`) REFERENCES `trx_purchaseorder` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `trx_pembayaran_item_ibfk_2` FOREIGN KEY (`pembayaranid`) REFERENCES `mst_bahan` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_pembayaran_item
-- ----------------------------
INSERT INTO `trx_pembayaran_item` VALUES (12, 7, 9, '2020-12-28 07:59:50', '2020-12-28 07:59:50');
INSERT INTO `trx_pembayaran_item` VALUES (13, 10, 10, '2020-12-28 08:00:01', '2020-12-28 08:00:01');
INSERT INTO `trx_pembayaran_item` VALUES (14, 11, 10, '2020-12-28 08:00:46', '2020-12-28 08:00:46');

-- ----------------------------
-- Table structure for trx_penerimaan
-- ----------------------------
DROP TABLE IF EXISTS `trx_penerimaan`;
CREATE TABLE `trx_penerimaan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poid` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nomor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_penerimaan
-- ----------------------------
INSERT INTO `trx_penerimaan` VALUES (8, '7', 'IN-02', '2020-12-23', NULL, '2020-12-19 22:50:36', '2020-12-19 22:50:36');
INSERT INTO `trx_penerimaan` VALUES (9, '7', 'IN-02', '2020-12-23', NULL, '2020-12-19 22:50:36', '2020-12-19 22:50:36');

-- ----------------------------
-- Table structure for trx_penerimaan_item
-- ----------------------------
DROP TABLE IF EXISTS `trx_penerimaan_item`;
CREATE TABLE `trx_penerimaan_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penerimaanid` int(11) NULL DEFAULT NULL,
  `poid` int(11) NULL DEFAULT NULL,
  `poitemid` int(11) NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `poid`(`poitemid`) USING BTREE,
  CONSTRAINT `trx_penerimaan_item_ibfk_1` FOREIGN KEY (`poitemid`) REFERENCES `trx_purchaseorder_item` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_penerimaan_item
-- ----------------------------
INSERT INTO `trx_penerimaan_item` VALUES (12, 8, 7, 5, 2, NULL, '2020-12-20 04:15:57', '2020-12-20 04:15:57', 0);
INSERT INTO `trx_penerimaan_item` VALUES (13, 8, 7, 6, 3, NULL, '2020-12-20 04:16:02', '2020-12-20 04:16:02', 1);
INSERT INTO `trx_penerimaan_item` VALUES (14, 8, 7, 5, 4, NULL, '2020-12-20 04:16:13', '2020-12-20 04:16:13', 1);
INSERT INTO `trx_penerimaan_item` VALUES (15, NULL, 7, NULL, NULL, NULL, '2020-12-20 22:52:01', '2020-12-20 22:52:01', NULL);
INSERT INTO `trx_penerimaan_item` VALUES (16, 8, 7, 5, 3, NULL, '2020-12-21 05:43:02', '2020-12-21 05:43:02', 1);
INSERT INTO `trx_penerimaan_item` VALUES (17, 9, 7, 6, 3, NULL, '2020-12-21 05:43:27', '2020-12-21 05:43:27', 1);
INSERT INTO `trx_penerimaan_item` VALUES (18, 9, 7, 5, 3, NULL, '2020-12-21 05:44:17', '2020-12-21 05:44:17', 0);

-- ----------------------------
-- Table structure for trx_purchaseorder
-- ----------------------------
DROP TABLE IF EXISTS `trx_purchaseorder`;
CREATE TABLE `trx_purchaseorder`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `supplier_id` int(11) NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `total` int(255) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status_pembayaran` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT 'UNPAID, PAID',
  `status_penerimaan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'PROGRESS, DONE',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `supplier_id`(`supplier_id`) USING BTREE,
  CONSTRAINT `trx_purchaseorder_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `mst_supplier` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_purchaseorder
-- ----------------------------
INSERT INTO `trx_purchaseorder` VALUES (7, 'PO-11', 1, '2020-12-04', 117000, NULL, 'PAID', 'DONE', '2020-12-13 11:41:55', '2020-12-28 07:59:50');
INSERT INTO `trx_purchaseorder` VALUES (10, 'PO-12', 1, '2020-12-04', 117000, NULL, 'PAID', 'DONE', '2020-12-13 11:41:55', '2020-12-28 08:00:01');
INSERT INTO `trx_purchaseorder` VALUES (11, 'PO-13', 1, '2020-12-04', 117000, NULL, 'PAID', 'DONE', '2020-12-13 11:41:55', '2020-12-28 08:00:46');

-- ----------------------------
-- Table structure for trx_purchaseorder_item
-- ----------------------------
DROP TABLE IF EXISTS `trx_purchaseorder_item`;
CREATE TABLE `trx_purchaseorder_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poid` int(11) NULL DEFAULT NULL,
  `bahanid` int(11) NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `harga` int(255) NULL DEFAULT NULL,
  `total` int(255) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `issesuai` int(255) NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `poid`(`poid`) USING BTREE,
  INDEX `bahanid`(`bahanid`) USING BTREE,
  CONSTRAINT `trx_purchaseorder_item_ibfk_1` FOREIGN KEY (`poid`) REFERENCES `trx_purchaseorder` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `trx_purchaseorder_item_ibfk_2` FOREIGN KEY (`bahanid`) REFERENCES `mst_bahan` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_purchaseorder_item
-- ----------------------------
INSERT INTO `trx_purchaseorder_item` VALUES (5, 7, 8, 4, 18000, 72000, NULL, 0, '2020-12-13 11:42:25', '2020-12-13 11:43:43');
INSERT INTO `trx_purchaseorder_item` VALUES (6, 7, 14, 3, 15000, 45000, NULL, 0, '2020-12-13 11:42:44', '2020-12-13 11:42:44');
INSERT INTO `trx_purchaseorder_item` VALUES (8, 10, 8, 4, 18000, 72000, NULL, 0, '2020-12-13 11:42:25', '2020-12-13 11:43:43');
INSERT INTO `trx_purchaseorder_item` VALUES (9, 10, 14, 3, 15000, 45000, NULL, 0, '2020-12-13 11:42:44', '2020-12-13 11:42:44');
INSERT INTO `trx_purchaseorder_item` VALUES (10, 10, 8, 4, 18000, 72000, NULL, 0, '2020-12-13 11:42:25', '2020-12-13 11:43:43');
INSERT INTO `trx_purchaseorder_item` VALUES (11, 10, 14, 3, 15000, 45000, NULL, 0, '2020-12-13 11:42:44', '2020-12-13 11:42:44');

-- ----------------------------
-- Table structure for trx_salesorder
-- ----------------------------
DROP TABLE IF EXISTS `trx_salesorder`;
CREATE TABLE `trx_salesorder`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `total` int(255) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'WAITING APPROVE, DONE',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_salesorder
-- ----------------------------
INSERT INTO `trx_salesorder` VALUES (1, 'OUt-01', '2020-12-11', NULL, NULL, NULL, NULL, '2020-12-23 05:41:04');
INSERT INTO `trx_salesorder` VALUES (5, 'SO-05', '2020-12-11', 2030000, NULL, 'DONE', NULL, NULL);

-- ----------------------------
-- Table structure for trx_salesorder_item
-- ----------------------------
DROP TABLE IF EXISTS `trx_salesorder_item`;
CREATE TABLE `trx_salesorder_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soid` int(11) NULL DEFAULT NULL,
  `bahanid` int(11) NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `harga` int(255) NULL DEFAULT NULL,
  `total` int(255) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'approve, reject oleh gudang',
  `approved_at` datetime(0) NULL DEFAULT NULL,
  `approved_by` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'nama user',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `poid`(`soid`) USING BTREE,
  INDEX `bahanid`(`bahanid`) USING BTREE,
  CONSTRAINT `trx_salesorder_item_ibfk_2` FOREIGN KEY (`bahanid`) REFERENCES `mst_bahan` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `trx_salesorder_item_ibfk_3` FOREIGN KEY (`soid`) REFERENCES `trx_salesorder` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_salesorder_item
-- ----------------------------
INSERT INTO `trx_salesorder_item` VALUES (1, 1, 1, 22, 15000, 330000, NULL, NULL, '2020-12-23 05:13:46', 'REJECTED', '2020-12-23 05:13:46', 'yudi');
INSERT INTO `trx_salesorder_item` VALUES (2, 5, 1, 22, 15000, 330000, NULL, NULL, '2020-12-23 05:03:36', 'APPROVED', '2020-12-23 05:03:36', 'yudi');
INSERT INTO `trx_salesorder_item` VALUES (3, 5, 9, 10, 20000, 200000, NULL, NULL, '2020-12-23 05:07:46', 'APPROVED', '2020-12-23 05:07:46', 'yudi');
INSERT INTO `trx_salesorder_item` VALUES (5, 5, 3, 90, 13000, 1170000, NULL, NULL, '2020-12-23 05:14:07', 'APPROVED', '2020-12-23 05:14:07', 'yudi');

-- ----------------------------
-- Table structure for trx_salesretur
-- ----------------------------
DROP TABLE IF EXISTS `trx_salesretur`;
CREATE TABLE `trx_salesretur`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soid` int(11) NULL DEFAULT NULL,
  `nomor` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `total` int(255) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `soid`(`soid`) USING BTREE,
  CONSTRAINT `trx_salesretur_ibfk_1` FOREIGN KEY (`soid`) REFERENCES `trx_salesorder` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_salesretur
-- ----------------------------
INSERT INTO `trx_salesretur` VALUES (2, 5, 'RO-11', '2020-12-14', 104000, NULL, '2020-12-28 08:31:04', '2020-12-28 08:31:17');

-- ----------------------------
-- Table structure for trx_salesretur_item
-- ----------------------------
DROP TABLE IF EXISTS `trx_salesretur_item`;
CREATE TABLE `trx_salesretur_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `returid` int(11) NULL DEFAULT NULL,
  `soid` int(11) NULL DEFAULT NULL,
  `soitemid` int(11) NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `total` int(255) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `poid`(`soid`) USING BTREE,
  INDEX `bahanid`(`soitemid`) USING BTREE,
  CONSTRAINT `trx_salesretur_item_ibfk_1` FOREIGN KEY (`soitemid`) REFERENCES `mst_bahan` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `trx_salesretur_item_ibfk_2` FOREIGN KEY (`soid`) REFERENCES `trx_salesorder` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_salesretur_item
-- ----------------------------
INSERT INTO `trx_salesretur_item` VALUES (5, 2, 5, 5, 8, 104000, 'rusak', '2020-12-28 08:31:17', '2020-12-28 08:31:17');

SET FOREIGN_KEY_CHECKS = 1;
