<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', "HomeController@index")->name("home");

//AUTH
Route::get('login', 'Auth\LoginController@login')->name("login");
Route::post('login', 'Auth\LoginController@authenticate');
Route::post('logout', 'Auth\LoginController@logout');
Route::get('getlogout', 'Auth\LoginController@logout');
Route::get('postlogout', 'Auth\LoginController@postlogout');
Route::get('oauth2/logout', 'Auth\LoginController@handleLogoutSSO');




Route::get('mst/supplier/delete/{id}', 'Mst\SupplierController@delete');

Route::resource('mst/supplier', 'Mst\SupplierController');

Route::get('mst/bahan/detailjson/{id}', 'Mst\BahanController@detailJson');
Route::get('mst/bahan/delete/{id}', 'Mst\BahanController@delete');
Route::resource('mst/bahan', 'Mst\BahanController');

Route::get('mst/user/delete/{id}', 'Mst\UserController@delete');

Route::resource('mst/user', 'Mst\UserController');




Route::resource('trx/stok', 'Trx\StokController');

Route::post('trx/purchasing_item', 'Trx\PurchasingController@addItem');
Route::get('trx/purchasing_item/delete/{id}', 'Trx\PurchasingController@deleteItem');
Route::get('trx/purchasing/headerjson/{id}', 'Trx\PurchasingController@headerJson');
Route::get('trx/purchasing/detailjson/{id}', 'Trx\PurchasingController@detailJson');
Route::get('trx/purchasing/delete/{id}', 'Trx\PurchasingController@delete');
Route::get('trx/purchasing/send/{id}', 'Trx\PurchasingController@sendEmail');
Route::resource('trx/purchasing', 'Trx\PurchasingController');

Route::post('trx/penerimaan_item', 'Trx\PenerimaanController@updateItem');
Route::get('trx/penerimaan/detailjson/{id}', 'Trx\PenerimaanController@detailJson');
Route::get('trx/penerimaan/delete/{id}', 'Trx\PenerimaanController@delete');
Route::resource('trx/penerimaan', 'Trx\PenerimaanController');

Route::post('trx/pembayaran_item', 'Trx\PembayaranUtangController@addItem');
Route::get('trx/pembayaran_item/delete/{id}', 'Trx\PembayaranUtangController@deleteItem');
Route::get('trx/pembayaran/delete/{id}', 'Trx\PembayaranUtangController@delete');
Route::resource('trx/pembayaran', 'Trx\PembayaranUtangController');

Route::post('trx/penjualan_item', 'Trx\PenjualanController@addItem');
Route::get('trx/penjualan_item/approve/{id}/{status}', 'Trx\PenjualanController@approveItem');
Route::get('trx/penjualan_item/delete/{id}', 'Trx\PenjualanController@deleteItem');
Route::get('trx/penjualan/detailjson/{id}', 'Trx\PenjualanController@detailJson');
Route::get('trx/penjualan/delete/{id}', 'Trx\PenjualanController@delete');
Route::resource('trx/penjualan', 'Trx\PenjualanController');

Route::post('trx/retur_item', 'Trx\ReturController@updateItem');
Route::get('trx/retur/detailjson/{id}', 'Trx\ReturController@detailJson');
Route::get('trx/retur/delete/{id}', 'Trx\ReturController@delete');
Route::resource('trx/retur', 'Trx\ReturController');


Route::post('export/pembayaranutang', 'Export\PdfController@pembayaranutang');
Route::post('export/penerimaan', 'Export\PdfController@penerimaan');
Route::post('export/penjualan', 'Export\PdfController@penjualan');
Route::post('export/purchasing', 'Export\PdfController@purchasing');
Route::post('export/retur', 'Export\PdfController@retur');
Route::post('export/stok', 'Export\PdfController@stok');

Route::get('export/purchasing/{id}', 'Export\PdfController@purchasingDetail');
Route::get('export/retur/{id}', 'Export\PdfController@returDetail');
Route::get('export/penjualan/{id}', 'Export\PdfController@penjualanDetail');
Route::get('export/penerimaan/{id}', 'Export\PdfController@penerimaanDetail');
Route::get('export/pembayaran/{id}', 'Export\PdfController@pembayaranDetail');

